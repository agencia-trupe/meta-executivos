@section('conteudo')
	
	<div id="slides-home">
		@if($banners)
			<div id="slides">
				@foreach($banners as $k => $banner)
					<div class="slide" @if($k > 0) style='display:none;' @endif>
						<a href="{{$banner->link}}" style="background-image:url('assets/images/banners/{{$banner->imagem}}');">
							<img src="assets/images/banners/{{$banner->imagem}}">
						</a>
					</div>
				@endforeach
			</div>
			<div id="slides-nav"></div>
		@endif
	</div>

@stop
