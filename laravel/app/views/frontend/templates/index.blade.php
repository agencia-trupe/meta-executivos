<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=980,initial-scale=1">
    
    <meta name="keywords" content="" />

	<title>Meta Executivos</title>
	<meta name="description" content="">
	<meta property="og:title" content="Meta Executivos"/>
	<meta property="og:description" content=""/>

    <meta property="og:site_name" content="Meta Executivos"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>
	
	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/fancybox/source/jquery.fancybox',
		'css/base',
		'css/'.str_replace('-', '', Route::currentRouteName())
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>	
	
	@if(App::environment()=='local')
		<?=Assets::JS(array('vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('vendor/modernizr/modernizr'))?>
	@endif

	<script>
		// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
		// Date();a=s.createElement(o),
		// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		// ga('create', 'UA-', '');
		// ga('send', 'pageview');
	</script>
</head>
<body>

	<header>
		<div id="aux-nav">
			<div class="centro">
				<div class="telefone">
					@if($contato->telefone)
						{{ Tools::formataTelefone($contato->telefone) }}
					@endif
				</div>
				<ul>
					@if($contato->facebook)
						<li><a href="{{ $contato->facebook }}" class="fb-link" target="_blank" title="Facebook">Facebook</a></li>
					@endif
					@if($contato->twitter)
						<li><a href="{{ $contato->twitter }}" class="tw-link" target="_blank" title="Twitter">Twitter</a></li>
					@endif
					@if($contato->linkedin)
						<li><a href="{{ $contato->linkedin }}" class="lk-link" target="_blank" title="Linkedin">Linkedin</a></li>
					@endif
					<li>
						<a href="{{ Lang::get('frontend.base.lang.trocar') }}" title="{{ Lang::get('frontend.base.lang.outraVersao') }}">
							<img src="assets/images/layout/{{ Lang::get('frontend.base.lang.imagem') }}" alt="{{ Lang::get('frontend.base.lang.outraVersao') }}"> {{ Lang::get('frontend.base.lang.outraVersaoReduzido') }}
						</a>
					</li>
				</ul>
			</div>
		</div>
		<nav>
			<div class="centro">
				<a href="home" title="{{Lang::get('frontend.base.nav.paginaInicial')}}" id="link-home"><img src="assets/images/layout/logo.png" alt="{{Lang::get('frontend.base.nav.paginaInicial')}}"></a>
				<ul id="main-nav">
					<li>
						<a href="home" title="{{Lang::get('frontend.base.nav.home')}}" @if(str_is('home*', Route::currentRouteName())) class='ativo' @endif>{{Lang::get('frontend.base.nav.home')}}</a>
					</li>
					<li class="comsub">
						<a href="empresa" id="mn-empresa" title="{{Lang::get('frontend.base.nav.empresa')}}" @if(str_is('quemsomos*', Route::currentRouteName()) || str_is('diferenciais*', Route::currentRouteName())) class='ativo' @endif>{{Lang::get('frontend.base.nav.empresa')}} <div class="seta"></div></a>
						<ul class="submenu">
							<li><a href="empresa/quemsomos" title="{{Lang::get('frontend.base.nav.subempresa.quemsomos')}}">{{Lang::get('frontend.base.nav.subempresa.quemsomos')}}</a></li>
							<li><a href="empresa/diferenciais" title="{{Lang::get('frontend.base.nav.subempresa.diferenciais')}}">{{Lang::get('frontend.base.nav.subempresa.diferenciais')}}</a></li>
						</ul>
					</li>
					<li class="comsub">
						<a href="solucoes" id="mn-solucoes" title="{{Lang::get('frontend.base.nav.solucoes')}}" @if(str_is('executive_search*', Route::currentRouteName()) || str_is('desenvolvimento*', Route::currentRouteName())) class='ativo' @endif>{{Lang::get('frontend.base.nav.solucoes')}} <div class="seta"></div></a>
						<ul class="submenu">
							<li><a href="solucoes/executive_search" title="{{Lang::get('frontend.base.nav.subsolucoes.executivesearch')}}">{{Lang::get('frontend.base.nav.subsolucoes.executivesearch')}}</a></li>
							<li><a href="solucoes/desenvolvimento_organizacional" title="{{Lang::get('frontend.base.nav.subsolucoes.desenvolvimento')}}">{{Lang::get('frontend.base.nav.subsolucoes.desenvolvimento')}}</a></li>
						</ul>
					</li>
					<li>
						<a href="oportunidades" title="{{Lang::get('frontend.base.nav.oportunidades')}}" @if(str_is('oportunidades*', Route::currentRouteName())) class='ativo' @endif>{{Lang::get('frontend.base.nav.oportunidades')}}</a>
					</li>
					<li class="comsub">
						<a href="cases" id="mn-cases" title="{{Lang::get('frontend.base.nav.cases')}}" @if(str_is('cases*', Route::currentRouteName())) class='ativo' @endif>{{Lang::get('frontend.base.nav.cases')}} <div class="seta"></div></a>
						<ul class="submenu">
							<li><a href="cases/cases" title="{{Lang::get('frontend.base.nav.subcases.cases')}}">{{Lang::get('frontend.base.nav.subcases.cases')}}</a></li>
							<li><a href="cases/depoimentos" title="{{Lang::get('frontend.base.nav.subcases.depoimentos')}}">{{Lang::get('frontend.base.nav.subcases.depoimentos')}}</a></li>
						</ul>
					</li>
					<li>
						<a href="contato" title="{{Lang::get('frontend.base.nav.contato')}}" @if(str_is('contato*', Route::currentRouteName())) class='ativo' @endif>{{Lang::get('frontend.base.nav.contato')}}</a>
					</li>
				</ul>
			</div>
		</nav>
	</header>

	<!-- Conteúdo Principal -->
	<div class="main {{ 'main-'. str_replace('-', '', Route::currentRouteName()) }}">
		@yield('conteudo')
	</div>

	<div class="barra-servicos">
		<div class="centro">
			<div class="conheca">
				<span class="linha linha1">{{Lang::get('frontend.base.barra.linha1')}}</span>
				<span class="linha linha2">{{Lang::get('frontend.base.barra.linha2')}}</span>
				<span class="linha linha3">{{Lang::get('frontend.base.barra.linha3')}}</span>
			</div>
			<div class="servicos-executive">
				<a href="solucoes/executive_search" class="titulo" title="{{Lang::get('frontend.base.nav.subsolucoes.executivesearch')}}">
					<div class="imagem">
						<img src="assets/images/layout/icone-executive-search.png" alt="{{Lang::get('frontend.base.nav.subsolucoes.executivesearch')}}">
					</div>
					{{ Tools::negritoUltimaPalavra($solucoesExecTitulo->{Lang::get('frontend.base.campos.titulo')}) }}
				</a>
				<ul>
					@if($solucoesExecLista)
						@foreach($solucoesExecLista as $item)
							<li><a href="solucoes/executive_search/{{ $item->{Lang::get('frontend.base.campos.slug')} }}" title="{{ $item->{Lang::get('frontend.base.campos.titulo')} }}"><span>&raquo;</span> {{ $item->{Lang::get('frontend.base.campos.titulo')} }}</li></a>
						@endforeach
					@endif
				</ul>
			</div>
			<div class="servicos-desenvolvimento">
				<a href="solucoes/desenvolvimento_organizacional" class="titulo" title="{{Lang::get('frontend.base.nav.subsolucoes.desenvolvimento')}}">
					<div class="imagem">
						<img src="assets/images/layout/icone-desenvolvimento-organizacional.png" alt="{{Lang::get('frontend.base.nav.subsolucoes.desenvolvimento')}}">
					</div>
					<span>{{ Tools::negritoUltimaPalavra($solucoesDevTitulo->{Lang::get('frontend.base.campos.titulo')}) }}</span>
				</a>
				<ul>
					@if($solucoesDevLista)
						@foreach($solucoesDevLista as $item)
							<li><a href="solucoes/desenvolvimento_organizacional/{{ $item->{Lang::get('frontend.base.campos.slug')} }}" title="{{ $item->{Lang::get('frontend.base.campos.titulo')} }}"><span>&raquo;</span> {{ $item->{Lang::get('frontend.base.campos.titulo')} }}</li></a>
						@endforeach
					@endif
				</ul>
			</div>
		</div>
	</div>

	<footer>
		<div class="centro">
			<div class="footer-coluna coluna1">
				<ul class="topmenu">
					<li>
						<a href="home" title="{{Lang::get('frontend.base.nav.home')}}">{{Lang::get('frontend.base.nav.home')}}</a>
					</li>
					<li class="comsub">
						<a href="empresa" title="{{Lang::get('frontend.base.nav.empresa')}}">{{Lang::get('frontend.base.nav.empresa')}}</a>
						<ul class="submenu">
							<li><a href="empresa/quemsomos" title="{{Lang::get('frontend.base.nav.subempresa.quemsomos')}}">{{Lang::get('frontend.base.nav.subempresa.quemsomos')}}</a></li>
							<li><a href="empresa/diferenciais" title="{{Lang::get('frontend.base.nav.subempresa.diferenciais')}}">{{Lang::get('frontend.base.nav.subempresa.diferenciais')}}</a></li>
						</ul>
					</li>
					<li>
						<a href="oportunidades" title="{{Lang::get('frontend.base.nav.oportunidades')}}">{{Lang::get('frontend.base.nav.oportunidadesExtendido')}}</a>
					</li>
					<li class="comsub">
						<a href="cases" title="{{Lang::get('frontend.base.nav.cases')}}">{{Lang::get('frontend.base.nav.cases')}}</a>
						<ul class="submenu">
							<li><a href="cases/cases" title="{{Lang::get('frontend.base.nav.subcases.cases')}}">{{Lang::get('frontend.base.nav.subcases.cases')}}</a></li>
							<li><a href="cases/depoimentos" title="{{Lang::get('frontend.base.nav.subcases.depoimentos')}}">{{Lang::get('frontend.base.nav.subcases.depoimentos')}}</a></li>
						</ul>
					</li>
					<li>
						<a href="contato" title="{{Lang::get('frontend.base.nav.contato')}}">{{Lang::get('frontend.base.nav.contato')}}</a>
					</li>
				</ul>
			</div>
			<div class="footer-coluna coluna2">
				<ul class="topmenu">
					<li class="comsub">
						<a href="solucoes/{{ $solucoesExecTitulo->{Lang::get('frontend.base.campos.slug')} }}" title="{{ $solucoesExecTitulo->{Lang::get('frontend.base.campos.titulo')} }}">{{ $solucoesExecTitulo->{Lang::get('frontend.base.campos.titulo')} }}</a>
						<ul class="submenu">
							@if($solucoesExecLista)
								@foreach($solucoesExecLista as $item)
									<li><a href="solucoes/executive_search/{{ $item->{Lang::get('frontend.base.campos.slug')} }}" title="{{ $item->{Lang::get('frontend.base.campos.titulo')} }}">{{ $item->{Lang::get('frontend.base.campos.titulo')} }}</a></li>
								@endforeach
							@endif
						</ul>
					</li>
					<li class="comsub">
						<a href="solucoes/{{ $solucoesDevTitulo->{Lang::get('frontend.base.campos.slug')} }}" title="{{ $solucoesDevTitulo->{Lang::get('frontend.base.campos.titulo')} }}">{{ $solucoesDevTitulo->{Lang::get('frontend.base.campos.titulo')} }}</a>
						<ul class="submenu">
							@if($solucoesDevLista)
								@foreach($solucoesDevLista as $itemD)
									<li><a href="solucoes/desenvolvimento_organizacional/{{ $itemD->{Lang::get('frontend.base.campos.slug')} }}" title="{{ $itemD->{Lang::get('frontend.base.campos.titulo')} }}">{{ $itemD->{Lang::get('frontend.base.campos.titulo')} }}</a></li>
								@endforeach
							@endif
						</ul>
					</li>
				</ul>
				<div class="grupo">
					<p>{{Lang::get('frontend.base.footer.grupo')}}</p>
					<img src="assets/images/layout/footer-grupometarh.png" alt="Grupo Meta RH">
				</div>
			</div>
			<div class="footer-coluna coluna3">
				<div class="social">
					@if($contato->facebook)
						<a href="{{ $contato->facebook }}" class="fb-link" target="_blank" title="Facebook">Facebook</a>
					@endif
					@if($contato->twitter)
						<a href="{{ $contato->twitter }}" class="tw-link" target="_blank" title="Twitter">Twitter</a>
					@endif
					@if($contato->linkedin)
						<a href="{{ $contato->linkedin }}" class="lk-link" target="_blank" title="Linkedin">Linkedin</a>
					@endif
				</div>
				@if($contato->telefone)
					<div class="telefone">{{ Tools::formataTelefone($contato->telefone) }}</div>
				@endif
				@if($contato->endereco)
					<div class="endereco">{{$contato->endereco}}</div>
				@endif
				<div class="assinatura">
					&copy; {{date('Y')}} {{ Lang::get('frontend.base.footer.direitos') }}<br>
					<a href="http://www.trupe.net" target="_blank" title="Criação de sites: Trupe Agência Criativa">Criação de sites: Trupe Agência Criativa <img src="assets/images/layout/footer-trupe.png" alt="Trupe Agência Criativa"></a>
				</div>
			</div>
		</div>
	</footer>

	<?=Assets::JS(array(
		'vendor/fancybox/source/jquery.fancybox',
		'vendor/jquery.cycle/jquery.cycle.all',
		'js/main'
	))?>

</body>
</html>
