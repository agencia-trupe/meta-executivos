@section('conteudo')

	<dix id="faixa-azul-titulo">
		<h1 class="centro">
			<span>{{ Lang::get('frontend.empresa.titulo') }} &raquo;</span> {{ Lang::get('frontend.empresa.quemsomos') }}
		</h1>
	</dix>

	<div class="conteudo">
		<div class="centro">
			<div class="texto cke" style="width:500px">
				{{$texto->{Lang::get('frontend.base.campos.texto')} }}

				<a href="empresa/diferenciais" class="link-diferenciais" title="{{Lang::get('frontend.empresa.diferenciais')}}">
					<span>{{Lang::get('frontend.empresa.diferenciais')}} &raquo;</span>
				</a>
			</div>

			<div class="imagem" style="width:445px;padding-top:13px">
				{{--<img src="assets/images/layout/quemsomos-foto.jpg" alt="{{ Lang::get('frontend.empresa.quemsomos') }}">--}}
				<iframe width="445" height="250" src="https://www.youtube.com/embed/2KUNDTziJpM" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</div>

	@if($equipe)
		@foreach($equipe->chunk(2) as $chunk)
		<div id="equipe">
			<div class="centro">
				@foreach ($chunk as $key => $value)
					<div class="membro">
						<div class="imagem">
							<img src="assets/images/equipe/{{$value->imagem}}" alt="{{$value->{Lang::get('frontend.base.campos.titulo')} }}">
						</div>
						<div class="titulo">
							<h2>{{$value->titulo_pt }}</h2>
							<div class="divisao">
								@if($value->divisao == 'executive_search')
									<div class="imagem">
										<img src="assets/images/layout/quemsomos-executive-search.png" alt="{{Lang::get('frontend.base.nav.subsolucoes.executivesearch')}}">
									</div>
									<p>{{Lang::get('frontend.empresa.responsavelpor')}}</p>
									<h3>{{Tools::negritoUltimaPalavra(Lang::get('frontend.base.nav.subsolucoes.executivesearch'))}}</h3>
								@elseif($value->divisao == 'mentoria_sistemica')
									<div class="imagem">
										<img src="assets/images/layout/quemsomos-mentoria-sistemica.png" alt="{{Lang::get('frontend.base.nav.subsolucoes.mentoria')}}">
									</div>
									<p>{{Lang::get('frontend.empresa.responsavelpor')}}</p>
									<h3 style="width:256px">{{Tools::negritoDuasUltimasPalavras(Lang::get('frontend.base.nav.subsolucoes.mentoria'))}}</h3>
								@elseif($value->divisao == 'mentoria_sistemica')
								@else
									<div class="imagem">
										<img src="assets/images/layout/quemsomos-desenvolvimento-organizacional.png" alt="{{Lang::get('frontend.base.nav.subsolucoes.desenvolvimento')}}">
									</div>
									<p>{{Lang::get('frontend.empresa.responsavelpor')}}</p>
									<h3>{{Tools::negritoUltimaPalavra(Lang::get('frontend.base.nav.subsolucoes.desenvolvimento'))}}</h3>
								@endif
							</div>
						</div>
						<div class="texto cke">
							{{ $value->{Lang::get('frontend.base.campos.texto')} }}
						</div>
					</div>
				@endforeach
			</div>
		</div>
		@endforeach
	@endif

@stop
