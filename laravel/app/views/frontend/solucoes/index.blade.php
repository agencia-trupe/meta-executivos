@section('conteudo')
	
	<dix id="faixa-azul-titulo">
		<h1 class="centro">
			<span>{{ Lang::get('frontend.solucoes.titulo') }} &raquo;</span> {{ Lang::get('frontend.solucoes.exec') }}
		</h1>
	</dix>

	<div class="conteudo">
		<div class="centro">
			<aside>
				<ul>
					<li>
						<a href="solucoes/executive_search" title="{{ Lang::get('frontend.solucoes.exec') }}" class="link-exec @if($secao=='exec') ativo @endif">
							<div class="imagem">
								<img src="assets/images/layout/icone-executive-search.png" alt="{{ Lang::get('frontend.solucoes.exec') }}">
							</div>
							<span>
								{{ Tools::negritoUltimaPalavra(Lang::get('frontend.solucoes.exec')) }}
							</span>
						</a>
					</li>
					@if($listaExec)
						@foreach ($listaExec as $key => $value)
							<li @if($key == 0) class='pull-up'@endif>
								<a href="solucoes/executive_search/{{$value->slug_pt }}" title="{{$value->{Lang::get('frontend.base.campos.titulo')} }}" class="link-exec-{{$value->slug_pt}} @if($marcar == $value->slug_pt) ativo @endif">{{$value->{Lang::get('frontend.base.campos.titulo')} }}</a>
							</li>
						@endforeach
					@endif
				</ul>
				<ul>
					<li>
						<a href="solucoes/desenvolvimento_organizacional" title="{{ Lang::get('frontend.solucoes.dev') }}" class="link-dev @if($secao=='dev') ativo @endif">
							<div class="imagem">
								<img src="assets/images/layout/icone-desenvolvimento-organizacional.png" alt="{{ Lang::get('frontend.solucoes.dev') }}">
							</div>
							<span>
								{{ Tools::negritoUltimaPalavra(Lang::get('frontend.solucoes.dev')) }}
							</span>
						</a>
					</li>
					@if($listaDev)
						@foreach ($listaDev as $key => $value)
							<li @if($key == 0) class='pull-up'@endif>
								<a href="solucoes/desenvolvimento_organizacional/{{$value->slug_pt }}" title="{{$value->{Lang::get('frontend.base.campos.titulo')} }}" class="link-exec-{{$value->slug_pt}} @if($marcar == $value->slug_pt) ativo @endif">{{$value->{Lang::get('frontend.base.campos.titulo')} }}</a>
							</li>
						@endforeach
					@endif
				</ul>
			</aside>
			<section>
				<h2><span>{{Tools::negritoUltimaPalavra($texto->{Lang::get('frontend.base.campos.titulo')}) }}</span></h2>
					@if($marcar != 'cursos-e-workshops')
						<div class="texto cke">
							{{$texto->{Lang::get('frontend.base.campos.texto')} }}
						</div>
					@else
						<div class="texto cursos">
							@if($cursos)
								@foreach($cursos as $curso)
									<div class="curso">
										<div class="titulo">
											{{$curso->{Lang::get('frontend.base.campos.titulo')} }}
										</div>
										<div class="descricao cke">
											{{$curso->{Lang::get('frontend.base.campos.texto')} }}
										</div>
									</div>
								@endforeach
							@endif
						</div>
					@endif
			</section>
		</div>
	</div>

@stop
