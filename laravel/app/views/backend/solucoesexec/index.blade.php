@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Soluções - Executive Search
    </h2>

    <hr>

    <h4><span class="glyphicon glyphicon-circle-arrow-right"></span> Texto de Abertura</h4>

    <table class='table table-striped table-bordered table-hover table-sortable' style="margin-top:15px;">

        <thead>
            <tr>
                <th>Título</th>
                <th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{$solucao->titulo_pt}}</td>
                <td>
                    Português : <br>{{ Str::words(strip_tags($solucao->texto_pt), 15)}}
                    <hr>
                    Inglês : <br>{{ Str::words(strip_tags($solucao->texto_en), 15)}}
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.solucoes.edit', $solucao->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>
        </tbody>
    </table>

    <hr>

    <h4><span class="glyphicon glyphicon-circle-arrow-right"></span> Textos Secundários</h4>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='solucoes_executive_search' style="margin-top:15px;">

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Título</th>
				<th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm btn-small'>mover</a></td>
				<td>
                    {{ $registro->titulo_pt }}                    
                </td>
				<td>
                    Português: <br>{{ Str::words(strip_tags($registro->texto_pt), 15) }}
                    <hr>
                    Inglês : <br>{{ Str::words(strip_tags($registro->texto_en), 15) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.solucoesexec.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop