@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.solucoesexec.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTituloPT">Título em Português</label>
					<input disabled type="text" class="form-control" id="inputTituloPT" name="titulo_pt" value="{{$registro->titulo_pt}}" required>
				</div>
				<div class="form-group">
					<label for="inputTituloEN">Título em Inglês</label>
					<input disabled type="text" class="form-control" id="inputTituloEN" name="titulo_en" value="{{$registro->titulo_en}}" required>
				</div>
				<div class="form-group">
					<label for="inputTextoPT">Texto em Português</label>
					<textarea name="texto_pt" class="form-control" id="inputTextoPT" >{{$registro->texto_pt }}</textarea>
				</div>
				<div class="form-group">
					<label for="inputTextoEN">Texto em Inglês</label>
					<textarea name="texto_en" class="form-control" id="inputTextoEN" >{{$registro->texto_en }}</textarea>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.solucoesexec.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop