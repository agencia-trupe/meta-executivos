@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.solucoesdev.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo em Português">Título em Português</label>
					<input type="text" class="form-control" id="inputTítulo em Português" name="titulo_pt" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo_pt'] }}" @endif required>
				</div>
				<div class="form-group">
					<label for="inputTítulo em Inglês">Título em Inglês</label>
					<input type="text" class="form-control" id="inputTítulo em Inglês" name="titulo_en" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo_en'] }}" @endif required>
				</div>
				<div class="form-group">
					<label for="inputTexto em Português">Texto em Português</label>
					<textarea name="texto_pt" class="form-control" id="inputTexto em Português" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_pt'] }} @endif</textarea>
				</div>	
				<div class="form-group">
					<label for="inputTexto em Inglês">Texto em Inglês</label>
					<textarea name="texto_en" class="form-control" id="inputTexto em Inglês" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_en'] }} @endif</textarea>
				</div>	

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.solucoesdev.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop