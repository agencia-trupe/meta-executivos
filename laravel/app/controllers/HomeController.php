<?php

use \Banner;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.home.index')->with('banners', Banner::where('idioma', App::getLocale())->orderBy('ordem', 'asc')->get());
	}

}
