<?php

use \Contato, \Solucao, \SolucoesDev, \SolucoesExec;

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout)->with('contato', Contato::first())
													 ->with('solucoesExecTitulo', Solucao::find(1))
													 ->with('solucoesExecLista', SolucoesExec::orderBy('ordem', 'ASC')->get())
													 ->with('solucoesDevTitulo', Solucao::find(2))
													 ->with('solucoesDevLista', SolucoesDev::orderBy('ordem', 'ASC')->get());
		}
	}

}
