<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, Equipe;

class EquipeController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '2';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.equipe.index')->with('registros', Equipe::orderBy('ordem', 'ASC')->get())->with('limiteInsercao', $this->limiteInsercao);		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.equipe.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Equipe;

		$object->titulo_pt = Input::get('titulo_pt');
		$object->divisao = Input::get('divisao');
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');
		
		$imagem = Thumb::make('imagem', 197, 226, 'equipe/');
		if($imagem) $object->imagem = $imagem;

		if($this->limiteInsercao && sizeof( Equipe::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Membro da Equipe criado com sucesso.');
			return Redirect::route('painel.equipe.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Membro da Equipe!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.equipe.edit')->with('registro', Equipe::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Equipe::find($id);

		$object->titulo_pt = Input::get('titulo_pt');
		$object->divisao = Input::get('divisao');
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');
		
		$imagem = Thumb::make('imagem', 197, 226, 'equipe/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Membro da Equipe alterado com sucesso.');
			return Redirect::route('painel.equipe.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Membro da Equipe!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Equipe::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Membro da Equipe removido com sucesso.');

		return Redirect::route('painel.equipe.index');
	}

}