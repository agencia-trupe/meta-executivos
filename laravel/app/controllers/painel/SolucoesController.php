<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, Solucao;

class SolucoesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '10';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Redirect::back();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::back();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Redirect::back();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.solucoes.edit')->with('registro', Solucao::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Solucao::find($id);

		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');
		$object->slug_pt = Str::slug($object->titulo_pt, '_');
		$object->slug_en = Str::slug($object->titulo_en, '_');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			$rota = ($object->id == 1) ? 'painel.solucoesexec.index' : 'painel.solucoesdev.index';
			return Redirect::route($rota);

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Redirect::back();
	}

}