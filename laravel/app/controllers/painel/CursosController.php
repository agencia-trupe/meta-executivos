<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, Curso;

class CursosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.cursos.index')->with('registros', Curso::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.cursos.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Curso;

		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Curso criado com sucesso.');
			return Redirect::route('painel.cursos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Curso!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.cursos.edit')->with('registro', Curso::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Curso::find($id);

		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Curso alterado com sucesso.');
			return Redirect::route('painel.cursos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Curso!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Curso::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Curso removido com sucesso.');

		return Redirect::route('painel.cursos.index');
	}

}