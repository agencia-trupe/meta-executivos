<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, Solucao, SolucoesDev;

class SolucoesDevController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = '3';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.solucoesdev.index')->with('registros', SolucoesDev::orderBy('ordem', 'ASC')->get())
																		->with('limiteInsercao', $this->limiteInsercao)
																		->with('solucao', Solucao::findOrFail(2));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.solucoesdev.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new SolucoesDev;

		$object->titulo_pt = Input::get('titulo_pt');
		$object->slug_pt = Str::slug(Input::get('titulo_pt'));
		$object->titulo_en = Input::get('titulo_en');
		$object->slug_en = Str::slug(Input::get('titulo_en'));
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');

		if($this->limiteInsercao && sizeof( SolucoesDev::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));
		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.solucoesdev.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));	

		}
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.solucoesdev.edit')->with('registro', SolucoesDev::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = SolucoesDev::find($id);

		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');
		$object->slug_pt = Str::slug($object->titulo_pt);
		$object->slug_en = Str::slug($object->titulo_en);

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.solucoesdev.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = SolucoesDev::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.solucoesdev.index');
	}

}