<?php

use \Solucao, \SolucoesExec, \SolucoesDev, \Curso;

class SolucoesDevController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.solucoes.index')->with('texto', Solucao::find(2))
																			  ->with('listaExec', SolucoesExec::orderBy('ordem', 'ASC')->get())
																			  ->with('listaDev', SolucoesDev::orderBy('ordem', 'ASC')->get())
																			  ->with('secao', 'dev')
																			  ->with('marcar', '');
	}

	public function detalhes($slug)
	{
		$cursos = Curso::orderBy('ordem', 'asc')->get();
		$this->layout->content = View::make('frontend.solucoes.index')->with('texto', SolucoesDev::where('slug_pt', '=', $slug)->first())
																			  ->with('listaExec', SolucoesExec::orderBy('ordem', 'ASC')->get())
																			  ->with('listaDev', SolucoesDev::orderBy('ordem', 'ASC')->get())
																			  ->with('secao', 'dev')
																			  ->with('marcar', $slug)
																			  ->with('cursos', $cursos);
	}
}
