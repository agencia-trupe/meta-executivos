<?php

class SolucoesDevSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
                'titulo_pt' => 'Assessment Center',
				'slug_pt' => 'assessment_center',
                'titulo_en' => 'Assessment Center',
				'slug_en' => 'assessment_center',
                'texto_pt' => '<p>Texto em Português</p>',
                'texto_en' => '<p>Texto em Inglês</p>'
            ),
            array(
                'titulo_pt' => 'Coaching',
                'slug_pt' => 'coaching',
                'titulo_en' => 'Coaching',
                'slug_en' => 'coaching',
                'texto_pt' => '<p>Texto em Português</p>',
                'texto_en' => '<p>Texto em Inglês</p>'
            ),
            array(
                'titulo_pt' => 'Cursos e Workshops',
                'slug_pt' => 'cursos_e_workshops',
                'titulo_en' => 'Cursos e Workshops',
                'slug_en' => 'cursos_e_workshops',
                'texto_pt' => '<p>Texto em Português</p>',
                'texto_en' => '<p>Texto em Inglês</p>'
            )
        );

        DB::table('solucoes_desenvolvimento')->insert($data);
    }

}
