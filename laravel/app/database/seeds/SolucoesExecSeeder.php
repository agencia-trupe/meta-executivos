<?php

class SolucoesExecSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
                'titulo_pt' => 'Hunting',
				'slug_pt' => 'hunting',
                'titulo_en' => 'Hunting',
				'slug_en' => 'hunting',
                'texto_pt' => '<p>Texto em Português</p>',
                'texto_en' => '<p>Texto em Inglês</p>'
            ),
            array(
                'titulo_pt' => 'Mapeamento de Mercado',
                'slug_pt' => 'mapeamento_de_mercado',
                'titulo_en' => 'Mapeamento de Mercado',
                'slug_en' => 'mapeamento_de_mercado',
                'texto_pt' => '<p>Texto em Português</p>',
                'texto_en' => '<p>Texto em Inglês</p>'
            )
        );

        DB::table('solucoes_executive_search')->insert($data);
    }

}
