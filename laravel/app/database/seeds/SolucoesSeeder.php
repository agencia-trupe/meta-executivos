<?php

class SolucoesSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
                'titulo_pt' => 'Executive Search',
				'titulo_en' => 'Executive Search',
				'texto_pt' => '<p>Texto em Português</p>',
                'texto_en' => '<p>Texto em Inglês</p>'
            ),
            array(
                'titulo_pt' => 'Desenvolvimento Organizacional',
                'titulo_en' => 'Desenvolvimento Organizacional',
                'texto_pt' => '<p>Texto em Português</p>',
                'texto_en' => '<p>Texto em Inglês</p>'
            )
        );

        DB::table('solucoes')->insert($data);
    }

}
