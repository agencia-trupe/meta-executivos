<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOportunidadesSoftdeleteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oportunidades', function(Blueprint $table)
		{
			$table->dropColumn('arquivado');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oportunidades', function(Blueprint $table)
		{
			$table->dropColumn('deleted_at');
		});
	}

}
