<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOportunidadesRemoveInscTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oportunidades', function(Blueprint $table)
		{
			$table->dropColumn('inscricoes_abertas');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oportunidades', function(Blueprint $table)
		{
			$table->integer('inscricoes_abertas')->after('data_expiracao');
		});
	}

}
