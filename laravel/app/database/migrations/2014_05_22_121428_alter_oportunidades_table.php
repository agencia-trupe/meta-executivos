<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOportunidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oportunidades', function(Blueprint $table)
		{
			$table->integer('arquivado')->default('0')->after('idioma');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oportunidades', function(Blueprint $table)
		{
			$table->dropColumn('arquivado');
		});
	}

}
