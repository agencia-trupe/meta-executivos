<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOportunidadesSlugTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('oportunidades', function(Blueprint $table)
		{
			$table->string('slug')->after('titulo');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oportunidades', function(Blueprint $table)
		{
			$table->dropColumn('slug');
		});
	}

}
