<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDepEmpresaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('novosite_depoimentos', function(Blueprint $table)
		{
			$table->string('empresa')->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('novosite_depoimentos', function(Blueprint $table)
		{
			$table->dropColumn('empresa');
		});
	}

}
