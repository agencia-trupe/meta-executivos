<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolucoesExecutiveSearchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('solucoes_executive_search', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo_pt');
			$table->string('slug_pt');
			$table->string('titulo_en');
			$table->string('slug_en');
			$table->text('texto_pt');
			$table->text('texto_en');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('solucoes_executive_search');
	}

}
