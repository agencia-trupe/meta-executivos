<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOportunidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('oportunidades', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('empresa');
			$table->string('titulo');
			$table->string('n_posicoes');
			$table->string('cidade');
			$table->string('estado');
			$table->date('data_cadastro');
			$table->date('data_expiracao')->nullable();
			$table->integer('inscricoes_abertas');
			$table->text('texto');
			$table->string('idioma');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('oportunidades');
	}

}
