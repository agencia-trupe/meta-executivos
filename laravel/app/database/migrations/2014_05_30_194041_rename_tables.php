<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		 Schema::rename('banners', 'novosite_banners');
		 Schema::rename('candidatos', 'novosite_candidatos');
		 Schema::rename('cases', 'novosite_cases');
		 Schema::rename('contato', 'novosite_contato');
		 Schema::rename('cursos', 'novosite_cursos');
		 Schema::rename('depoimentos', 'novosite_depoimentos');
		 Schema::rename('diferenciais', 'novosite_diferenciais');
		 Schema::rename('equipe', 'novosite_equipe');
		 Schema::rename('oportunidades', 'novosite_oportunidades');
		 Schema::rename('quem_somos', 'novosite_quem_somos');
		 Schema::rename('solucoes', 'novosite_solucoes');
		 Schema::rename('solucoes_desenvolvimento', 'novosite_solucoes_desenvolvimento');
		 Schema::rename('solucoes_executive_search', 'novosite_solucoes_executive_search');
		 Schema::rename('usuarios', 'novosite_usuarios');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		 Schema::rename('novosite_banners', 'banners');
		 Schema::rename('novosite_candidatos', 'candidatos');
		 Schema::rename('novosite_cases', 'cases');
		 Schema::rename('novosite_contato', 'contato');
		 Schema::rename('novosite_cursos', 'cursos');
		 Schema::rename('novosite_depoimentos', 'depoimentos');
		 Schema::rename('novosite_diferenciais', 'diferenciais');
		 Schema::rename('novosite_equipe', 'equipe');
		 Schema::rename('novosite_oportunidades', 'oportunidades');
		 Schema::rename('novosite_quem_somos', 'quem_somos');
		 Schema::rename('novosite_solucoes', 'solucoes');
		 Schema::rename('novosite_solucoes_desenvolvimento', 'solucoes_desenvolvimento');
		 Schema::rename('novosite_solucoes_executive_search', 'solucoes_executive_search');
		 Schema::rename('novosite_usuarios', 'usuarios');
	}

}
