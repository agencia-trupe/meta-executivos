<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSolucoesExecTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('solucoes_executive_search', function(Blueprint $table)
		{
			$table->integer('ordem');
			$table->string('imagem');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('solucoes_executive_search', function(Blueprint $table)
		{
			$table->dropColumn('ordem');
			$table->dropColumn('imagem');
		});
	}

}
