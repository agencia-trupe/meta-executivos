<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('equipe', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo_pt');
			$table->string('titulo_en');
			$table->string('imagem');
			$table->string('divisao');
			$table->text('texto_pt');
			$table->text('texto_en');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('equipe');
	}

}
