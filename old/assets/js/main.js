//Retira a margem direita do último elemento do widget de notícias - home/todas
$(".clipping:nth-child(3)").css({'margin-right' : '0'});

//Prepara a requisição ajax do tipo POST para enviar os dados do formulário de
//Newsletters - home
$(function() {
  $('#newslettersalva').click(function() {
    var form_data = {
      nome : $('.nome').val(),
      email : $('.email').val(),
      vagas : $('#vagas').val(),
      novidades : $('#novidades-news').val(),
      eventos : $('#eventos').val(),
      pesquisas : $('#resultados').val(),
      noticias: $('#noticias').val(),
      ajax : '1'
    };
  	$.ajax({
    	url: "http://metaexecutivos.com.br/newsletters/ajax_check",
    	type: 'POST',
    	async : false,
    	data: form_data,
    	success: function(msg) {
      	alert(msg);
    	}
  	});
  	return false;
  });
});

//Jqery Accordion - empresas
$(function() {
  $("#accordion").accordion({ autoHeight: false });
});

//Verifica o suporte ao HTML Placeholder - home/contato
jQuery(function() {
  jQuery.support.placeholder = false;
  test = document.createElement('input');
  if('placeholder' in test) jQuery.support.placeholder = true;
});

//Hack para navegadores que não oferecem suporte ao HTML5 Placeholder - home/contato
$(function() {
  if(!$.support.placeholder) { 
    var active = document.activeElement;
    $(':text').focus(function () {
      if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
        $(this).val('').removeClass('hasPlaceholder');
      }
    }).blur(function () {
      if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
        $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
      }
    });
    $(':text').blur();
    $(active).focus();
    $('form').submit(function () {
      $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
    });
  }
});



//Prepara a requisição ajax do tipo POST para enviar os dados do formulário de
//contato - contato
$(function() {
  $('#submit').click(function() {
    $('#ajax-loader').show(); 
    var form_data = {
      nome : $('.nome').val(),
      email : $('.email').val(),
      telefone : $('.telefone').val(),
      mensagem : $('.mensagem').val(),
       ajax : '1'
    };
    $.ajax({
      url: "http://metaexecutivos.com.br/contato/ajax_check",
      type: 'POST',
      async : false,
      data: form_data,
      success: function(msg) {
        $('#message').html(msg);
      }
    });
    return false;
  });
});

//Esconde o gif de ajax loader cada vez qe um arequisição ajax é completada - contato
jQuery(function() {
  $("#ajax-loader").ajaxComplete(function() {
    $(this).hide();
  });
});

$(function() {
  $('#header-image').before('<div id="nav-header">').cycle({
    pager: '#nav-header',
    manualTrump: false,
  })
});


//Serviços - Desenvolvimento
$(function(){
  $(".cursos-interna").hide();
  $(".cursos-titulo").click(function(event){
    event.preventDefault(event);
    var atual = $(this).next(".cursos-interna");
    $(".cursos-interna").not(atual).slideUp("normal").prev().css("background-color","#E2EAF0").css("color","#2c425e");
    $(this).next(".cursos-interna").slideToggle("normal", function(){
      if($(this).is(":visible")){
        $(this).prev().css("background-color","#28689A").css("color","#FFF");
      }else{
        $(this).prev().css("background-color","#E2EAF0").css("color","#2c425e");
      }
    });
  });
});