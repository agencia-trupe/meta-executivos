<?php
//no direct accessdefined('_JEXEC') or die('Restricted access');
?>
<script type="text/javascript">
<?php
if ($_POST['nome'] && $_POST['area'] && $_POST['cargo'] && $_POST['descricao']) {

	require_once('modules/mod_cadastrocurriculo/phpmailer/class.phpmailer.php');
	$mail                = new PHPMailer();
	$mail->SetFrom('naoresponda@metaexecutivos.com.br', 'Meta Executivos');
	$mail->AddReplyTo(trim($params->get('sendto')), 'Curriculo');
	$mail->AddAddress(trim($params->get('sendto')));
	$mail->AddAddress('curriculo.metaexec@gmail.com');

	$mail->Subject       = utf8_decode('Meta Executivos - Cadastro de Currículo');
	
	$file = 'modules/mod_cadastrocurriculo/curriculos/' . md5(time()) . '.' . end(explode('.', $_FILES['file']['name']));
	move_uploaded_file($_FILES['file']['tmp_name'], $file);	
	$mail->AddAttachment($file);
	$mail->IsHTML(true);
	
	$mensagem = '<b>Nome:</b> ' . $_POST['nome'] . '<br />';		
	$mensagem .= '<b>Área:</b> ' . $_POST['area'] . '<br />';		
	$mensagem .= '<b>Cargo:</b> ' . $_POST['cargo'] . '<br />';		
	$mensagem .= '<b>Telefone:</b> ' . $_POST['tel'] . '<br />';		
	$mensagem .= '<b>E-mail:</b> ' . $_POST['email'] . '<br />';		
	$mensagem .= '<b>Descrição:</b> ' . nl2br($_POST['descricao']) . '<br />';		
	$mail->MsgHTML($mensagem);
	
	if ($mail->Send()) {		
		@unlink($file);		
		echo "alert('Currículo enviado com sucesso!'); window.location = '';";		
	}
}
?>
function validateCurriculo() {
	var form = document.formCadastroCurriculo;
	if (form.nome.value == '' || form.area.value == '' || form.cargo.value == '' || form.file.value == '' || form.descricao.value == '') {
		alert('Preencha todos os campos!');
		return false;
	}

	return true;
}</script>

<div class="cadastro-curriculo content-box">
	<h2>Candidatos</h2>
	<div class="readmore" onclick="window.location = 'candidatos.php'; return false;"><a href="candidatos.php" title="Candidatos"></a></div>
	<div class="block">
		<div><img src="images/banners/candidatos.jpg" /></div>
		<div class="body">
			<form action="" method="post" name="formCadastroCurriculo" onsubmit="return validateCurriculo()" enctype="multipart/form-data">
			<h3>Cadastre seu Currículo</h3>
			<p>Cadastre seu currículo e faça parte do nosso precioso banco de dados.</p>
			
			<table width="100%" cellspacing="0">
				<tr>
					<th><label for="nome">Nome:</label></th>
					<td><input type="text" name="nome" id="nome" maxlength="120" /></td>
				</tr>
				<tr>
					<th><label for="area">Área:</label></th>
					<td><input type="text" name="area" id="area" maxlength="120" /></td>
				</tr>
				<tr>
					<th><label for="cargo">Cargo:</label></th>
					<td><input type="text" name="cargo" id="cargo" maxlength="120" /></td>
				</tr>
				<tr>
					<th><label for="tel">Telefone:</label></th>
					<td><input type="text" name="tel" id="tel" maxlength="14" /></td>
				</tr>
				<tr>
					<th><label for="email">E-mail:</label></th>
					<td><input type="text" name="email" id="email" maxlength="254" /></td>
				</tr>
				<tr>
					<th><label for="file">Currículo:</label></th>
					<td><input type="file" name="file" id="file" size="6" /></td>
				</tr>
				<tr>
					<th valign="top"><label for="descricao">Descrição:</label></th>
					<td valign="top"><textarea name="descricao" id="descricao"></textarea></td>
				</tr>
			</table>
			<div class="submit"><input type="submit" name="submit" id="submit" value="Enviar" /></div>
			</form>
		</div>
	</div>
</div>