<?php
/**
 * @version 1.4 $Id: mod_rsflashion.php
 * @package Joomla 1.5.x
 * @subpackage RS-Flashion flash image slideshow module.
 * @copyright (C) 2010-2015 RS Web Solutions (http://www.rswebsols.com)
 * @license GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

if(!function_exists('rsws_sql_regcase')) {
	function rsws_sql_regcase($rsws_string, $rsws_encoding='auto') {
		$rsws_max = mb_strlen($rsws_string, $rsws_encoding);
		for($rsws_i=0; $rsws_i<$rsws_max; $rsws_i++) {
			$rsws_char = mb_substr($rsws_string, $rsws_i, 1, $rsws_encoding);
			$rsws_up = mb_strtoupper($rsws_char, $rsws_encoding);
			$rsws_low = mb_strtolower($rsws_char, $rsws_encoding);
			$rsws_ret .= ($rsws_up!=$rsws_low) ? '['.$rsws_up.$rsws_low.']' : $rsws_char;
	  	}
	  	return $rsws_ret;
	}
}

$rsws_insertSWFOBJECT = $params->get('rsws_insertSWFOBJECT', 1);
$rsws_width = $params->get('rsws_width', 600);
$rsws_height = $params->get('rsws_height', 300);
$rsws_barHeight = $params->get('rsws_barHeight', 25);
$rsws_bar_color = $params->get('rsws_bar_color', 'cccccc');
$rsws_controllerTop = ($params->get('rsws_controllerTop', '1') == '1' ? 'false' : 'true');
$rsws_bar_transparency = $params->get('rsws_bar_transparency', 70);
$rsws_transition = $params->get('rsws_transition', 0);
$rsws_transitionSpeed = $params->get('rsws_transitionSpeed', 1);
$rsws_slideShowTime = $params->get('rsws_slideShowTime', 3);
$rsws_imageshow = $params->get('rsws_imageshow', 1);

if($rsws_transition == '0') {
	$rsws_f_transition = 'true';
	$rsws_v_transition = 'false';
} elseif($rsws_transition == '1') {
	$rsws_f_transition = 'false';
	$rsws_v_transition = 'false';
} elseif($rsws_transition == '2') {
	$rsws_f_transition = 'false';
	$rsws_v_transition = 'true';
}

// Set FTP credentials, if given
$rsws_module_path = JPATH_BASE.DS.'modules'.DS.'mod_rsflashion'.DS;
jimport('joomla.client.helper');
JClientHelper::setCredentialsFromRequest('ftp');
$ftp = JClientHelper::getCredentials('ftp');

$rsws_file = $rsws_module_path.'xml'.DS.'rsflashion_'.$module->id.'.xml';

$rsws_txt = '<?xml version="1.0" encoding="UTF-8"?><banner width="'.$rsws_width.'" height="'.$rsws_height.'" backgroundColor="0xffffff" backgroundTransparency="100" startWith="1" barHeight="'.$rsws_barHeight.'" fadeTransition="'.$rsws_f_transition.'" verticalTransition="'.$rsws_v_transition.'" controllerTop="'.$rsws_controllerTop.'" transitionSpeed="'.$rsws_transitionSpeed.'" titleX="0" titleY="0"><items>';

if($rsws_imageshow == '1') {
	$rsws_basic_folder = $params->get('rsws_basic_folder', 'images/stories/slideshow/');
	$rsws_basic_url = $params->get('rsws_basic_url', '');
	$rsws_basic_url = str_replace('&amp;', '&', $rsws_basic_url);
	$rsws_basic_url = str_replace('&', '&amp;', $rsws_basic_url);
	$rsws_basic_url_target = $params->get('rsws_basic_url_target', '_self');
	
	$rsws_jpgimages = glob("".$rsws_basic_folder.rsws_sql_regcase("*.jpg"));
	$rsws_jpegimages = glob("".$rsws_basic_folder.rsws_sql_regcase("*.jpeg"));
	$rsws_pngimages = glob("".$rsws_basic_folder.rsws_sql_regcase("*.png"));
	$rsws_gifimages = glob("".$rsws_basic_folder.rsws_sql_regcase("*.gif"));
	
	$rsws_image = $rsws_jpgimages;
	
	$j=0;
	for($i=count($rsws_jpgimages);$i<count($rsws_jpgimages)+count($rsws_jpegimages);$i++) {
		$rsws_image[$i]=$rsws_jpegimages[$j];
		$j=$j+1;
	}
	
	$j=0;
	for($i=count($rsws_jpgimages);$i<count($rsws_jpgimages)+count($rsws_pngimages);$i++) {
		$rsws_image[$i]=$rsws_pngimages[$j];
		$j=$j+1;
	}
	
	$j=0;
	for($i=count($rsws_image);$i<count($rsws_jpgimages)+count($rsws_pngimages)+count($rsws_gifimages);$i++) {
		$rsws_image[$i]=$rsws_gifimages[$j];
		$j=$j+1;
	}
	for($i=0;$i<count($rsws_image);$i++) {
		if(file_exists($rsws_image[$i])) {
			$rsws_txt .= '<item><title></title><path>'.JURI::root().$rsws_image[$i].'</path><url>'.$rsws_basic_url.'</url><target>'.$rsws_basic_url_target.'</target><bar_color>0x'.$rsws_bar_color.'</bar_color><bar_transparency>'.$rsws_bar_transparency.'</bar_transparency><slideShowTime>'.$rsws_slideShowTime.'</slideShowTime></item>';
		}
	}
} else {
	$rsws_adv_images = $params->get('rsws_adv_images', '');
	$rsws_adv_urls = $params->get('rsws_adv_urls', '');
	$rsws_adv_urls_target = $params->get('rsws_adv_urls_target', '');
	$rsws_adv_bar_color = $params->get('rsws_adv_bar_color', '');
	$rsws_adv_bar_transparency = $params->get('rsws_adv_bar_transparency', '');
	$rsws_adv_slideshow_time = $params->get('rsws_adv_slideshow_time', '');
	
	$rsws_all_images = explode(',', $rsws_adv_images);
	$rsws_all_urls = explode(',', $rsws_adv_urls);
	$rsws_all_urls_target = explode(',', $rsws_adv_urls_target);
	$rsws_all_bar_color = explode(',', $rsws_adv_bar_color);
	$rsws_all_bar_transparency = explode(',', $rsws_adv_bar_transparency);
	$rsws_all_slideshow_time = explode(',', $rsws_adv_slideshow_time);
	for($i=0;$i<count($rsws_all_images);$i++) {
		$rsws_temp_image = trim($rsws_all_images[$i]);
		$rsws_temp_url = trim($rsws_all_urls[$i]);
		$rsws_temp_target = trim($rsws_all_urls_target[$i]);
		$rsws_temp_bar_color = trim($rsws_all_bar_color[$i]);
		$rsws_temp_bar_transparency = trim($rsws_all_bar_transparency[$i]);
		$rsws_temp_slideshow_time = trim($rsws_all_slideshow_time[$i]);
		if(file_exists($rsws_temp_image)) {
			$rsws_temp_image = JURI::root().$rsws_temp_image;
			
			$rsws_temp_url = str_replace('&amp;', '&', $rsws_temp_url);
			$rsws_temp_url = str_replace('&', '&amp;', $rsws_temp_url);
			
			if(($rsws_temp_target == '_blank') || ($rsws_temp_target == '_self')) {
				$rsws_temp_target = $rsws_temp_target;
			} else {
				$rsws_temp_target = '_blank';
			}
			if(strlen($rsws_temp_bar_color) == '6') {
				$rsws_temp_bar_color = $rsws_temp_bar_color;
			} else {
				$rsws_temp_bar_color = $rsws_bar_color;
			}
			if(($rsws_temp_bar_transparency >=0) && ($rsws_temp_bar_transparency<=100)) {
				$rsws_temp_bar_transparency = $rsws_temp_bar_transparency;			
			} else {
				$rsws_temp_bar_transparency = $rsws_bar_transparency;
			}
			if($rsws_temp_slideshow_time) {
				$rsws_temp_slideshow_time = $rsws_temp_slideshow_time;
			} else {
				$rsws_temp_slideshow_time = $rsws_slideShowTime;
			}
			$rsws_txt .= '<item><title></title><path>'.$rsws_temp_image.'</path><url>'.$rsws_temp_url.'</url><target>'.$rsws_temp_target.'</target><bar_color>0x'.$rsws_temp_bar_color.'</bar_color><bar_transparency>'.$rsws_temp_bar_transparency.'</bar_transparency><slideShowTime>'.$rsws_temp_slideshow_time.'</slideShowTime></item>';
		}
	}
}

$rsws_txt .= '</items></banner>';

jimport('joomla.filesystem.file');
//if (JFile::exists($rssn_file)) {
	// Try to make the params file writeable
	if (!$ftp['enabled'] && JPath::isOwner($rsws_file) && !JPath::setPermissions($rsws_file, '0755')) {
		JError::raiseNotice('SOME_ERROR_CODE', JText::_('Could not make the file writable'));
	}

	JFile::write($rsws_file, $rsws_txt);

	// Try to make the params file unwriteable
	if (!$ftp['enabled'] && JPath::isOwner($rsws_file) && !JPath::setPermissions($rsws_file, '0555')) {
		JError::raiseNotice('SOME_ERROR_CODE', JText::_('Could not make the file unwritable'));
	}
//}
$rsws_document	=& JFactory::getDocument();

if($rsws_insertSWFOBJECT == '1') {
	$rsws_document->addScript( JURI::root().'modules/mod_rsflashion/js/swfobject.js');
}

$rsws_js_controller = 'var cacheBuster = "?t=" + Date.parse(new Date()); var stageW = "'.$rsws_width.'"; var stageH = "'.$rsws_height.'"; var attributes = {}; attributes.id = \'RSFlashionModule'.$module->id.'\'; attributes.name = attributes.id; var params = {}; params.wmode = "transparent"; params.bgcolor = "#ffffff"; var flashvars = {}; flashvars.componentWidth = stageW; flashvars.componentHeight = stageH; flashvars.pathToFiles = ""; flashvars.xmlPath = "'.JURI::root().'modules/mod_rsflashion/xml/rsflashion_'.$module->id.'.xml"; swfobject.embedSWF("'.JURI::root().'modules/mod_rsflashion/swf/preview.swf"+cacheBuster, attributes.id, stageW, stageH, "9.0.124", "'.JURI::root().'modules/mod_rsflashion/swf/expressInstall.swf", flashvars, params);';

$rsws_document->addScriptDeclaration($rsws_js_controller);
?>
<div id="RSFlashionModule<?php echo $module->id; ?>"><p>In order to view this object you need Flash Player 9+ support!</p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/></a><p><?php echo base64_decode('UG93ZXJlZCBieQ==').' <a href="'.base64_decode('aHR0cDovL3d3dy5yc3dlYnNvbHMuY29t').'" target="_blank">'.base64_decode('UlMgV2ViIFNvbHV0aW9ucw==').'</a>'; ?></p></div>