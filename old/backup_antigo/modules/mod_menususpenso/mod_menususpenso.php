<?php
defined('_JEXEC') or die('Restricted access');
//$params->get('menutype');

echo '
<script type="text/javascript">
	function showItem(level, line) {
		try {
			document.getElementById(\'sub\' + line + \'Item\' + level).style.display = \'\'; 
		} catch (error) {}
	}
	
	function hideItem(level, line) {
		try {
			document.getElementById(\'sub\' + line + \'Item\' + level).style.display = \'none\';
		} catch (error) {}
	}
</script>
';

$db	=& JFactory::getDBO();

function generateSubMenu($id, $level, $line) {
	$db	=& JFactory::getDBO();

	$sql = "SELECT id, name, link, type, browserNav FROM #__menu WHERE parent = '" . $id . "' AND published = '1' ORDER BY ordering";
	$db->setQuery($sql);
	$rows = $db->loadObjectList();
	
	if (count($rows)) {
		echo '<div class="subitem' . $level . '" style="display: none;" id="sub' . $line . 'Item' . $level . '">';
		
		foreach ($rows as $row) {
			$sql = "SELECT count(*) as c FROM #__menu WHERE parent = '" . $row->id . "' AND published = '1'";
			$db->setQuery($sql);
			$count = $db->loadObjectList();
			$count = $count[0]->c;
			
			echo '<div onmouseover="showItem(' . ($level+1) . ', ' . $line . ')" onmouseout="hideItem(' . ($level+1) . ', ' . $line . ')" class="subitemlevel' . $level;
			if ($count) echo ' bgsubitem';
			echo '">';
			
			$line = generateSubMenu($row->id, ($level+1), $line);
			
			if ($row->type != 'url') $row->link .= '&Itemid=' . $row->id;
			
			if ($level > 1) $row->name = str_replace(' ', '&nbsp;', $row->name);
			
			echo '<a href="' . JRoute::_($row->link) . '" title="' . $row->name . '"';
			if ($row->browserNav != 0) { echo ' target="_blank"'; }
			echo '>' . $row->name . '</a>';

			echo '</div>';
			$line++;
		}

		echo '</div>';
	}
	
	return $line;
}

$sql = "SELECT id, name, link, home, browserNav FROM #__menu WHERE menutype = '" . $params->get('menutype') . "' AND published = '1' AND parent = '0' ORDER BY ordering";
$db->setQuery($sql);
$rows = $db->loadObjectList();
	
echo '<ul class="item">';

$x = 1;
foreach ($rows as $row) {
	echo '<li class="item" onmouseover="showItem(1, ' . $x . ')" onmouseout="hideItem(1, ' . $x . ')">';
	
	$x = generateSubMenu($row->id, 1, $x);
	
	if ($row->home) {
		echo '<a href="./" title="' . $row->name . '"';
			if ($row->browserNav != 0) { echo ' target="_blank"'; }
			echo '>' . $row->name . '</a>';
	} else {
		if ($row->type != 'url') $row->link .= '&Itemid=' . $row->id;
		echo '<a href="' . JRoute::_($row->link) . '" title="' . $row->name . '"';
			if ($row->browserNav != 0) { echo ' target="_blank"'; }
			echo '>' . $row->name . '</a>';
	}
	
	echo '</li>';
	$x++;
}

echo '</ul>';
?>