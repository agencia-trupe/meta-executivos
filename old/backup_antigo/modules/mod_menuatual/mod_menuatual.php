<?php
defined('_JEXEC') or die('Restricted access');

$Itemid = JRequest::getVar('Itemid');

$db	=& JFactory::getDBO();
$sql = "SELECT parent FROM #__menu WHERE id = '" . $Itemid . "'";
$db->setQuery($sql);
$rows = $db->loadObjectList();
$parent = $rows[0]->parent;

if ($parent) {

	$sql = "SELECT id, name, link, type, browserNav FROM #__menu WHERE parent = '" . $parent . "' AND published = '1' ORDER BY ordering";
	$db->setQuery($sql);
	$rows = $db->loadObjectList();

	echo '<ul class="menuatual">';

	$x = 1;
	foreach ($rows as $row) {
		echo '<li';
		if ($row->id == $Itemid) echo ' class="active"';
		echo '>&#0187; ';
	
		if ($row->type != 'url') $row->link .= '&Itemid=' . $row->id;
	
		if ($row->home) {
			echo '<a href="./" title="' . $row->name . '"';
			if ($row->browserNav != 0) { echo ' target="_blank"'; }
			echo '>' . $row->name . '</a>';
		} else {
			echo '<a href="' . JRoute::_($row->link) . '" title="' . $row->name . '"';
			if ($row->browserNav != 0) { echo ' target="_blank"'; }
			echo '>' . $row->name . '</a>';
		}
		echo '</li>';
		$x++;
	}

echo '</ul><div class="clear"></div>';

}
?>