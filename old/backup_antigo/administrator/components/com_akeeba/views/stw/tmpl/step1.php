<?php
/**
 * @package AkeebaBackup
 * @copyright Copyright (c)2006-2011 Nicholas K. Dionysopoulos
 * @license GNU General Public License version 3, or later
 * @version $Id$
 * @since 1.3
 */

// Protect from unauthorized access
defined('_JEXEC') or die('Restricted Access');
?>

<form name="adminForm" action="index.php" method="get">
<input type="hidden" name="option" value="com_akeeba" />
<input type="hidden" name="view" value="stw" />
<input type="hidden" name="task" value="step2" />
<?php echo JHTML::_( 'form.token' ); ?>

<fieldset>
	<legend><?php echo JText::_('STW_LBL_STEP1') ?></legend>
	
	<p><?php echo JText::_('STW_LBL_STEP1_INTRO');?></p>
	
	<?php if($this->stw_profile_id > 0): ?>
	<input type="radio" name="method" value="none" checked="checked">
		<?php echo JText::_('STW_PROFILE_STW') ?>
	</input>
	<br/>
	<?php endif; ?>
	
	<input type="radio" name="method" value="copyfrom">
		<?php echo JText::_('STW_PROFILE_COPYFROM') ?>
		<?php echo JHTML::_('select.genericlist', $this->profilelist, 'oldprofile'); ?>
	</input>
	<br/>
	
	<input type="radio" name="method" value="blank" <?php echo ($this->stw_profile_id > 0) ? '' : 'checked="checked"' ?>>
		<?php echo JText::_('STW_PROFILE_BLANK') ?>
	</input>
	<br/>
	
	<p>
		<input type="submit" value="<?php echo JText::_('STW_LBL_NEXT') ?>" />
	</p>
	
</fieldset>
	
</form>