a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"
	
";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:68:"
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:41:"Joomla! Developer Network - Security News";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:65:"Joomla! - the dynamic portal engine and content management system";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:46:"http://developer.joomla.org/security/news.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 21 Mar 2012 17:32:42 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:40:"Joomla! - Open Source Content Management";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-gb";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:15:{i:0;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"[20120304] - Core - Password Change";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:99:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/JukET1dgfDM/394-20120304-core-password-change.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:80:"http://developer.joomla.org/security/news/394-20120304-core-password-change.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1108:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong> Severity:</strong> High</li>
<li><strong>Versions:</strong> 2.5.2, 2.5.1, 2.5.0, and all 1.7.x and 1.6.x releases</li>
<li><strong>Exploit type:</strong> Password Change</li>
<li><strong>Reported Date:</strong> 2012-March-8</li>
<li><strong>Fixed Date:</strong> 2012-March-15</li>
</ul>
<h2>Description</h2>
<p>Insufficient randomness leads to password reset vulnerability.</p>
<h2>Affected Installs</h2>
<p>Joomla! versions 2.5.2, 2.5.1, 2.5.0, and all 1.7.x and 1.6.x versions</p>
<h2>Solution</h2>
<p>Upgrade to version 2.5.3</p>
<p>Reported by <span id=":1of" dir="ltr">George Argyros and Aggelos Kiayias</span></p>
<h2>Contact</h2>
<p>The JSST at the Joomla! Security Center.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=JukET1dgfDM:r9wKfnwUsCY:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/JukET1dgfDM" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 16 Mar 2012 07:21:02 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:80:"http://developer.joomla.org/security/news/394-20120304-core-password-change.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"[20120303] - Core - Privilege Escalation";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:104:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/n5w8L96w-LM/395-20120303-core-privilege-escalation.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:85:"http://developer.joomla.org/security/news/395-20120303-core-privilege-escalation.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1091:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong> Severity:</strong> High</li>
<li><strong>Versions:</strong> 2.5.2, 2.5.1, 2.5.0, and all 1.7.x and 1.6.x releases</li>
<li><strong>Exploit type:</strong> Privilege Escalation</li>
<li><strong>Reported Date:</strong> 2012-March-12</li>
<li><strong>Fixed Date:</strong> 2012-March-15</li>
</ul>
<h2>Description</h2>
<p>Programming error allows privilege escalation in some cases.</p>
<h2>Affected Installs</h2>
<p>Joomla! versions 2.5.2, 2.5.1, 2.5.0, and all 1.7.x and 1.6.x versions</p>
<h2>Solution</h2>
<p>Upgrade to version 2.5.3</p>
<p>Reported by <span id=":1of" dir="ltr">Jeff Channel</span></p>
<h2>Contact</h2>
<p>The JSST at the Joomla! Security Center.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=n5w8L96w-LM:Wlz_Ez88Wys:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/n5w8L96w-LM" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 15 Mar 2012 12:00:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:85:"http://developer.joomla.org/security/news/395-20120303-core-privilege-escalation.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"[20120301] - Core - SQL Injection";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:97:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/L_dDHx34L4A/391-20120301-core-sql-injection.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:78:"http://developer.joomla.org/security/news/391-20120301-core-sql-injection.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1038:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong> Severity:</strong> High</li>
<li><strong>Versions:</strong> 2.5.1, 2.5.0 and 1.7.0 - 1.7.5</li>
<li><strong>Exploit type:</strong> SQL Injection</li>
<li><strong>Reported Date:</strong> 2012-February-29</li>
<li><strong>Fixed Date:</strong> 2012-March-05</li>
</ul>
<h2>Description</h2>
<p>Inadequate escaping leads to SQL injection vulnerability.</p>
<h2>Affected Installs</h2>
<p>Joomla! version 2.5.1, 2.5.0, 1.7.4, and all earlier 1.7.x versions</p>
<h2>Solution</h2>
<p>Upgrade to version 2.5.2</p>
<p>Reported by Ching Shiong Sow, Stratsec</p>
<h2>Contact</h2>
<p>The JSST at the Joomla! Security Center.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=L_dDHx34L4A:P1U95KcfDuQ:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/L_dDHx34L4A" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 05 Mar 2012 14:00:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:78:"http://developer.joomla.org/security/news/391-20120301-core-sql-injection.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"[20120302] - Core - XSS Vulnerability";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:101:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/hpSgU9ABRDc/392-20120302-core-xss-vulnerability.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:82:"http://developer.joomla.org/security/news/392-20120302-core-xss-vulnerability.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1008:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong> Severity:</strong> Moderate</li>
<li><strong>Versions:</strong> 2.5.1 and 2.5.0</li>
<li><strong>Exploit type:</strong> XSS Vulnerability</li>
<li><strong>Reported Date:</strong> 2012-February-29</li>
<li><strong>Fixed Date:</strong> 2012-March-05</li>
</ul>
<h2>Description</h2>
<p>Inadequate filtering leads to XSS vulnerability.</p>
<h2>Affected Installs</h2>
<p>Joomla! version 2.5.1 and 2.5.0.</p>
<h2>Solution</h2>
<p>Upgrade to version 2.5.2</p>
<p>Reported by <span id=":1of" dir="ltr">Phil Purviance</span></p>
<h2>Contact</h2>
<p>The JSST at the Joomla! Security Center.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=hpSgU9ABRDc:lqQX14gOfys:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/hpSgU9ABRDc" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 05 Mar 2012 14:00:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:82:"http://developer.joomla.org/security/news/392-20120302-core-xss-vulnerability.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"[20120201] - Core - Information Disclosure";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:106:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/PkBR45UJQxo/387-20120201-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/387-20120201-core-information-disclosure.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1054:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong> Severity:</strong> Low</li>
<li><strong>Versions:</strong> 2.5.0 and 1.7.0 - 1.7.4</li>
<li><strong>Exploit type:</strong> Information Disclosure</li>
<li><strong>Reported Date:</strong> 2012-January-29</li>
<li><strong>Fixed Date:</strong> 2012-February-02</li>
</ul>
<h2>Description</h2>
<p>Inadequate validation leads to information disclosure in administrator.</p>
<h2>Affected Installs</h2>
<p>Joomla! version 2.5.0, 1.7.4, and all earlier 1.7.x versions</p>
<h2>Solution</h2>
<p>Upgrade to version 1.7.5 or 2.5.1 or higher</p>
<p>Reported by Jakub Galczyk</p>
<h2>Contact</h2>
<p>The JSST at the Joomla! Security Center.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=PkBR45UJQxo:tozT3WXEdn0:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/PkBR45UJQxo" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 02 Feb 2012 05:25:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/387-20120201-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"[20120202] - Core - Information Disclosure";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:106:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/MFhhodAeXho/388-20120202-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/388-20120202-core-information-disclosure.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1058:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong> Severity:</strong> Moderate</li>
<li><strong>Versions:</strong> 1.7.4 and all earlier 1.7.x versions</li>
<li><strong>Exploit type:</strong> Information Disclosure</li>
<li><strong>Reported Date:</strong> 2012-January-06</li>
<li><strong>Fixed Date:</strong> 2012-February-02</li>
</ul>
<h2>Description</h2>
<p>On some servers the error log could be read by unauthorised users.</p>
<h2>Affected Installs</h2>
<p>Joomla! version 1.7.4 and all earlier 1.7.x versions</p>
<h2>Solution</h2>
<p>Upgrade to version 2.5.1 or 1.7.5 or higher</p>
<p>Reported by Alain Rivest</p>
<h2>Contact</h2>
<p>The JSST at the Joomla! Security Center.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=MFhhodAeXho:TcD6ohzsuCc:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/MFhhodAeXho" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 02 Feb 2012 05:25:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/388-20120202-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"[20120203] - Core - Information Disclosure";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:106:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/LY07jV4Rnvs/389-20120203-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/389-20120203-core-information-disclosure.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1047:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong> Severity:</strong> Low</li>
<li><strong>Versions:</strong> 2.5.0 and 1.7.0 - 1.7.4</li>
<li><strong>Exploit type:</strong> Information Disclosure</li>
<li><strong>Reported Date:</strong> 2012-January-29</li>
<li><strong>Fixed Date:</strong> 2012-February-02</li>
</ul>
<h2>Description</h2>
<p>Inadequate validation leads to path disclosure in administrator.</p>
<h2>Affected Installs</h2>
<p>Joomla! version 2.5.0, 1.7.4, and all earlier 1.7.x versions</p>
<h2>Solution</h2>
<p>Upgrade to version 2.5.1 or 1.7.5 or higher</p>
<p>Reported by Jakub Galczyk</p>
<h2>Contact</h2>
<p>The JSST at the Joomla! Security Center.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=LY07jV4Rnvs:YgvDxlGAUzQ:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/LY07jV4Rnvs" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 02 Feb 2012 05:25:21 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/389-20120203-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"[20120101] - Core - Information Disclosure";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:106:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/MYKnZ2QJKYE/382-20120101-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/382-20120101-core-information-disclosure.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1050:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong> Severity:</strong> Low</li>
<li><strong>Versions:</strong> 1.7.3 and all earlier 1.7 and 1.6 versions</li>
<li><strong>Exploit type:</strong> Information Disclosure</li>
<li><strong>Reported Date:</strong> 2012-January-07</li>
<li><strong>Fixed Date:</strong> 2012-January-24</li>
</ul>
<h2>Description</h2>
<p>Inadequate filtering leads to information disclosure.</p>
<h2>Affected Installs</h2>
<p>Joomla! version 1.7.3 and all earlier versions</p>
<h2>Solution</h2>
<p>Upgrade to version 1.7.4 or 2.5.0 or higher</p>
<p>Reported by Erwan Peton - Intrinsec</p>
<h2>Contact</h2>
<p>The JSST at the Joomla! Security Center.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=MYKnZ2QJKYE:8PTy5GokuuM:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/MYKnZ2QJKYE" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Jan 2012 09:45:28 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/382-20120101-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"[20120102] - Core - XSS Vulnerability";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:101:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/XAEsWEG3dgU/383-20120102-core-xss-vulnerability.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:82:"http://developer.joomla.org/security/news/383-20120102-core-xss-vulnerability.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1036:"<ul>
<li><strong>Project: </strong>Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong>Severity:</strong> Moderate</li>
<li><strong>Versions:</strong> 1.7.3 and all earlier 1.7 and 1.6 versions</li>
<li><strong>Exploit type:</strong> XSS Vulnerability</li>
<li><strong>Reported Date:</strong> 2011-November-16</li>
<li><strong>Fixed Date:</strong> 2012-January-24</li>
</ul>
<h2>Description</h2>
<p>Inadequate filtering leads to XSS vulnerability.</p>
<h2>Affected Installs</h2>
<p>Joomla! version 1.7.3 and all earlier versions</p>
<h2>Solution</h2>
<p>Upgrade to version 1.7.4 or 2.5.0 or higher</p>
<p>Reported by Ankita Kapadia</p>
<h2>Contact</h2>
<p>The JSST at the Joomla! Security Center.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=XAEsWEG3dgU:63AinNntsww:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/XAEsWEG3dgU" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Jan 2012 09:45:28 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:82:"http://developer.joomla.org/security/news/383-20120102-core-xss-vulnerability.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"[20120103] - Core - Information Disclosure";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:106:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/Ed0TMAvyQ4g/384-20120103-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/384-20120103-core-information-disclosure.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1045:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong>Severity:</strong> Low</li>
<li><strong>Versions:</strong> 1.7.3 and all earlier 1.7 and 1.6 versions</li>
<li><strong>Exploit type:</strong> Information Disclosure</li>
<li><strong>Reported Date:</strong> 2011-December-19</li>
<li><strong>Fixed Date:</strong> 2012-January-24</li>
</ul>
<h2>Description</h2>
<p>Inadequate filtering leads to information disclosure.</p>
<h2>Affected Installs</h2>
<p>Joomla! version 1.7.3 and all earlier versions</p>
<h2>Solution</h2>
<p>Upgrade to version 1.7.4 or 2.5.0 or higher</p>
<p>Reported by Jean-Marie Simonet</p>
<h2>Contact</h2>
<p>The JSST at the Joomla! Security Center.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=Ed0TMAvyQ4g:blmC1ASORQc:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/Ed0TMAvyQ4g" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Jan 2012 09:45:28 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/384-20120103-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:10;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"[20120104] - Core - XSS Vulnerability";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:101:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/K4UuOr8BroM/385-20120104-core-xss-vulnerability.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:82:"http://developer.joomla.org/security/news/385-20120104-core-xss-vulnerability.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1034:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong>Severity:</strong> Moderate</li>
<li><strong>Versions:</strong> 1.7.3 and all earlier versions</li>
<li><strong>Exploit type:</strong> XSS Vulnerability</li>
<li><strong>Reported Date:</strong> 2012-January-22</li>
<li><strong>Fixed Date:</strong> 2012-January-24</li>
</ul>
<h2>Description</h2>
<p>Inadequate filtering leads to XSS vulnerability.</p>
<h2>Affected Installs</h2>
<p>Joomla! version 1.7.3 and all earlier 1.7 and 1.6 versions</p>
<h2>Solution</h2>
<p>Upgrade to version 1.7.4 or 2.5.0 or higher</p>
<p>Reported by David Jardin</p>
<h2>Contact</h2>
<p>The JSST at the Joomla! Security Center.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=K4UuOr8BroM:a7HBKupbzlE:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/K4UuOr8BroM" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Jan 2012 09:45:28 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:82:"http://developer.joomla.org/security/news/385-20120104-core-xss-vulnerability.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:11;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"[20111102] - Core - Password Change";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:99:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/JbROZtZZkvQ/374-20111102-core-password-change.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:80:"http://developer.joomla.org/security/news/374-20111102-core-password-change.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1246:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong>Severity:</strong> High</li>
<li><strong>Versions:</strong> 1.7.2 and all 1.6.x versions</li>
<li><strong>Exploit type:</strong> Password Change</li>
<li><strong>Reported Date:</strong> 2011-October-28</li>
<li><strong>Fixed Date:</strong> 2011-November-14</li>
</ul>
<h2>Description</h2>
<p><span id=":1d2" dir="ltr">Weak random number generation during password reset leads to possibility of changing a user's password.<br /></span></p>
<h2>Affected Installs</h2>
<p>Joomla! version 1.7.2 and all earlier 1.7.x and 1.6.x versions</p>
<h2>Solution</h2>
<p>Upgrade to the latest Joomla! version (1.7.3 or later)</p>
<p>Reported by Gregor Kopf and <span>David Jardin</span></p>
<h2>Contact</h2>
<p>The JSST at the <a href="http://developer.joomla.org/security.html" title="Contact the JSST">Joomla! Security Center</a>.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=JbROZtZZkvQ:e4a90cSPPRA:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/JbROZtZZkvQ" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Nov 2011 04:33:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:80:"http://developer.joomla.org/security/news/374-20111102-core-password-change.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:12;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"[20111103] - Core - Password Change";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:99:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/nF-FZ-0jMUM/375-20111103-core-password-change.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:80:"http://developer.joomla.org/security/news/375-20111103-core-password-change.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1247:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong>Severity:</strong> High</li>
<li><strong>Versions:</strong> 1.5.24 and all earlier 1.5 versions</li>
<li><strong>Exploit type:</strong> Password Change</li>
<li><strong>Reported Date:</strong> 2011-October-28</li>
<li><strong>Fixed Date:</strong> 2011-November-14</li>
</ul>
<h2>Description</h2>
<p><span id=":1d2" dir="ltr">Weak random number generation during password reset leads to possibility of changing a user's password.<br /></span></p>
<h2>Affected Installs</h2>
<p>Joomla! version 1.5.24 and all earlier 1.5 versions</p>
<h2>Solution</h2>
<p>Upgrade to the latest Joomla! 1.5 version (1.5.25 or later)</p>
<p>Reported by <span>Gregor Kopf and David Jardin</span></p>
<h2>Contact</h2>
<p>The JSST at the <a href="http://developer.joomla.org/security.html" title="Contact the JSST">Joomla! Security Center</a>.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=nF-FZ-0jMUM:nNhJ-8IavSc:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/nF-FZ-0jMUM" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Nov 2011 04:33:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:80:"http://developer.joomla.org/security/news/375-20111103-core-password-change.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:13;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"[20111101] - Core - XSS Vulnerability";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:101:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/sz1HyAL_294/373-20111101-core-xss-vulnerability.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:82:"http://developer.joomla.org/security/news/373-20111101-core-xss-vulnerability.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1178:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong>Severity:</strong> Medium</li>
<li><strong>Versions:</strong> 1.7.2 and all 1.6.x versions</li>
<li><strong>Exploit type:</strong> XSS</li>
<li><strong>Reported Date:</strong> 2011-October-21</li>
<li><strong>Fixed Date:</strong> 2011-November-14</li>
</ul>
<h2>Description</h2>
<p><span id=":1d2" dir="ltr">Inadequate filtering leads to XSS vulnerability in back end.<br /></span></p>
<h2>Affected Installs</h2>
<p>Joomla! version 1.7.2 and all earlier 1.7.x and 1.6.x versions</p>
<h2>Solution</h2>
<p>Upgrade to the latest Joomla! version (1.7.3 or later)</p>
<p>Reported by <span>Corné Hannema</span></p>
<h2>Contact</h2>
<p>The JSST at the <a href="http://developer.joomla.org/security.html" title="Contact the JSST">Joomla! Security Center</a>.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=sz1HyAL_294:eau9ddWZMWw:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/sz1HyAL_294" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 15 Nov 2011 04:33:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:82:"http://developer.joomla.org/security/news/373-20111101-core-xss-vulnerability.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:14;a:6:{s:4:"data";s:27:"
			
			
			
			
			
			
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:6:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"[20111001] - Core - Information Disclosure";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:106:"http://feeds.joomla.org/~r/JoomlaSecurityNews/~3/_TyaH8ToZ98/370-20111001-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/370-20111001-core-information-disclosure.html";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1130:"<ul>
<li><strong>Project:</strong> Joomla!</li>
<li><strong>SubProject:</strong> All</li>
<li><strong>Severity:</strong> Moderate</li>
<li><strong>Versions:</strong> 1.7.1</li>
<li><strong>Exploit type:</strong> Information Disclosure</li>
<li><strong>Reported Date:</strong> 2011-September-09</li>
<li><strong>Fixed Date:</strong> 2011-October-17</li>
</ul>
<h2>Description</h2>
<p><span id=":1d2" dir="ltr">Weak encryption causes potential information disclosure.<br /></span></p>
<h2>Affected Installs</h2>
<p>Joomla! version 1.7.1 and earlier</p>
<h2>Solution</h2>
<p>Upgrade to the latest Joomla! version (1.7.2 or later)</p>
<p>Reported by Jeff Channell</p>
<h2>Contact</h2>
<p>The JSST at the <a href="http://developer.joomla.org/security.html" title="Contact the JSST">Joomla! Security Center</a>.</p><div class="feedflare">
<a href="http://feeds.joomla.org/~ff/JoomlaSecurityNews?a=_TyaH8ToZ98:Bdw0j-A8nR0:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/JoomlaSecurityNews?d=yIl2AUoC8zA" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/JoomlaSecurityNews/~4/_TyaH8ToZ98" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:36:"dextercowley@gmail.com (Mark Dexter)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 17 Oct 2011 20:59:00 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:87:"http://developer.joomla.org/security/news/370-20111001-core-information-disclosure.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:2:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";s:4:"href";s:42:"http://feeds.joomla.org/JoomlaSecurityNews";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:2:{s:3:"rel";s:3:"hub";s:4:"href";s:32:"http://pubsubhubbub.appspot.com/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:3:{s:4:"info";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:3:"uri";s:18:"joomlasecuritynews";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:14:"emailServiceId";a:1:{i:0;a:5:{s:4:"data";s:18:"JoomlaSecurityNews";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:18:"feedburnerHostname";a:1:{i:0;a:5:{s:4:"data";s:28:"http://feedburner.google.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:11:{s:12:"content-type";s:23:"text/xml; charset=UTF-8";s:4:"etag";s:27:"h1pmeK3jSh+vJwWVJwn5bDTD2uc";s:13:"last-modified";s:29:"Wed, 21 Mar 2012 17:32:47 GMT";s:16:"content-encoding";s:4:"gzip";s:17:"transfer-encoding";s:7:"chunked";s:4:"date";s:29:"Wed, 21 Mar 2012 17:37:33 GMT";s:7:"expires";s:29:"Wed, 21 Mar 2012 17:37:33 GMT";s:13:"cache-control";s:18:"private, max-age=0";s:22:"x-content-type-options";s:7:"nosniff";s:16:"x-xss-protection";s:13:"1; mode=block";s:6:"server";s:3:"GSE";}s:5:"build";s:14:"20090627192103";}