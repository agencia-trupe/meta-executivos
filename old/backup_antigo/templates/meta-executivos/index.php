<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/meta-executivos/css/style.css" type="text/css" />
</head>

<body>
<div class="corpo">
	<div class="topo">
		<a href="" title="Meta Executivos" class="logo">Meta Executivos</a>
		
		<div class="top-position">
			<table width="100%" cellspacing="0">
				<tr><td height="89" align="center"><jdoc:include type="modules" name="top" /></td></tr>
			</table>
		</div>
		
		<div class="clear"></div>
		
		<div class="menu-position"><jdoc:include type="modules" name="menu" /></div>
		
		<div class="clear"></div>
		
		<div class="teaser-position">
			<table width="100%" cellspacing="0">
				<tr><td align="center"><jdoc:include type="modules" name="teaser" /></td></tr>
			</table>
		</div>
		
	</div>
		
	<div class="message-position">
		<jdoc:include type="message" />
	</div>
		
	<div class="left-before-content-position">
		<jdoc:include type="modules" name="left-before-content" />
	</div>
		
	<div class="right-before-content-position">
		<jdoc:include type="modules" name="right-before-content" />
	</div>
		
	<div class="content-position">
		<jdoc:include type="component" />
	</div>
		
	<div class="clear"></div>

	<div class="footer-position">
		© Copyright <a href="http://www.metarh.com.br/" title="Meta RH" target="_blank">Meta RH 2010</a> - Todos os direitos reservados
		<jdoc:include type="modules" name="footer" />
	</div>
</div>
</body>
</html>