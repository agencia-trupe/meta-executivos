<?php

/**
 * Seo Class
 *
 * Add Seo features into CodeIgniter projects.
 *
 * @license     MIT License
 * @package     Codeigniter
 * @subpackage  Libraries
 * @category    Library
 * @author      Nilton Freitas
 * @link        http://niltonfreitas.com/codeigniter-seo-library
 * @version     0.0.1
 * @todo        Docs
 */

Class Seo
{
	//General Site SEO info
	private $site_name;
	private $site_description;
	private $site_tags;

	function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->config('seo', TRUE);
        $this->site_name = $this->ci->config->item('site_name', 'seo');
	}
    /**
     *
     * @param  string $given [description]
     * @return [type]        [description]
     */
    function get_title($given = '')
    {
        $title = ($given == NULL) ? $this->site_name : 
                 $this->site_name . ' &middot; ' . $given;

        return $title;
    }
}

/* End of file datamapper.php */
/* Location: ./application/models/datamapper.php */
