<div class="conteudo-programas">
	<div class="interna">
		<header>
			<h1>Para candidatos</h1>
			<ul>
				<li><a <?php echo ($tipo == 'trainees') ? 'class="active"' : ''; ?> id="trainees" href="<?php echo base_url(); ?>programas/trainees">Trainees</a></li>
				<li><a <?php echo ($tipo == 'estagios') ? 'class="active"' : ''; ?> id="estagios" href="<?php echo base_url(); ?>programas/estagios">Estágios</a></li>
				<li><a <?php echo ($tipo == 'aprendizes') ? 'class="active"' : ''; ?> id="aprendizes" href="<?php echo base_url(); ?>programas/aprendizes">Aprendizes</a></li>
			</ul>
			<div class="clearfix"></div>
		</header>
		<div class="clearfix"></div>
		<div class="banners">
			<?php foreach($result as $programa): ?>
			<a href="<?php echo $programa->programa_link; ?>" title="<?php echo $programa->programa_empresa; ?> - <?php echo $programa->programa_id; ?>">
				<img width="515" height="88" src="<?php echo base_url() . 'assets/img/programas/banners/' . $programa->programa_imagem; ?>" alt="<?php echo $programa->programa_empresa; ?>">
			</a>
			<?php endforeach; ?>
			<?php echo $this->pagination->create_links(); ?>
		</div>			
		<div class="info">
			<?php if($tipo == 'estagios'): ?>
			<p><b>Nossos Programas de Estágio</b></p>
			<p>
				Proporcionam novos caminhos para aquisição de experiência 
				prática dos conhecimentos adquiridos na Graduação.<br>
				Confira os programas em andamento.
			</p>
			<?php elseif($tipo == 'trainees'): ?>
			<p><b>Nossos Programas de Trainee</b></p>
			<p>
				Treinam e desenvolvem jovens talentos com potencial e 
				competências diferenciadas para serem futuros lideres da 
				organização.<br>
				Confira os programas em andamento.
			</p>
			<?php elseif($tipo == 'aprendizes'): ?>
			<p><b>Nossos Programas de Menor Aprendiz</b></p>
			<p>
				Proporcionam oportunidades para jovens nas empresas, com o intuito 
				de capacitar profissionalmente para o ingresso no mercado de 
				trabalho.<br>
				Confira os programas em andamento.
			</p>
		<?php endif; ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>