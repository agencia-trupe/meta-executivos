<?php

class User extends DataMapper
{
    var $table = 'users';
    var $has_one = array("perfil");
    var $auto_populate_has_one = TRUE;
    
    function get_user($user_id)
    {
        $user = new User();
        $user->where('id', $user_id)->limit(1)->get();
        
        return $user;
    }
    
    function get_users($limit = NULL, $offset = NULL)
    {
        $u = new User();
        $u->order_by('last_login', 'desc')->get($limit, $offset);
        
        $arr = array();
        foreach($u->all as $user)
        {
            $arr[] = $user;
        }
        
        return $arr;
    }
    function ban_user($id, $reason)
    {
        $user = new User();
        $user->where('id', $id);
        $update = $user->update(array(
            'banned' => '1',
            'ban_reason' => $reason
        ));
        
        return $update;
    }
    function rehab_user($id)
    {
        $user = new User();
        $user->where('id', $id);
        $update = $user->update(array(
            'banned' => '0',
        ));
        
        return $update;
    }
}

/* End of file file.php */
/* Location: ./application/moules/module/controllers/controller.php */