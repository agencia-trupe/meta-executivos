<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* news
*
* Model responsável pelas regras de negócio dos newss de estágio, trainee e aprendiz
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class News extends Datamapper{
    var $table = 'newsletters';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    /**
     * Retorna os newss ativos em ordem crescente de antiguidade
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de newss ativos
     */
    function get_all($tipo, $limit, $offset)
    {
        $news = new news();
        $news->where('news_categoria', $tipo);
        $news->order_by('news_data_expiracao', 'asc');
        $news->get($limit, $offset);

        $arr = array();
        foreach($news->all as $newss)
        {
            $arr[] = $newss;
        }

        return $arr;

    }


    function cadastra($dados){

        $news = new news();

        $news->newsletters_nome = $dados['nome'];
        $news->newsletters_email = $dados['email'];
        $news->newsletters_vagas = $dados['vagas'];
        $news->newsletters_novidades = $dados['novidades'];
        $news->newsletters_eventos = $dados['eventos'];
        $news->newsletters_resultados = $dados['resultados'];
        $news->newsletters_noticias = $dados['noticias'];
        $news->newsletters_data_cadastro = time();

        if($news->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }

    }
}