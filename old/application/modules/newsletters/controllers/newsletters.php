<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Contato
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class Newsletters extends MX_Controller
{
        /**
         * Exibe o formulário de contato
         */
        public function index()
        {
                $data['titulo'] = 'Meta Executivos &middot; Contato';
                $data['pagina'] = 'contato';
                $data['conteudo_principal'] = "contato/index";
                $this->load->view('layout/main', $data);
        }

        function ajax_check()
        {
                $this->load->library('form_validation');
                $this->load->library('typography');

                if($this->input->post('ajax') == '1')
                {
                        $this->form_validation->set_rules('nome', 'nome',
                            'required|xss_clean|max_length[255]');
                        $this->form_validation->set_rules('email', 'email',
                            'trim|valid_email|required|xss_clean');
                        $this->form_validation->set_error_delimiters('', '');

                        if($this->form_validation->run() == FALSE)
                        {
                                echo validation_errors();
                        }
                        else
                        {
                            $this->load->model('news');

                            $dados = array(
                                'nome' => $this->input->post('nome'),
                                'email' => $this->input->post('email'),
                                'vagas' => $this->input->post('vagas'),
                                'novidades' => $this->input->post('novidades'),
                                'eventos' => $this->input->post('eventos'),
                                'resultados' => $this->input->post('pesquisas'),
                                'noticias' => $this->input->post('noticias'),
                            );

                           if($this->news->cadastra($dados))
                           {
                                echo 'Cadastro efetuado com sucesso!';
                           }
                           else
                           {
                                echo 'Houve um erro ao efetuar seu cadastro,
                                por favor, tente novamente.';
                           }
                        }
                }
        }
}

