<div id="direita">
    <div id="curso">
        <img src="<?php echo base_url(); ?>assets/img/featured/unique5.png"/>
        <header>
            <h3>Curso</h3>
            <h1>Etiqueta Profissional Lorem Ipsum Dolor Sic Amec Lorem Ipsum</h1>
            
                
        </header>
        <div class="clearfix"></div>
        <div class="curso-actions">
            <div class="btn-group">
                <button class="btn btn-small btn-primary dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-pencil"></i> Inscreva-se <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <?php foreach($turmas as $turma): ?>
                        <li><a href="<?php echo base_url() . 'itens/inscricao/' . $turma->id; ?>"><?php echo $turma->local . ' - ' . $turma->data; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div><!-- /btn-group -->
            <div class="btn-group">
                <button class="btn btn-small btn-info dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-share"></i> Compartilhe <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Linkedin</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Envie por email</a></li>
                </ul>
            </div><!-- /btn-group -->
            <div class="clearfix"></div>
        </div>
        <div id="curso-content">
            <dl>
                <dt><i class="icon-calendar"></i> <span>Próximas turmas</span></dt>
                <dd><ul>
                        <?php foreach($turmas as $turma): ?>
                        <li><?php echo $turma->local . ' - ' . $turma->data; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </dd>
                
                
                <dt><i class="icon-time"></i> Carga Horária</dt>
                <dd><?php echo $item->horas; ?> horas</dd>
                
                <dt><i class="icon-screenshot"></i> Objetivo</dt>
                <dd> 
                    <?php echo $item->objetivos; ?>
                </dd>
                
                <dt><i class="icon-user"></i> Público Alvo</dt>
                <dd><?php echo $item->publico; ?></dd>
                <dt><i class="icon-ok"></i> Pré-requisitos</dt>
                <dd><?php echo $item->requisitos; ?></dd>
                <dt><i class="icon-list-alt"></i> Programa</dt>
                <dd>
                    <?php echo $item->programa; ?>
                </dd>
                <dt><i class="icon-book"></i> Metodologia</dt>
                <dd>
                    <?php echo $item->metodologia; ?>
                </dd>
            </dl>
        </div>
         <div class="curso-actions">
            <div class="btn-group">
                <button class="btn btn-small btn-primary dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-pencil"></i> Inscreva-se <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <?php foreach($turmas as $turma): ?>
                        <li><a href="#"><?php echo $turma->local . ' - ' . $turma->data; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div><!-- /btn-group -->
            <div class="btn-group">
                <button class="btn btn-small btn-info dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-share"></i> Compartilhe <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Linkedin</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Envie por email</a></li>
                </ul>
            </div><!-- /btn-group -->
            <div class="clearfix"></div>
        </div>
    </div>
    
    
</div>						
                <div class="clearfix"></div>

		</div>
                <div id="container-base"></div>
		
