<?php

class Busca extends CI_Model
{
    function buscar($entidade, $valor)
    {
        switch ($entidade)
        {
            case 'vagas':
                    $query = $this->db->like('vaga_empresa', $valor, 'both')->get('vagas');
                break;
            case 'programas':
                    $query = $this->db->like('programa_empresa', $valor, 'both')->get('programas');
                break;
            case 'noticias':
                $query = $this->db->like('noticia_titulo', $valor, 'both')->get('noticias');
                break;
           break;
        }
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $result[] = $row;
            }

            return $result;
        }
    }
}
/* End of file file.php */
/* Location: ./application/moules/module/controllers/controller.php */