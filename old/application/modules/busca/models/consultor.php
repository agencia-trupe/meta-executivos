<?php
/*
 * @var string $horas
 */
class Consultor extends DataMapper {
    
    var $table = 'consultores';
    
     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    
    function save_consultor($dados){
        
        $item = new Consultor();
        
        $item->nome = $dados['nome'];
        $item->email = $dados['email'];
        $item->telefone1 = $dados['telefone1'];
        $item->telefone2 = $dados['telefone2'];
        
        if($item->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }
       
    }
    
    function update_consultor($id, $dados){
        
        $consultor = new Consultor();
        $consultor->where('id', $id);
        $update = $consultor->update(array(
            
            'nome' => $dados['nome'],
            'email' => $dados['email'],
            'telefone1' => $dados['telefone1'],
            'telefone2' => $dados['telefone2'],
            
            ));
        
        return $update;
       
    }
    
    function delete_consultor($id){
        $consultor = new Consultor();
        $consultor->where('id', $id)->get();
        

        if($consultor->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
    
    function get_consultor($id)
    {
        $consultor = new Consultor();
        $consultor->where('id', $id);
        $consultor->limit(1);
        $consultor->get();
        
        if($consultor->exists())
        {
            return $consultor;
        }
        else
        {
            return FALSE;
        }
    }
    
    function get_item_by_id($id)
    {
        $item = new Item();
        $item->where('id', $id);
        $item->limit(1);
        $item->get();
        
        if($item->exists())
        {
            return $item;
        }
        else
        {
            return FALSE;
        }
    }
    
    
    
    function get_id($slug)
    {
        $i = new Item();
        $i->where('slug', $slug);
        $i->limit(1);
        $i->get();
        
        return $i->id;
    }
    
    /*function get_turmas($id)
    {
        $i = new Item();
        $i->where('id', $id);
        $i->get();
        
        $i->turma->get_iterated();
        
        $arr = array();
        foreach($i->turma->all as $turmas)
        {
            $arr[] = $turmas;
        }
        
        return $arr;
        
    } */
    
    function get_turmas($id)
    {
        $item = new Turma();
        $item->where('item_id', $id);
        $item->get();
        
        $arr = array();
        foreach($item->all as $turma)
        {
            $arr[] = $turma;
        }
        
        return $arr;
    }
    
    
}

/* End of file employee.php */
/* Location: ./application/moules/atleta/models/atleta_model.php */