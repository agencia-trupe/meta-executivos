<div class="span9">
    <div class="row-fluid well">
    <?php if(isset($turma)): ?>
    <div id="curso">
        
        
                       <?php echo anchor("turmas/edita/$turma->id", 'Editar', 'class="btn btn-mini btn-primary"'); ?> <?php echo anchor("turmas/apaga/$turma->id", 'Apagar', 'class="btn btn-mini btn-danger"'); ?>

                
        <div class="clearfix"></div>
        <h2>Informações da Turma</h2>
        <div id="curso-content">
            <dl class="dl-horizontal">
                
                <dt>Id</dt>
                <dd><?php echo $turma->id; ?> </dd>
                
                <dt>Treinamento</dt>
                <dd> 
                    <?php echo $turma->item->titulo; ?>
                </dd>
                
                <dt>Consultor</dt>
                <dd><?php echo $turma->consultor->nome; ?></dd>
                
                <dt>Valor</dt>
                <dd><?php echo $turma->valor; ?></dd>
                
                <dt>Total de vagas</dt>
                <dd><?php echo $turma->vagas; ?></dd>
                
                <dt>Vagas disponíveis</dt>
                <dd><?php 
                
                echo $disponiveis; ?></dd>

                <dt>Data</dt>
                <dd><?php echo date('d/m/Y', strtotime($turma->data)); ?></dd>
                
                <dt>Local</dt>
                <dd><?php echo $turma->local; ?></dd>
                
            </dl>
            
             
        </div>
         
    </div>
    
    <?php else: ?>
    <p>Treinamento não encontrado</p>
    <?php endif; ?>
    </div> 
    <div class="row-fluid well">
        <h2>Inscrições</h2>
        <table class="table table-bordered table-striped">
            <thead>
            <th>Nome</th><th>Email</th><th>Telefone</th><th>Empresa</th><th>Status</th>
            </thead>
            <?php foreach($alunos as $aluno): ?>
            <tr>
                <td><?php echo $aluno->nome; ?></td>
                <td><?php echo $aluno->email; ?></td>
                <td><?php echo $aluno->telefone; ?></td>
                <td><?php echo $aluno->empresa; ?></td>
                <td><?php if($aluno->status == '0'){echo 'Pagamento pendente';}
                            elseif($aluno->status == '1') {echo 'Pagamento realizado';}
                                elseif($aluno->status == '2') {echo 'Pagamento contestado';}
                                    elseif($aluno->status == '3') {echo 'Inscrição canceladafeh';}
                ?></td>
            </tr>
            
        <?php endforeach; ?>
        </table>
        
    </div>
</div>	


                <div class="clearfix"></div>

		</div>
		
