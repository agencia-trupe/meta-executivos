<div class="span9">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="alert alert-info">
            <p>Para buscar um consultor, primeiro selecione um campo e um critério
            para a busca. Após selecionar campo e critério, digite o valor e clique em 
            buscar.</p>
        </div>
    </div>
    
</div><!--/span-->
         
          
      