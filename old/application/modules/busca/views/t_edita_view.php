<div class="span9">
 
<ul class="nav nav-pills">
  <li><a href="<?php echo base_url(); ?>turmas/lista">Listar Turmas</a></li>
  <li class="active">
    <a href="#">Editar Turma</a>
  </li>
</ul>

    <?php
   $valor = array(
       'name' => 'valor',
       'value' =>  set_value('valor', $turma->valor),
       'class' => 'span1',
       'id'  =>  'appendedPrependedInput'
   );
    
    echo form_open('turmas/atualiza', 'class="well form-horizontal"');
    
    echo form_hidden('id', $turma->id);
    ?>

    <div class="row-fluid">
        </div>
    
    
      <div class="control-group">
            <label class="control-label" for="treinamento">Treinamento</label>
            <div class="controls">
              <?php 
                
                $treinamento_display = form_dropdown_from_db($name = 'treinamento', $item, $selected = array($turma->item_id), $extra = 'class="span6"');

                if($treinamento_display)
                {
                echo $treinamento_display;
                }
                
                
                ?>
                <?php echo form_error('treinamento'); ?>
            </div>
      </div>  
      <div class="control-group">
            <label class="control-label" for="consultor">Consultor</label>
            <div class="controls">
              <?php 
                
                $consultor_display = form_dropdown_from_db($name = 'consultor', $consultor, $selected = array($turma->consultor_id), $extra = 'class="span4"');

                if($consultor_display)
                {
                echo $consultor_display;
                }
                
                
                ?>
                <?php echo form_error('consultor'); ?>
            </div>
      </div>  
    <div class="control-group">
            <label class="control-label" for="vagas">Vagas</label>
            <div class="controls">
              <?php echo form_input('vagas', set_value('vagas', $turma->vagas)); ?>
              <span class="help-inline"><?php echo form_error('vagas'); ?></span>
            </div>
     </div>
      <div class="control-group">
            <label class="control-label" for="data">Data</label>
            <div class="controls">
              <?php echo form_input('data', set_value('data', $turma->data), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('data'); ?></span>
            </div>
     </div>
     <div class="control-group">
            <label class="control-label" for="local">Local</label>
            <div class="controls">
              <?php echo form_input('local', set_value('local', $turma->local)); ?>
              <span class="error-inline"><?php echo form_error('local'); ?></span>
            </div>
     </div>
     <div class="control-group">
            <label class="control-label" for="appendedPrependedInput">Valor</label>
            <div class="controls">
              <div class="input-prepend input-append">
                <span class="add-on">R$</span><?php echo form_input($valor); ?><span class="add-on">,00</span>
                <span class="error-inline"><?php echo form_error('valor'); ?></span>
              </div>
            </div>
    </div>
    
                    
    
        <?php echo form_submit('submit', 'Alterar', 'class="btn btn-warning"'); ?>

    <?php echo form_close(); ?>
    
    
</fieldset>
</div>