<div class="span9">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <legend>Resultados da busca por <em><?php echo $entidade . '</em>: ' . $valor; ?></legend>
        <?php foreach($results as $result): ?>
            <?php if($entidade == 'noticias'): ?>
                <div class="row-fluid">
                    <?php if($result->noticia_imagem): ?>
                        <img style="float:left; margin-right: 15px;" src="<?php echo base_url(); ?>assets/img/noticias/thumbs/<?php echo $result->noticia_imagem; ?>" alt="">
                    <?php endif; ?>
                    <h3 style="margin-top: 0; margin-bottom: 0"><?php echo $result->noticia_titulo; ?></h3>
                    <p style="margin-bottom: 0;"><b><em><?php echo $result->noticia_resumo; ?></b></em></p>
                    <p><?php echo substr(strip_tags($result->noticia_conteudo), 0, 160); ?></p>
                    <div class="clearfix"></div>
                    <a href="<?php echo base_url(); ?>painel/noticias/editar/<?php echo $result->id; ?>" target="_blank" class="btn btn-small btn-warning">Editar notícia</a>
                </div>
            <?php elseif($entidade == 'vagas'): ?>
                <div class="row-fluid">
                    <h3 style="margin-top: 0; margin-bottom: 0"><?php echo $result->vaga_empresa; ?></h3>
                    <p><b><em>Número de vagas: <?php echo $result->vaga_quantidade; ?></b></em></p>
                    <p><?php echo substr(strip_tags($result->vaga_descritivo), 0, 160); ?></p>
                    <div class="clearfix"></div>
                    <a href="<?php echo base_url(); ?>painel/vagas/editar/<?php echo $result->id; ?>" target="_blank" class="btn btn-small btn-warning">Editar vaga</a>
                </div>
            <?php elseif($entidade == 'programas'): ?>
                <div class="row-fluid">
                    <h3 style="margin-top: 0; margin-bottom: 0"><?php echo $result->programa_empresa; ?></h3>
                    <p><b><em>Categoria: <?php echo $result->programa_categoria; ?></b></em></p>
                    <p>Data de expiração: <?php echo date('d/m/Y', $result->programa_data_expiracao); ?></p>
                    <div class="clearfix"></div>
                    <a href="<?php echo base_url(); ?>painel/programas/editar/<?php echo $result->id; ?>" target="_blank" class="btn btn-small btn-warning">Editar programa</a>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div><!--/span-->  