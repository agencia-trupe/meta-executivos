<div class="span9">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
    </div>
    <?php if($entidade != NULL): ?>
    <?php echo form_open('busca/adm/resultado', 'class="form-search"'); 
        echo form_hidden('entidade', set_value('entidade', $entidade));
    ?>
    <div class="control-group">
        <div class="controls">
            <?php 
            $valor = array(
                'name' => 'valor',
                'id'    =>  'valor',
                'class' =>  'input-xlarge search-query',
                'value' => set_value('valor'),
                'Placeholder' => ($entidade == 'vagas' || $entidade == 'programas') 
                ? 'Digite o nome da Empresa' : 'Digite parte do título',
            );

            echo form_input($valor);

            ?>
            <?php echo form_submit('submit', 'Buscar', 'class="btn btn-success"'); ?>
            <span><?php echo form_error('valor'); ?></span>
        </div>

    </div>

    <?php echo form_close(); ?>
    <?php endif; ?>

</div><!--/span-->