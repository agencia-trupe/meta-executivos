<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Publico extends MX_Controller
{
    
        
    
        function index(){
            redirect();
        }
    
	function nova($entidade = NULL){
            
            
                    $data['title'] = 'Meta Executivos - Busca';
                    $data['main_content'] = 's_publico_view';
                    $this->load->view('itens/includes/template', $data);
        }
        
        
        function resultado(){
            
                $data['valor'] = $this->input->post('valor');
                        $config = array(
                            
                            array(
                                'field' => 'valor',
                                'label' => 'valor',
                                'rules' => 'required',
                            ),

                        );  
               
                
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

        
                if($this->form_validation->run() == FALSE ){
                    $data['title'] = 'Meta Executivos - Busca';
                    $data['main_content'] = 's_publico_view';
                    $this->load->view('itens/includes/template', $data);
                    
                }
                //Fim do if form_validation false
                else
                {
                    
                    $this->load->model('busca');
                    if($this->busca->buscar_publico($this->input->post('valor')))
                    {
                        $data['results'] = $this->busca->buscar_publico($this->input->post('valor'));
                        
                        $data['title'] = 'Meta Executivos - Busca';
                        $data['main_content'] = 's_resultpublico_view';
                        $this->load->view('itens/includes/template', $data);
                        
                    }
                    else
                    {
                        $entidade = $this->input->post('entidade');
                        $this->session->set_flashdata('error', 'Sua pesquisa por"' . $data['valor'] . '"não retornou
                            nenhum resultado');
                        redirect("busca/publico/nova/");
                    }
                  
                    
                }
                //Fim do else form_validation
                
                
            
        }
        //Fim do método
      
     
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */