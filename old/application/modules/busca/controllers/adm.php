<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adm extends MX_Controller
{

        var $module;

        function __construct() {
            parent::__construct();
        $this->module = 'busca';

        }
        function index(){
            $this->nova();
        }

	function nova($entidade = NULL){

            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {

                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $data['module'] = $this->module;
                    $data['title'] = 'Meta Executivos - Busca';

                    if($entidade == NULL)
                    {
                        $data['message'] = '<p>Para iniciar uma busca, selecione na 
                            barra lateral o tipo de busca que deseja realizar</p>';
                    }
                    $data['entidade'] = $this->uri->segment(4); 
                    $data['main_content'] = 's_adm_view';
                    $this->load->view('includes/template', $data);

                }
                else
                {

                    /*
                     *  @todo   Logger
                     */
                    $this->session->set_flashdata('error', 'Erro de permissão. 
                        Você precisa ser administrador para realizar essa ação');
                    redirect("turmas/lista");

                }

            }
        }

        function resultado(){

            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            } 
            //Fim do if logado
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {

                $config = array(
                    array(
                        'field' => 'valor',
                        'label' => 'valor',
                        'rules' => 'required',
                    ),

                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');


                if($this->form_validation->run() == FALSE ){

                    $data['module'] = $this->module;
                    $data['title'] = 'Meta Executivos - Busca';
                    $entidade = $this->input->post('entidade');

                    if($entidade == NULL)
                    {
                        $data['message'] = '<p>Para iniciar uma busca, selecione na 
                            barra lateral o tipo de busca que deseja realizar</p>';
                    }

                    $data['entidade'] = $entidade; 
                    $data['main_content'] = 's_adm_view';
                    $this->load->view('includes/template', $data);

                }
                //Fim do if form_validation false
                else
                {
                    $this->load->model('busca');
                    if($this->busca->buscar($this->input->post('entidade'),  
                                            $this->input->post('valor') 
                                            )
                      )
                    {
                        $data['results'] = $this->busca->buscar($this->input->post('entidade'), 
                                            $this->input->post('valor')
                                            );

                        $data['module'] = $this->module;
                        $data['title'] = 'Meta Executivos - Busca';
                        $data['entidade'] = $this->input->post('entidade');
                        $data['valor'] = $this->input->post('valor');
                        $data['main_content'] = 's_resultadm_view';
                        $this->load->view('includes/template', $data);

                    }
                    else
                    {
                        $entidade = $this->input->post('entidade');
                        $this->session->set_flashdata('error', 'Sua pesquisa não retornou
                            nenhum resultado, tente um campo ou termo diferente');
                        redirect("busca/adm/nova/$entidade");
                    }
                  
                    
                }
                //Fim do else form_validation
                }
                //Fim do if admin or manager
                else
                {
                    /*
                     *  @todo   Logger
                     */
                    $this->session->set_flashdata('error', 'Erro de permissão. 
                        Você precisa ser administrador para realizar essa ação');
                    redirect("turmas/lista");
            
                }
                //Fim do else admin or manager    
            
                
            }
            //Fim do else Logado
            
        }
        //Fim do método
        
        function atualiza(){
            
            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            } 
            else    
            {
                $config = array(
                    array(
                        'field' => 'treinamento',
                        'label' => 'treinamento',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'consultor',
                        'label' => 'consultor',
                        'rules' => 'required',
                    ),
                    
                    array(
                        'field' => 'data',
                        'label' => 'data',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'local',
                        'label' => 'local',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'valor',
                        'label' => 'valor',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'vagas',
                        'label' => 'vagas',
                        'rules' => 'required',
                    ),
                     
                );  
                
                $id = $this->input->post('id');
                
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

        
                if($this->form_validation->run() == FALSE ){
                      
                    $data['item'] = 'SELECT id,titulo FROM itens';
                $data['consultor'] = 'SELECT id,nome FROM consultores';
                $data['module'] = 'turmas';
                $data['title'] = 'Meta Executivos - turmas - Editar';
                if($this->turma->get_turma($id)){
                $data['turma'] = $this->turma->get_turma($id);
                
                $data['main_content'] = 't_edita_view';
                $this->load->view('includes/template', $data);
                }
                    
                    
                }
                else
                {   
                
                /*
                 * Cria um array com os dados vindos do formulário para que sejam
                 * passados ao model responsável por persisir os dados no 
                 * banco.
                 */
                $id = $this->input->post('id');
                
                $dados = array(
                    'item_id' => $this->input->post('treinamento'),
                    'consultor_id' => $this->input->post('consultor'),
                    'data' => $this->input->post('data'),
                    'valor' => $this->input->post('valor'),
                    'local' => $this->input->post('local'),
                    'vagas' => $this->input->post('vagas'),
                    
                );
                
                $this->load->model('itens/turma');
                
                /*
                 * Caso os dados sejam salvos com sucesso, carrega a lista de 
                 * ítens, caso contrário, exibe mensagem de erro ao usuário.
                 * 
                 * @todo Log de erro ao salvar usuário
                 *  
                 */
                if($this->turma->update_turma($id, $dados)){
                    $this->session->set_flashdata('success', 'turma alterado
                        com sucesso');
                    redirect('turmas/lista');
                }
                else{
                    $this->session->set_flashdata('error', 'Erro de atualização. 
                        A ação não pode ser
                    realizada, tente novamente ou entre em contato com o suporte');
                    redirect('turmas/lista');
                }

                }
                
                
            }
            
            
            
            
            
            
            
            
            
            
        }
        
        
     
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */