<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Feed extends CI_Model {
        
        
	function twitter_feed($username = '', $max = 5, $cache_time = 3600, $timeout = 1, $clear_cache = false) {

        // pattern replacement rules to process the tweet texts (by Phil Sturgeon)
        $patterns = array(
            // Detect URL's
            '|([a-z]{3,9}://[a-z0-9-_./?&+]*)|i' => '<a href="$0" target="_blank">$0</a>',
            
            // Detect Email
            '|[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,6}|i' => '<a href="mailto:$0">$0</a>',
            
            // Detect Twitter @usernames
            '|@([a-z0-9-_]+)|i' => '<a href="http://twitter.com/$1" target="_blank">$0</a>',
            
            // Detect Twitter #tags
            '|#([a-z0-9-_]+)|i' => '<a href="http://twitter.com/search?q=#$1" target="_blank">$0</a>'
        );
        
        // cache file and live file
        $cache_file = './application/cache/twitter/'.$username.'.xml';
        $live_file = 'http://api.twitter.com/1/statuses/user_timeline.xml?screen_name=' . $username;

        // clear cache
        if($clear_cache) unlink($cache_file);

        // check if a cache file exists        
        if(file_exists($cache_file)) {
            
            // is the cache file still valid?
            if(filemtime($cache_file) + $cache_time > mktime()) {
                // read the rss feed from the cache
                $rss_feed = file_get_contents($cache_file);
            }
            // if not valid anymore
            else {
                // try and get the live feed
                $context = stream_context_create(array('http' => array('timeout' => $timeout)));
                $rss_feed = @file_get_contents($live_file, 0, $context);
                // save rss feed in cache file if the rss feed was retrieved successfully
                if($rss_feed) file_put_contents($cache_file, $rss_feed);
                // else use the cached file instead
                else $rss_feed = file_get_contents($cache_file);                    
            }
        }
        // cache file doesn't exist
        else {
            // try and get the live feed
            $context = stream_context_create(array('http' => array('timeout' => $timeout)));
            $rss_feed = @file_get_contents($live_file, 0, $context);
            // save rss feed in cache file if the rss feed was retrieved successfully
            if($rss_feed) file_put_contents($cache_file, $rss_feed);
            // else failed to retrieve feed
            else $rss_feed = false;            
        }
        
        // Ok. We have an rss feed. Let's process it
        if($rss_feed) {
            // we use simplexml to create an object out of the rss feed
            $tweets = @simplexml_load_string($rss_feed);
            // simplexml couldn't process the rss feed properly
            if(!$tweets) {
                // we return an error
                $return = 'Twitter feed not found';
                // we clear the cache. No use for a cache file that can't be processed
                unlink($cache_file);
            }
            // yes. we have something to process
            else {
                // set our counter to '0'
                $i = 0;
                $return = array();
                // loop through the tweets
                foreach($tweets as $tweet) {
                    // check the counter
                    if($i >= $max) continue;
                    // add the tweet (text is being parsed)) to the return variable
                    $return[] = array(
                        'datetime' => $tweet->created_at,
                        'text' => preg_replace(array_keys($patterns), array_values($patterns), $tweet->text),
                        'source' => $tweet->source,
                        'link' => 'http://twitter.com/' . $username . '/status/' . $tweet->id 
                    );
                    // counter + 1
                    $i++;
                }
            }
        }
        // we ended up with no rss feed, return error
        else $return = 'Twitter feed not found';
        
        // return the result
        return $return;    
    } 

}

/* End of file file.php */
/* Location: ./application/modules/home/models/twitter.php*/
