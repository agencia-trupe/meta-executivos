<div class="twitter-feed">
	<div class="feed">
		<a href="https://twitter.com/MetaTALENTOS" id="siga"></a>
		<ul>
			<?php foreach($tweets as $tweet): ?>
				<li>
					<p><?php echo $tweet['text']; ?></p>
					<?php if(round(abs(strtotime($tweet['datetime']) - time()) / 60) < 60): ?>
					<a class="time" target="_blank" href="<?php echo $tweet['link']; ?>"><?php echo 'Há ' . round(abs(strtotime($tweet['datetime']) - time()) / 60) . 'min'; ?></a>
					<?php elseif(round(abs(strtotime($tweet['datetime']) - time()) / 3600) < 24): ?>
					<a class="time" target="_blank" href="<?php echo $tweet['link']; ?>"><?php echo 'Há ' . round(abs(strtotime($tweet['datetime']) - time()) / 3600) . 'h'; ?></a>
					<?php else: ?>
					<a class="time" target="_blank" href="<?php echo $tweet['link']; ?>"><?php echo date('d/m', strtotime($tweet['datetime'])); ?></a>
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
			</ul>
	</div>
</div>