<div id="clipping" class="<?php echo $pagina; ?>">
			<header>
				<p>Você pode acompanhar todas as novidades da Meta Executivos também nas redes sociais:</p>
				<ul>
					<li><a href="http://www.facebook.com/MetaTalentos" id="facebook" ></a></li>
					<li><a href="http://twitter.com/MetaTALENTOS" id="twitter"></a></li>
					<li><a href="#" id="linkedin"></a></li>
				</ul>
			</header>
			<aside class="left">
				<img width="220" height="251" src="<?php echo base_url(); ?>assets/img/noticias.jpg" alt="Meta Executivos - Notícias">
			</aside>
			<div class="clipping-loop">
				<header>Nossas novidades e artigos relacionados:</header>
				<?php foreach($results as $result): ?>
					<article class="clipping">
						<h1><?php echo $result->noticia_titulo; ?></h1>
						<section>
							<p>
								<em><b><?php echo substr($result->noticia_resumo, 0, 160); ?> ...</b></em>
							</p>
						</section>
						<a href="<?php echo base_url(); ?>novidades/post/<?php echo $result->id; ?>" class="mais">Continuar lendo &raquo;</a>
					</article>
				<?php endforeach; ?>
			</div>
		</div>