<div class="conteudo-novidades">
    <div class="texto-right">
        <div class="clearfix"></div>
        <article id="principal">
            <div class="conteudo">
                <h1><?php echo $post->noticia_titulo; ?></h1>
                <?php if($post->noticia_imagem): ?>
                <img class="thumb" src="<?php echo base_url(); ?>assets/img/noticias/<?php echo $post->noticia_imagem; ?>" alt="Miniatura Notícia 1">
            <?php endif; ?>
                <?php echo $post->noticia_conteudo; ?>
            </div>
            <div class="clearfix"></div>
            <a href="<?php echo site_url('noticias'); ?>" id="noticias-voltar">voltar</a>
        </article>
    </div>
    <div class="clearfix"></div>
</div>