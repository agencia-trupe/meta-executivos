<div class="conteudo-novidades">
    <div class="texto-right">
        <div class="clearfix"></div>
        <?php foreach($results as $result): ?>
            <article>
            <div class="conteudo">
                <h1><?php echo $result->noticia_titulo; ?></h1>
                <?php if($result->noticia_imagem): ?>
                    <a href="<?php echo base_url(); ?>novidades/post/<?php echo $result->id; ?>" class="thumb">
                        <img src="<?php echo base_url(); ?>assets/img/noticias/thumbs/<?php echo $result->noticia_imagem; ?>" alt="<?php echo $result->noticia_titulo; ?>">
                    </a>
                <?php endif; ?>
                <p><b><?php echo $result->noticia_resumo; ?></b></p>
                <p><?php echo substr(strip_tags($result->noticia_conteudo), 0, 240); ?>...</p>
            </div>
            <div class="clearfix"></div>
            <div class="separador"></div>
            <div class="more">
                <a href="<?php echo base_url(); ?>novidades/post/<?php echo $result->id; ?>">Continuar lendo &raquo;</a>
            </div>
            <div class="clearfix"></div>
            </article>
        <?php endforeach; ?>
        <?php echo $this->pagination->create_links(); ?>
    </div>
</div>
    <div class="clearfix"></div>