<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* clientes 
* 
* @author Nilton de Freitas - Trupe 
* @link http://trupe.net
*
*/
Class Novidades extends MX_Controller
{
    public function lista()
    {      
        $this->load->library('pagination');
            $pagination_config = array(
                        'base_url'       => base_url() . 'novidades/lista/',
                        'total_rows'     => $this->db->where('noticia_data_publicacao <', time())->get('noticias')->num_rows(),
                        'per_page'       => 3,
                        'num_links'      => 5,
                        'next_link'      => 'próximo',
                        'prev_link'      => 'anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE, 
                        'full_tag_open'  => '<div class="pagination center"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                );
            $this->pagination->initialize($pagination_config);
            //Obtendo resultados no banco
            $this->load->model('noticias/noticia');
            $data['results'] = $this->noticia->get_all($pagination_config['per_page'], $this->uri->segment(3));

        $data['titulo'] = 'Meta Executivos &middot; Novidades | Artigos';
        $data['pagina'] = 'novidades';
        $data['header_titulo'] = 'Notícias';
        $data['header_image'] = '07.jpg';
        $data['conteudo_principal'] = "novidades/lista";
        $this->load->view('layout/template', $data);
    }

    public function post($id)
    {
        $this->load->model('noticias/noticia');
        $data['post'] = $this->noticia->get_noticia($id);
        $data['relateds'] = $this->noticia->get_relate($id);
        $anterior = $this->noticia->get_anterior($id);
        $data['anterior'] = $anterior ? $anterior->id : NULL;
        $proximo = $this->noticia->get_proximo($id);
        $data['proximo'] = $proximo ? $proximo->id : NULL;
        $data['titulo'] = 'Meta Executivos &middot; Novidades | Artigos';
        $data['pagina'] = 'novidades';
        $data['conteudo_principal'] = "novidades/post";
        $data['header_image'] = '07.jpg';
        $data['header_titulo'] = 'Notícias';
        $this->load->view('layout/template', $data);
    }

    /**
     * Verifica se o tipo de cliente passado como parâmetro está no array
     * dos clientes e retorna a lista de clientes do tipo caso verdadeiro.
     * @return mixed Lista de clientes ativos
     */


    /**
     * Exibe a página de detalhes de um case
     * @param  int $id 
     */
    public function detalhe($id = NULL)
    {
        if($id == NULL)
        {
            redirect('clientes');
        }
        else
        {
            $this->load->model('clientes/cliente');
            //Verifica a existencia do cliente/case e retorna ao index caso falso.
            if(!$this->cliente->get_cliente($id))
            {
                redirect('clientes');
            }
            else{
                $data['cliente'] = $this->cliente->get_cliente($id);
                //Verifica a existência de registros anteriores e próximos e os retorna
                //caso seja verdadeiro.
                $anterior = $this->cliente->get_anterior($id);
                $data['anterior'] = $anterior ? $anterior->id : NULL;
                $proximo = $this->cliente->get_proximo($id);
                $data['proximo'] = $proximo ? $proximo->id : NULL;
                //Layout
                $data['titulo'] = 'Meta Executivos &middot; Clientes | Cases - ' . $data['cliente']->nome;
                $data['pagina'] = 'clientes';
                $data['conteudo_principal'] = "clientes/detalhe";
                $this->load->view('layout/main', $data);
            }
        }
    }
}