    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo base_url(); ?>">Meta Executivos</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li 
                  class="<?php if($module === 'vagas')
                  {
                      echo 'active';
                  }
                  ?>"
              >
                <a href="<?php echo base_url(); ?>painel/vagas/lista">Vagas</a></li>
              <li 
                  class="<?php if($module === 'noticias')
                  {
                      echo 'active';
                  }
                  ?>"
              >
                <a href="<?php echo base_url(); ?>painel/noticias/lista">Notícias</a></li>
                <li 
                  class="<?php if($module === 'newsletter')
                  {
                      echo 'active';
                  }
                  ?>"
              >
                <a href="<?php echo base_url(); ?>painel/newsletter/">Newsletter</a></li>
              <li 
                  class="<?php if($module === 'busca')
                  {
                      echo 'active';
                  }
                  ?>"
              >
                <a href="<?php echo base_url(); ?>busca/adm/">Busca</a></li>
            </ul>
            <ul class="nav pull-right">
                <li><?php echo anchor('logout', 'Sair'); ?></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">