<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adminvagas extends MX_Controller
{
    function index(){

            $this->lista();
        }
        /**
         *Lista todos os vagas cadastrados atualmente
         *
         * @return [type] [description]
         */
        function lista()
        {
            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',
                    $this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
              {
                $this->load->library('pagination');
                $this->load->library('table');
                $pagination_config = array(
                            'base_url'       => site_url() . 'painel/vagas/lista/',
                            'total_rows'     => $this->db->get('vagas')->num_rows(),
                            'per_page'       => 30,
                            'num_links'      => 5,
                            'next_link'      => 'próximo',
                            'prev_link'      => 'anterior',
                            'first_link'     => FALSE,
                            'last_link'      => FALSE, 
                            'full_tag_open'  => '<div class="pagination center"><ul>',
                            'full_tag_close' => '</ul></div>',
                            'cur_tag_open'   => '<li class="active"><a href="#">',
                            'cur_tag_close'  => '</a></li>',
                            'num_tag_open'   => '<li>',
                            'num_tag_close'  => '</li>',
                            'next_tag_open'   => '<li>',
                            'next_tag_close'  => '</li>',
                            'prev_tag_open'   => '<li>',
                            'prev_tag_close'  => '</li>',
                    );
                    $this->pagination->initialize($pagination_config);
                    //Obtendo resultados no banco
                    $this->load->model('vagas/vaga');
                    $data['result'] = $this->vaga->
                    get_all_all($pagination_config['per_page'], $this->uri->
                        segment(4));

                    $data['title'] = 'Meta Executivos - Vagas - Lista';
                    $data['module'] = 'vagas';
                    $data['main_content'] = 'v_lista_view';
                    $this->load->view('includes/template', $data);
              }
              else
              {
              Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de 
                acesso area administrativa sem privilégios');
              $this->session->set_flashdata('error', 'Erro de permissão. 
                                                    Você precisa ser 
                                                    administrador para realizar essa ação');
              redirect();

              } 
            }
        }
        /**
         * Mostra a página de edição de um vaga cujo id foi passado como 
         * parâmetro.
         *
         * @param  [int] $id [description]
         * @return [mixed]     [description]
         */
        function editar($id)
        {
            /**
             * Verifica se o usuário está logado para então prosseguir ou não.
             */
            if (!$this->tank_auth->is_logged_in())
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                //Verifica se o usuário tem nível de acesso permitido
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $id = $this->uri->segment(4);
                    if(!$id)
                    {
                        $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                        redirect('painel/vagas/lista');
                    }
                    else
                    {
                        $this->load->model('vagas/vaga');
                        $data['module'] = 'vagas';
                        $data['title'] = 'Meta Executivos - vagas - Editar';

                        if($this->vaga->get_vaga($id))
                        {
                            $data['vaga'] = $this->vaga->get_vaga($id);
                            $data['acao'] = 'editar';
                            $data['main_content'] = 'v_cadastra_view';
                            $this->load->view('includes/template', $data);
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                            realizada, tente novamente ou entre em contato com o suporte');
                            redirect('vagas/lista');
                        }
                    }
                }
                else
                {
                    Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                    $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                    redirect();
                }
            }
        }

        function detalhe($slug = FALSE)
        {
            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                      if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
        {


            $slug = $this->uri->segment(3);
            if (!$slug) 
            {
                $this->lista();
            }
            else
            {

                $this->load->model('itens/vaga'); 
                $this->load->model('itens/turma');
                if($this->vaga->get_vaga($slug))
                {
                    $data['vaga'] = $this->vaga->get_vaga($slug);
                }
                $id = $this->vaga->get_id($slug);
                $data['module'] = 'vagas';
                $data['turmas'] = $this->vaga->get_vaga_turmas($id);
                $data['title'] = 'Meta Executivos';
                $data['main_content'] = 't_detalhe_view';
                $this->load->view('includes/template', $data);
            }
        }
        else
        {
        Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
        $this->session->set_flashdata('error', 'Erro de permissão. 
                                              Você precisa ser administrador para realizar essa ação');
        redirect();

        }

            }
        }

        function imagem($slug)
        {
            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                        if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
        {
            $slug = $this->uri->segment(3);
            if (!$slug)
            {
                $this->lista();
            }
            else
            {

                $this->load->model('itens/vaga'); 
                $this->load->model('itens/turma');
                if($this->vaga->get_vaga($slug))
                {
                    $data['vaga'] = $this->vaga->get_vaga($slug);
                }
                $id = $this->vaga->get_id($slug);
                $data['module'] = 'vagas';
                $data['turmas'] = $this->vaga->get_vaga_turmas($id);
                $data['title'] = 'Meta Executivos';
                $data['main_content'] = 't_imagem_view';
                $this->load->view('includes/template', $data);
            }

        }
        else
        {
        Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
        $this->session->set_flashdata('error', 'Erro de permissão. 
                                              Você precisa ser administrador para realizar essa ação');
        redirect();

        }
            }
        }

        function salva_imagem()
        {

          if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                        if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
        {
              $gallery_path = realpath(APPPATH . '../assets/img');

            $config['upload_path'] = $gallery_path;
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size'] = '100';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
                $id = $this->input->post('id');


        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('vagadata'))
        {
             $this->session->set_flashdata('error', $this->upload->display_errors());
                         $slug = $this->input->post('slug');
                         redirect("vagas/imagem/$slug");
        }
        else
        {
                        /*
                         * Cria a variável data passando a ela o array com os 
                         * dados do upload da imagem
                         */
            $data  = $this->upload->data();

                        $this->load->model('imagem');
                        
                        /*
                         * Se o model responsável por gerar o thumbnail o fizer
                         * e retornar o booleano true, retorna a página de sucesso.
                         */
                        if($this->imagem->thumbnail($data, $id))
                        {
                            
                            $this->session->set_flashdata('success', 'Foto adicionada com sucesso');
                            $slug = $this->input->post('slug');
                            redirect("vagas/imagem/$slug");
                        }
                        else
                        {
                            $this->session->set_flashdata('error', 'Erro ao salvar foto, tente novamente');
                            $slug = $this->input->post('slug');
                            redirect("vagas/imagem/$slug");

                        }


        }

        }
        else
        {
        Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
        $this->session->set_flashdata('error', 'Erro de permissão. 
                                              Você precisa ser administrador para realizar essa ação');
        redirect();

        }
            }
        }

        function cadastra()
        {

            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                  if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                  {
                      $data['title'] = 'Meta Executivos - vagas - Novo vaga';
                      $data['module'] = 'vagas';
                      $data['acao'] = 'cadastra';
                      $data['main_content'] = 'v_cadastra_view';
                      $this->load->view('includes/template', $data);
                  }
                  else
                  {
                  Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                  $this->session->set_flashdata('error', 'Erro de permissão.
                                                        Você precisa ser administrador para realizar essa ação');
                  redirect();

                  }
            }
        }

        function salva(){

            if (!$this->tank_auth->is_logged_in())
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                $config = array(
                    array(
                        'field' => 'empresa',
                        'label' => 'empresa',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'titulo',
                        'label' => 'titulo',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'descritivo',
                        'label' => 'quantidade',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'link',
                        'label' => 'email',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'local',
                        'label' => 'local',
                        'rules' => 'required',
                    ),
                    array(
                        'field' => 'data_expiracao',
                        'label' => 'data_expiracao',
                        'rules' => 'required',
                    ),
                );
                $this->load->library('form_validation');
                $this->form_validation->set_rules($config);
                $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');
                $data['acao'] = $this->input->post('acao');

                if($this->form_validation->run() == FALSE ){
                      $data['title'] = 'Meta Executivos - Vagas - Nova Vaga';
                      $data['module'] = 'vagas';
                      $data['main_content'] = 'v_cadastra_view';
                      $this->load->view('includes/template', $data);
                }
                else
                {   //Verifica se foi feito o upload de uma imagem
                    if(strlen($_FILES["imagem"]["name"])>0)
                    {
                        $config['upload_path'] = './assets/img/vagas/';
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size'] = '100';
                        $config['max_width']  = '800';
                        $config['max_height']  = '600';

                        $this->load->library('upload', $config);

                        if ( ! $this->upload->do_upload('imagem'))
                        {
                                $data['error'] = array('error' => $this->upload->display_errors());

                                $data['title'] = 'Meta Executivos - Vagas - Nova Vaga';
                                $data['module'] = 'vagas';
                                $data['main_content'] = 'v_cadastra_view';
                                $this->load->view('includes/template', $data);
                        }
                        else
                        {
                            $this->load->library('image_moo');
                            //Is only one file uploaded so it ok to use it with $uploader_response[0].
                            $upload_data = $this->upload->data();
                            $file_uploaded = $upload_data['full_path'];
                            $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                            if($this->image_moo->load($file_uploaded)->set_background_colour("#FFF")->resize(260,68,TRUE)->save($new_file,true))
                            {
                                 $this->load->model('vagas/vaga');

                                //prepara o array com os dados para enviar ao model
                                $dados = array(
                                        'empresa' => $this->input->post('empresa'),
                                        'titulo' => $this->input->post('titulo'),
                                        'descritivo' => $this->input->post('descritivo'),
                                        'quantidade' => $this->input->post('quantidade'),
                                        'link' => $this->input->post('link'),
                                        'local' => $this->input->post('local'),
                                        'imagem' => $upload_data['file_name'],
                                        'destaque' => $this->input->post('destaque'),
                                        'data_expiracao' => $this->input->post('data_expiracao'),
                                    );

                                if( ! $this->vaga->cadastra_imagem($dados))
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/vagas/cadastra');
                                }
                                else
                                {
                                    $this->session->set_flashdata('success', 'Vaga cadastrada
                                    com sucesso!');
                                    redirect('painel/vagas/lista');
                                }
                            }
                            else
                            {
                                $this->session->set_flashdata('error', 'A ação não pode ser
                                realizada, tente novamente ou entre em contato com o suporte');
                                redirect('painel/vagas/cadastra');
                            }
                        }
                    }
                    else
                    {
                        $this->load->model('vagas/vaga');

                        //prepara o array com os dados para enviar ao model
                        $dados = array(
                                'empresa' => $this->input->post('empresa'),
                                'titulo' => $this->input->post('titulo'),
                                'descritivo' => $this->input->post('descritivo'),
                                'quantidade' => $this->input->post('quantidade'),
                                'link' => $this->input->post('link'),
                                 'local' => $this->input->post('local'),
                                'destaque' => $this->input->post('destaque'),
                                'data_expiracao' => $this->input->post('data_expiracao'),
                            );

                        if( ! $this->vaga->cadastra($dados))
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                            realizada, tente novamente ou entre em contato com o suporte');
                            redirect('painel/vagas/cadastra');
                        }
                        else
                        {
                            $this->session->set_flashdata('success', 'Vaga cadastrada
                            com sucesso!');
                            redirect('painel/vagas/lista');
                        }
                    }
                }
            }
        }

        function atualiza()
        {
            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $config = array(
                        array(
                            'field' => 'empresa',
                            'label' => 'empresa',
                            'rules' => 'required',
                        ),
                        array(
                        'field' => 'link',
                        'label' => 'email',
                        'rules' => 'required',
                        ),
                        array(
                            'field' => 'local',
                            'label' => 'local',
                            'rules' => 'required',
                        ),
                        array(
                            'field' => 'data_expiracao',
                            'label' => 'data_expiracao',
                            'rules' => 'required',
                        ),
                         array(
                            'field' => 'titulo',
                            'label' => 'titulo',
                            'rules' => 'required',
                        ),
                          array(
                            'field' => 'descritivo',
                            'label' => 'descritivo',
                            'rules' => 'required',
                        ),
                           array(
                            'field' => 'quantidade',
                            'label' => 'quantidade',
                            'rules' => 'required',
                        ),
                    );
                    $this->load->library('form_validation');
                    $this->form_validation->set_rules($config);
                    $this->form_validation->set_error_delimiters('<p><span class="label label-important">Erro</span> ', '  </p><br>');

                    if($this->form_validation->run() == FALSE )
                    {
                        $id  = $this->input->post('id');
                        $this->load->model('vagas/vaga');
                        $data['module'] = 'vagas';
                        $data['title'] = 'Meta Executivos - vagas - Editar';
                        $data['vaga'] = $this->vaga->get_vaga($id);
                        $data['acao'] = 'editar';
                        $data['main_content'] = 'v_cadastra_view';
                        $this->load->view('includes/template', $data);
                    }
                    else
                    {
                        //verifica se foi postada uma imagem
                        if(strlen($_FILES["imagem"]["name"])>0)
                        {
                            $config['upload_path'] = './assets/img/vagas/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['max_size'] = '100';
                            $config['max_width']  = '800';
                            $config['max_height']  = '600';

                            $this->load->library('upload', $config);

                            if ( ! $this->upload->do_upload('imagem'))
                            {
                                    $data['error'] = array('error' => $this->upload->display_errors());

                                    $data['title'] = 'Meta Executivos - Vagas - Nova Vaga';
                                    $data['module'] = 'vagas';
                                    $data['main_content'] = 'v_cadastra_view';
                                    $this->load->view('includes/template', $data);
                            }
                            else
                            {
                                $this->load->library('image_moo');
                                //Is only one file uploaded so it ok to use it with $uploader_response[0].
                                $upload_data = $this->upload->data();
                                $file_uploaded = $upload_data['full_path'];
                                $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];

                                if($this->image_moo->load($file_uploaded)->set_background_colour("#FFF")->resize(260,68,TRUE)->save($new_file,true))
                                {
                                     $this->load->model('vagas/vaga');

                                    //prepara o array com os dados para enviar ao model
                                    $dados = array(
                                            'id' => $this->input->post('id'),
                                            'empresa' => $this->input->post('empresa'),
                                            'titulo' => $this->input->post('titulo'),
                                            'descritivo' => $this->input->post('descritivo'),
                                            'quantidade' => $this->input->post('quantidade'),
                                            'link' => $this->input->post('link'),
                                            'local' => $this->input->post('local'),
                                            'imagem' => $upload_data['file_name'],
                                            'destaque' => $this->input->post('destaque'),
                                            'data_expiracao' => $this->input->post('data_expiracao'),
                                        );

                                    if( ! $this->vaga->atualiza_imagem($dados))
                                    {
                                        $this->session->set_flashdata('error', 'A ação não pode ser
                                        realizada, tente novamente ou entre em contato com o suporte');
                                        redirect('painel/vagas/cadastra');
                                    }
                                    else
                                    {
                                        $this->session->set_flashdata('success', 'Vaga alterada
                                        com sucesso!');
                                        redirect('painel/vagas/lista');
                                    }
                                }
                                else
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/vagas/cadastra');
                                }
                            }
                        }
                        else
                        {

                            $this->load->model('vagas/vaga');

                            //prepara o array com os dados para enviar ao model
                            $dados = array(
                                   'id' => $this->input->post('id'),
                                        'empresa' => $this->input->post('empresa'),
                                        'titulo' => $this->input->post('titulo'),
                                        'descritivo' => $this->input->post('descritivo'),
                                        'quantidade' => $this->input->post('quantidade'),
                                        'link' => $this->input->post('link'),
                                         'local' => $this->input->post('local'),
                                        'imagem' => $upload_data['file_name'],
                                        'destaque' => $this->input->post('destaque'),
                                        'data_expiracao' => $this->input->post('data_expiracao'),
                                );


                                if( ! $this->vaga->atualiza($dados))
                                {
                                    $this->session->set_flashdata('error', 'A ação não pode ser
                                    realizada, tente novamente ou entre em contato com o suporte');
                                    redirect('painel/vagas/atualiza/' . $this->input->post('id'));
                                }
                                else
                                {
                                    $this->session->set_flashdata('success', 'vaga atualizado
                                    com sucesso!');
                                    redirect('painel/vagas/lista');
                                }
                        }
                    }

                  }
                  else
                  {
                  Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                  $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                  redirect();

                  }
            }
        }

        function apaga($id)
        {
            if (!$this->tank_auth->is_logged_in())
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                $data['main_content'] = 'system/mustLogin';
                $data['title'] = 'Meta Executivos - Erro de acesso';
                $this->load->view('start/templatenonav', $data);
            }
            else
            {
                if($this->tank_auth->is_role('admin') OR $this->tank_auth->is_role('manager'))
                {
                    $id = $this->uri->segment(4);
                    if(!$id)
                    {
                    $this->session->set_flashdata('error', 'A ação não pode ser
                        realizada, tente novamente ou entre em contato com o suporte');
                    redirect('painel/vagas/lista');
                    }
                    else
                    {
                        $this->load->model('vagas/vaga');
                        if($this->vaga->delete_vaga($id))
                        {
                            $this->session->set_flashdata('success', 'Registro apagado
                            com sucesso');
                             redirect('painel/vagas/lista');
                        }
                         else
                        {
                            $this->session->set_flashdata('error', 'A ação não pode ser
                             realizada, tente novamente ou entre em contado com o suporte');
                            redirect('painel/vagas/lista');
                        }
                    }
                }
                else
                {
                    Modules::run('sys/logs/registra', 'acesso', NULL, 'Tentativa de acesso area administrativa sem privilégios');
                    $this->session->set_flashdata('error', 'Erro de permissão. 
                                                        Você precisa ser administrador para realizar essa ação');
                    redirect();

                }
            }

        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */