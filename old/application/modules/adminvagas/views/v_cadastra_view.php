<div class="span9">

<legend><?php echo $acao == 'editar' ? 'Editar Vaga' : 'Nova Vaga' ;?></legend>
    <?php echo isset($error) ? $error['error'] : ''; ?>
    <?php
    $empresa = array('name'=>'empresa', 'id' => 'empresa', 'value'=>set_value('empresa', $acao == 'editar' ? $vaga->vaga_empresa : ''), 'class' => 'span11',);

    echo form_open_multipart(($acao == 'editar') ? 'painel/vagas/atualiza' : 'painel/vagas/salva', 'class="well"'); 
    ?>

    <div class="row-fluid">
      <?php if($acao == 'edita')
      {
        echo form_hidden('acao', 'edita');
      }
      else{
        echo form_hidden('acao', 'cadastra');
      }
      ?>  
      </div>
    <?php if($acao == 'editar') : ?>
    <?php echo form_hidden('id', set_value('id', $vaga->id)); ?>
  <?php endif; ?>
    <div class="row-fluid">
        <div class="span8">
            <label for="empresa">Empresa</label>
            <?php echo form_input($empresa); ?>
            <?php echo form_error('empresa'); ?>
        </div>
    </div>
    <br>
  <div class="row-fluid">
      <div class="span11">
          <label for="titulo">Título</label>
              <?php echo form_input(array('name'=>'titulo', 'id' => 'titulo', 'value'=>set_value('titulo', $acao == 'editar' ? $vaga->vaga_titulo : ''), 'class' => 'span11',)); ?>
              <?php echo form_error('titulo'); ?>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
            <label class="control-label" for="descritivo">Descritivo</label>
            <div class="controls">
              <?php echo form_textarea('descritivo', set_value('descritivo', $acao == 'editar' ? $vaga->vaga_descritivo : ''), 'class="tinymce span11"'); ?>
              <span class="help-inline"><?php echo form_error('descritivo'); ?></span>
            </div>
      </div>
      <div class="control-group">
            <label class="control-label" for="local">Local</label>
            <div class="controls">
              <?php echo form_input('local', set_value('local', $acao == 'editar' ? $vaga->vaga_local : ''), 'class="span4"'); ?>
              <span class="help-inline"><?php echo form_error('local'); ?></span>
            </div>
      </div>
      <div class="control-group">
            <label class="control-label" for="data_expiracao">Data de expiração</label>
            <div class="controls">
              <?php echo form_input('data_expiracao', set_value('data_expiracao', $acao == 'editar' ? date('d/m/Y', $vaga->vaga_data_expiracao) : ''), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('data_expiracao'); ?></span>
            </div>
      </div>
      <div class="control-group">
            <label class="control-label" for="link">Email</label>
            <div class="controls">
              <?php echo form_input('link', set_value('link', $acao == 'editar' ? $vaga->vaga_link : ''), 'class="span4"'); ?>
              <span class="help-inline"><?php echo form_error('link'); ?></span>
            </div>
      </div>
      <div class="control-group">
            <label class="control-label" for="quantidade">Quantidade de Vagas</label>
            <div class="controls">
              <?php echo form_input('quantidade', set_value('quantidade', $acao == 'editar' ? $vaga->vaga_quantidade : ''), 'class="span2"'); ?>
              <span class="help-inline"><?php echo form_error('quantidade'); ?></span>
            </div>
      </div>
      <div class="control-group">
            <div class="controls">
              <label for="destaqueimagem" style="float: left;">Destaque</label>
              <?php 
                $data = array(
                  'name'        => 'destaque',
                  'id'          => 'destaque',
                  'value'       => '1',
                  'checked'     => ($acao == 'editar' && $vaga->vaga_destaque == '1') ? TRUE : FALSE,
                  );

              echo form_checkbox($data);
              ?>
              <span class="help-inline"><?php echo form_error('destaque'); ?></span>
     </div>
     </div>
     <?php if($acao == 'editar'): ?>
     <?php if($vaga->vaga_imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/vagas/<?php echo $vaga->vaga_imagem; ?>" alt="<?php echo $vaga->vaga_empresa; ?>" >
     <?php endif; ?>
     <?php endif; ?>
     <div class="control-group">
            <label class="control-label" for="imagem"><?php echo ($acao == 'editar' ? 'Adicionar / Alterar Imagem' : 'Imagem'); ?></label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem'), 'id="datepicker"'); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
  </div>
  <?php echo form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?php echo form_close(); ?> 
</div>