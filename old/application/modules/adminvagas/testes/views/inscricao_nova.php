<div class="row-fluid">
    
    <div class="alert alert-info">
        <h4>Turma: <?php echo $turma->id; ?>
        <br>Treinamento: <?php echo $item->titulo; ?>
        <?php $valor = $turma->valor * $qtd; ?>
        <br>Valor total: R$ <?php if($qtd > 1){echo $valor;} else {echo $turma->valor;} ?>,00
        </h4>
        Esta é a página de inscrição para o treinamento <b>Nome do Treinamento</b><br/>
        Você pode efetuar até 5 inscrições diferentes incluindo a sua.<br/>
        As inscrições serão efetivadas após a confirmação do pagamento.
    </div>
    
    <h3>Quantos participantes deseja inscrever?</h3>
        <select id="qtd-inscricao">
        <option value="">Selecione uma opção</option>
        
        <option <?php if($qtd == '1'){echo 'selected="selected"';} ?> value="http://localhost/dsrh/interagir/inscricao/nova/2001/1/<?php if(isset($self)){echo 'self';} ?>">1 participante</option>
        
        <option <?php if($qtd == '2'){echo 'selected="selected"';} ?> value="http://localhost/dsrh/interagir/inscricao/nova/2001/2/<?php if(isset($self)){echo 'self';} ?>">2 participantes</option>
        
        <option <?php if($qtd == '3'){echo 'selected="selected"';} ?> value="http://localhost/dsrh/interagir/inscricao/nova/2001/3/<?php if(isset($self)){echo 'self';} ?>">3 participantes</option>
        
        <option <?php if($qtd == '4'){echo 'selected="selected"';} ?> value="http://localhost/dsrh/interagir/inscricao/nova/2001/4/<?php if(isset($self)){echo 'self';} ?>">4 participantes</option>
        
            
        <option <?php if($qtd == '5'){echo 'selected="selected"';} ?>  value="http://localhost/dsrh/interagir/inscricao/nova/2001/5/<?php if(isset($self)){echo 'self';} ?>">5 participantes</option>
       
        <option value="">Desejo inscrever mais de 5 participantes</option>
        </select> 
    
    
    
    <?php if($qtd >= '1'): ?>
    <h3>Você participará do treinamento ou a inscrição é apenas para terceiros?</h3>
    <select id="auto-inscricao">
    <option value="">Selecione uma opção</option>
    <option <?php if(isset($self)){echo 'selected="selected"'; } ?> value="http://localhost/dsrh/interagir/inscricao/nova/2001/<?php if(isset($qtd)){echo $qtd; } else {echo '1';} ?>/self/">Sim, eu participarei</option>
    <option value="http://localhost/dsrh/interagir/inscricao/nova/2001/<?php echo $qtd; ?>/">Não, estou inscrevendo outras pessoas</option>
    </select>
    <?php echo form_open('inscricao/processa'); ?>
    <?php 
    
        if(isset($self))
        {
        $nomeself = array(
            'name' => 'nome[]',
            'value' => $self->nome,
        );
        $emailself = array(
            'name' => 'email[]',
            'value' => $self->email,
        );
        }

        $nome = array(
            'name' => 'nome[]',
            'value' => '',
        );
        $rg = array(
            'name' => 'rg[]',
            'value' => '',
        );
        $email = array(
            'name' => 'email[]',
            'value' => '',
        );
        $telefone = array(
            'name' => 'telefone[]',
            'value' => '',
        );
        $empresa = array(
            'name' => 'empresa[]',
            'value' => '',
        );
        ?>
        Participante 1
        <?php
        if(isset($self)){
            echo form_label('Nome');
            echo form_input($nomeself);
            echo form_label('RG');
            echo form_input($rg);
            echo form_label('Email');
            echo form_input($emailself);
            echo form_label('Telefone');
            echo form_input($telefone);
            echo form_label('Empresa');
            echo form_input($empresa);
        }
        else
        {
        echo form_label('Nome');
        echo form_input($nome);
        echo form_label('RG');
            echo form_input($rg);
            echo form_label('Email');
            echo form_input($email);
            echo form_label('Telefone');
            echo form_input($telefone);
            echo form_label('Empresa');
            echo form_input($empresa);
        }
       
        echo form_hidden('turma_id', $id); ?>
        
        <?php if($qtd >= '2'): ?>
        <h3>Participante 2</h3>
        <?php 
        
        echo form_label('Nome');
        echo form_input($nome);
        echo form_label('RG');
            echo form_input($rg);
            echo form_label('Email');
            echo form_input($email);
            echo form_label('Telefone');
            echo form_input($telefone);
            echo form_label('Empresa');
            echo form_input($empresa);
        
        ?>
        <?php endif; ?>
        
         <?php if($qtd >= '3'): ?>
        <h3>Participante 3</h3>
        <?php 
        
        echo form_label('Nome');
        echo form_input($nome);
        echo form_label('RG');
            echo form_input($rg);
            echo form_label('Email');
            echo form_input($email);
            echo form_label('Telefone');
            echo form_input($telefone);
            echo form_label('Empresa');
            echo form_input($empresa);
        
        ?>
        <?php endif; ?>
        
          <?php if($qtd >= '4'): ?>
        <h3>Participante 4</h3>
        <?php 
        
        echo form_label('Nome');
        echo form_input($nome);
        echo form_label('RG');
            echo form_input($rg);
            echo form_label('Email');
            echo form_input($email);
            echo form_label('Telefone');
            echo form_input($telefone);
            echo form_label('Empresa');
            echo form_input($empresa);
        
        ?>
        <?php endif; ?>
        
          <?php if($qtd >= '5'): ?>
        <h3>Participante 5</h3>
        <?php 
        
        echo form_label('Nome');
        echo form_input($nome);
        echo form_label('RG');
            echo form_input($rg);
            echo form_label('Email');
            echo form_input($email);
            echo form_label('Telefone');
            echo form_input($telefone);
            echo form_label('Empresa');
            echo form_input($empresa);
        
        ?>
        <?php endif; ?>
        
        
        
        <div class="clearfix"></div>
        <?php echo form_submit('', 'Finalizar','class="btn btn-success"');
        
        echo form_close();
        ?>
          

    

    <?php endif; ?>
</div>
</div>