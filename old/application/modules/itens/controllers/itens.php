<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Itens extends MX_Controller
{
	

	function index(){
            
            redirect('');
            
        }
        
    
        
        function detalhe($slug = FALSE)
        {
            $slug = $this->uri->segment(3);
            if (!$slug) 
            {
                redirect('');
            }
            else
            {
                $this->load->model('item'); 
                $this->load->model('turma');
                if($this->item->get_item($slug))
                { 
                    $data['item'] = $this->item->get_item($slug);
                
                $id = $this->item->get_id($slug);
                
                $data['metas'] = array(
                    'title' => '<meta name="title" content="Instituto Interagir - '. $data['item']->titulo .'">',
                    'description' => '<meta name="description" content="'. strip_tags($data['item']->resumo) .'">',
                    'tags'  =>  '<meta name="tags" content="' . $data['item']->tags . '">',
                );
                $data['fb'] = array(
                	'title' => $data['item']->titulo,
                	'description' => strip_tags($data['item']->resumo),
                	'image'	=> base_url() . 'assets/img/itens/' . $data['item']->imagem,
                );
                
                $data['turmas'] = $this->item->get_item_turmas_limit($id);
                $data['title'] = 'Instituto Interagir - ' . $data['item']->titulo;
                $data['main_content'] = 'item_view';
                $this->load->view('includes/template', $data);
                }
                else
                {
                  $this->session->set_flashdata('error', 'Treinamento não encontrado. Verifique a url digitada ou use nossa busca.');
                  redirect();

                }
            }
        }
        
        
        
        function inscreve($id)
        {
        
            if (!$this->tank_auth->is_logged_in()) 
            {
                $this->session->set_userdata('bounce_uri',$this->uri->uri_string());
                redirect('');
            } 
            else    
            {
                       $this->load->model('turma');
                       if($this->turma->check_turma($id)){
                           $data['id'] = $id;
                       }
                       $data['title'] = 'Instituto Interagir - Inscriçao';
                       $data['main_content'] = 'inscricao_view';
                       $this->load->view('includes/template', $data);       
            }
        }
        
        
        
        
        
     
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */