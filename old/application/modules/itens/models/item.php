<?php
/*
 * @var string $horas
 */
class Item extends DataMapper {
    
    var $table = 'itens';
    var $has_many = array("turma");
    
     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    
    function save_item($dados){
        
        $item = new Item();
        
        $item->tipo = $dados['tipo'];
        $item->titulo = $dados['titulo'];
        $item->horas = $dados['horas'];
        $item->slug = $dados['slug'];
        $item->objetivos = $dados['objetivos'];
        $item->programa = $dados['programa'];
        $item->metodologia = $dados['metodologia'];
        $item->tags = $dados['tags'];
        $item->publico = $dados['publico'];
        $item->resumo = $dados['resumo'];
        
        if($item->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }
       
    }
    
    function update_item($id, $dados){
        
        $item = new Item();
        $item->where('id', $id);
        $update = $item->update(array(
            
            'titulo' => $dados['titulo'],
            'tipo' => $dados['tipo'],
            'horas' => $dados['horas'],
            'slug' => $dados['slug'],
            'objetivos' => $dados['objetivos'],
            'programa' => $dados['programa'],
            'metodologia' => $dados['metodologia'],
            'tags' => $dados['tags'],
            'publico' => $dados['publico'],
            'resumo' => $dados['resumo'], 
            
            ));
        
        return $update;
       
    }
    
    function delete_item($id){
        $item = new Item();
        $item->where('id', $id)->get();
        

        if($item->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
    
    function get_item($slug)
    {
        $item = new Item();
        $item->where('slug', $slug);
        $item->limit(1);
        $item->get();
        
        if($item->exists())
        {
            return $item;
        }
        else
        {
            return FALSE;
        }
    }
    
    function get_item_by_id($id)
    {
        $item = new Item();
        $item->where('id', $id);
        $item->limit(1);
        $item->get();
        
        if($item->exists())
        {
            return $item;
        }
        else
        {
            return FALSE;
        }
    }
    
    
    
    
    function get_id($slug)
    {
        $i = new Item();
        $i->where('slug', $slug);
        $i->limit(1);
        $i->get();
        
        return $i->id;
    }
    
    function get_itens_tipo($tipo)
    {
        $i = new Item();
        $i->where('tipo', $tipo);
        $i->get();
        
        
        $arr = array();
        foreach($i->all as $itens)
        {
            $arr[] = $itens;
        }
        
        return $arr;
        
    }
    
    function get_item_turmas($id)
    {
        $item = new Turma();
        $item->where('item_id', $id);
        $item->get();
        
        $arr = array();
        foreach($item->all as $turma)
        {
            $arr[] = $turma;
        }
        
        return $arr;
    }
    
	
    function get_item_turmas_limit($id)
    {
        $item = new Turma();
        $item->where(array('item_id' => $id, 'data >=' => date('Y-m-d'), 'data <' => date("Y-m-d", strtotime("+2 month"))));
        $item->get();
        
        $arr = array();
        foreach($item->all as $turma)
        {
            $arr[] = $turma;
        }
        
        return $arr;
    }
    
    
    
}

/* End of file employee.php */
/* Location: ./application/moules/atleta/models/atleta_model.php */