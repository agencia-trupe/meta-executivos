<?php
/*
 * @var string $horas
 */
class Aluno extends DataMapper {
    
    var $table = 'alunos';
    var $has_one = array("turma");
    var $auto_populate_has_one = TRUE;
    
     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    
    /**
     *  Retorna os alunos inscritos em determinada turma
     * 
     * @param   int
     * @return	array
     * 
     */
    
    function get_pagos_turma($turma_id){
        
        $a = new Aluno();
        $a->where(array('turma_id' => $turma_id, 'status' => '1'))->get();
        
        $arr = array();
        foreach($a->all as $aluno)
        {
            $arr[] = $aluno;
        }
        
        return $arr;
        
        
        
        
        
       
    }
    
    function get_alunos_turma($turma_id){
        
        $a = new Aluno();
        $a->where('turma_id', $turma_id)->get();
        
        $arr = array();
        foreach($a->all as $aluno)
        {
            $arr[] = $aluno;
        }
        
        return $arr;
        
        
        
        
        
       
    }
    
    function get_alunos_user($user_id){
        
        $a = new Aluno();
        $a->where('user_id', $user_id);
        $a->where('status !=', '10');
        $a->order_by('status', 'asc');
                $a->get();
                
        
        $arr = array();
        foreach($a->all as $aluno)
        {
            $arr[] = $aluno;
        }
        
        return $arr;
        
        
        
        
        
       
    }
    
    
    
    /**
     *  Cria os novos alunos no banco de dados
     * 
     * @param   array
     * @param   array
     * @return	bool
     * 
     */
    
    function batch_save($alunodata, $itemdata, $hash)
    {
        
            $tamanho = $itemdata['quantidade'] - 1;
            
            for($f=0; $f<=$tamanho; $f++){
                
                $a = new Aluno();
                
                $a->nome = $alunodata['nome'][$f];
                $a->email = $alunodata['email'][$f];
                $a->rg = $alunodata['rg'][$f];
                $a->empresa = $alunodata['empresa'][$f];
                $a->telefone = $alunodata['telefone'][$f];
                $a->user_id = $itemdata['user_id'];
                $a->valor = $itemdata['valor'];
                $a->turma_id = $itemdata['turma'];
                $a->status = 10;
                $a->data_cadastro = time();
                $a->hash = $hash;
                
                
                if($a->save())
                {
                    $result = TRUE;
                }
         
            }
            
            return $result;
            
            
    }
    
    /**
     * Atualiza o status dos alunos de acordo com o retorno enviado pelo 
     * pagamento digital.
     * 
     * @param   array
     * @param   array
     * @return	bool
     * 
     */
    
    function update_by_hash($hash, $status)
    {
        if($status == 0)
			{                
                $a = new Aluno();
                
                $a->where('hash', $hash);
                $query = $a->update('status', '0');
                
                
                if($query)
                {
                    $result = TRUE;
                }
         
            
            }
            return $result;
            
            
    }
    
     function batch_update($retorno, $status)
    {
        
           
            $tamanho = count($retorno['nome']) - 1;
            
            for($f=0; $f<=$tamanho; $f++){
                
                $a = new Aluno();
                
                $a->nome = $retorno['nome'][$f];
                $a->email = $retorno['email'][$f];
                $a->rg = $retorno['rg'][$f];
                $a->empresa = $retorno['empresa'][$f];
                $a->telefone = $retorno['telefone'][$f];
                $a->status = $status;
                
                
                if($a->save())
                {
                    $result = TRUE;
                }
         
            }
            
            return $result;
            
            
    }
    
    
    
    
    
    function get_turmas($id)
    {
        $item = new Turma();
        $item->where('item_id', $id);
        $item->get();
        
        $arr = array();
        foreach($item->all as $turma)
        {
            $arr[] = $turma;
        }
        
        return $arr;
    }
    
    
}

/* End of file employee.php */
/* Location: ./application/moules/atleta/models/atleta_model.php */