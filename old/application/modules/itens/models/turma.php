<?php

class Turma extends DataMapper {
    
    var $table = "turmas";
    var $has_one = array("item", "consultor");
    var $has_many = array("aluno");
    var $auto_populate_has_many = TRUE;
    var $auto_populate_has_one = TRUE;
    
     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    
    
    function get_vagas_by_id($id)
    {
        $turma = new Turma();
        $turma->where('id', $id)->limit(1)->get();
        
        $vagas = $turma->vagas;
    }
    
    function get_turmas_pdf()
    {
        $item = new Turma();
        $item->where(array('data >=' => date('Y-m-d'), 'data <' => date("Y-m-d", strtotime("+2 month"))));
        $item->order_by('data', 'asc');
        $item->get();
        
        $arr = array();
        foreach($item->all as $turma)
        {
            $arr[] = $turma;
        }
        
        return $arr;
    }
    
    function get_turmas($limit, $offset)
    {
        $turma = new Turma();
        $turma->order_by('data', 'asc');
        $turma->get($limit, $offset);
        
        $arr = array();
        foreach($turma->all as $turmas)
        {
            $arr[] = $turmas;
        }
        
        return $arr;
        
        
    }
    function get_turmas_home($limit, $offset)
    {
        $item = new Turma();
        $item->where(array('data >=' => date('Y-m-d'), 'data <' => date("Y-m-d", strtotime("+2 month"))));
        $item->order_by('data', 'asc');
        $item->get($limit, $offset);
        
        $arr = array();
        foreach($item->all as $turma)
        {
            $arr[] = $turma;
        }
        
        return $arr;
    }
    
     function delete_turma($id){
        $item = new Item();
        $item->where('id', $id)->get();
        

        if($item->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
    
    
    function update_turma($id, $dados){
        
        $turma = new Turma();
        $turma->where('id', $id);
        $update = $turma->update(array(
            
            'item_id' => $dados['item_id'],
            'consultor_id' => $dados['consultor_id'],
            'data' => $dados['data'],
            'vagas' => $dados['vagas'],
            'horario' => $dados['horario'],
            'valor' => $dados['valor'],
            'local' => $dados['local'],
            'endereco' => $dados['endereco'],
            'mapa' => $dados['mapa'],
            
            
            ));
        
        return $update;
       
    }
    
    function save_turma($dados){
        
        $turma = new Turma();
        
        $turma->item_id = $dados['item_id'];
        $turma->consultor_id = $dados['consultor_id'];
        $turma->data = $dados['data'];
        $turma->local = $dados['local'];
        $turma->endereco = $dados['endereco'];
        $turma->valor = $dados['valor'];
        $turma->horario = $dados['horario'];
        $turma->mapa = $dados['mapa'];
        $turma->vagas = $dados['vagas'];
        
        if($turma->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }
       
    }
    
    function get_item_turmas($id)
    {
        $item = new Turma();
        $item->where('item_id', $id);
        $item->get();
        
        $arr = array();
        foreach($item->all as $turma)
        {
            $arr[] = $turma;
        }
        
        return $arr;
    }
    
    function get_turma($id)
    {
        $turma = new Turma();
        $turma->where('id', $id);
        $turma->limit(1);
        $turma->get();
        
        return $turma;
    }
    
    function get_item_id($id)
    {
        $turma = new Turma();
        $turma->where('id', $id);
        $turma->limit(1);
        $turma->get();
        
        return $turma->item_id;
    }
    
    function check_turma($id)
    {
        $turma = new Turma();
        $turma->where('id', $id);
        $turma->limit(1);
        $turma->get();
        
        if($turma->exists())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    
}

/* End of file employee.php */
/* Location: ./application/moules/atleta/models/atleta_model.php */