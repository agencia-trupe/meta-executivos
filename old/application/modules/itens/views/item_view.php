<div id="direita">
    <?php if(isset($item)): ?>
    <div id="curso">
        <div class="lista-mini">
                    <?php if(isset($item->imagem)): ?>
                        <img style="" width="202" height="122" src="<?php echo base_url(); ?>assets/img/itens/<?php echo $item->imagem; ?>" />
                    <?php else: ?>
                        <p>Imagem não disponível.</p>
                    <?php endif; ?>
        </div>
        <header>
            <h3 class="item-tipo"><?php echo $item->tipo; ?></h3>
            <h1 class="item-titulo"><?php echo $item->titulo; ?></h1>
            
                
        </header>
        <div class="clearfix"></div>
               <div id="curso-content">
            <dl>
                
                <dt><i class="icon-calendar"></i> <span>Próximas turmas</span></dt>
                <dd>
                <?php if($turmas != NULL): ?>    
                    <ul>
                        <?php foreach($turmas as $turma): ?>
                        <li><?php echo '<b>Local: </b>' . $turma->local; ?></li> 
                        <li><?php echo '<b>Data: </b>' . date('d/m/Y', strtotime($turma->data)); ?></li>
                        <li><?php echo '<b>Horário: </b>' . $turma->horario . '<br>'; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php else: ?>
                    Não há turmas previstas no momento.<br>
                    Caso tenha interesse nesse curso, envie um email para <a href="mailto:equipe@institutointeragir.com.br">equipe@institutointeragir.com.br</a> sugerindo uma data.
                <?php endif; ?>
                </dd>
                
                
               
                
                
                <dt><i class="icon-time"></i> Carga Horária</dt>
                <dd><?php echo $item->horas; ?> horas</dd>
                
                <dt><i class="icon-screenshot"></i> Objetivo</dt>
                <dd> 
                    <?php echo $item->objetivos; ?>
                </dd>
                
                <dt><i class="icon-user"></i> Público Alvo</dt>
                <dd><?php echo $item->publico; ?></dd>
                
                <dt><i class="icon-list-alt"></i> Programa</dt>
                <dd>
                    <?php echo $item->programa; ?>
                </dd>
                <dt><i class="icon-book"></i> Metodologia</dt>
                <dd>
                    <?php echo $item->metodologia; ?>
                </dd>
            </dl>
        </div>
        
         <div class="curso-actions">
            <?php if($turmas != NULL): ?>
            <div class="btn-group">
                <a class="btn btn-info" href="<?php echo base_url() . 'inscricao/preinscreve/' . $item->id; ?>"><i class="icon-white icon-plus"></i> Mais informações</a>
                            </div><!-- /btn-group -->
            <?php endif; ?>
            <div class="btn-group">
                <button class="btn btn-small btn-info dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-share"></i> Compartilhe <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    
                    <li><a target="_blank" href="http://www.facebook.com/sharer.php?s= 100&p[title]=<?php echo $item->titulo; ?>&p[url]=<?php echo base_url() . $item->tipo .'s/detalhe/'. 
                    $item->slug; ?>&p[images][0]=<?php echo base_url() . 'assets/img/itens/' . $item->imagem; ?>&p[summary]=<?php echo strip_tags($item->resumo); ?>">Facebook</a>
                    </li>
                    
                    
                    <li><a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url() . $item->tipo .'s/detalhe/'. 
                    $item->slug; ?>&title=<?php echo 'Instituto Interagir - ' . $item->titulo; ?>&summary=<?php echo strip_tags($item->resumo); ?>&source=Instituto Interagir
">Linkedin</a></li>
                    
<li><a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo urlencode(base_url() . $item->tipo .'s/detalhe/'. 
                    $item->slug); ?>&via=_interagir&hashtags=<?php echo $item->tipo; ?>">Twitter</a></li>
                    <li class="divider"></li>
                    
                    <li><a href="mailto:?subject=Confira esse treinamento do Instituto Interagir&body=Olá, Confira esse treinamento do Instituto Interagir, clique no link: <?php echo base_url() . $item->tipo .'s/detalhe/'. 
                    $item->slug; ?>" title="Share by Email">Envie por email</a></li>
                </ul>
            </div><!-- /btn-group -->
            <div class="clearfix"></div>
        </div>

         
    </div>
    <?php else: ?>
    <p>Treinamento não encontrado</p>
    <?php endif; ?>
    
</div>						
                <div class="clearfix"></div>

		</div>
                <div id="container-base"></div>
		
