<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Cliente
*
* Model responsável pelas regras de negócio do módulo de Clientes e Cases
* 
* @author Nilton de Freitas - Trupe 
* @link http://trupe.net
*
*/
Class Cliente extends Datamapper{
    var $table = 'cases';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    /**
     * Retorna a lista de clientes/cases
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de clientes ativos
     */
    function get_all($limit, $offset)
    {
        $cliente = new Cliente();
        $cliente->order_by('cases_nome', 'asc');
        $cliente->get($limit, $offset);
        
        $arr = array();
        foreach($cliente->all as $clientes)
        {
            $arr[] = $clientes;
        }
        return $arr;
    }
    /**
     * Retorna os detalhes de um cliente baseado em seu id
     * @param  [int] $id
     * @return [mixed]
     */
    function get_cliente($id)
    {
        $cliente = new Cliente();
        $cliente->where('id', $id);
        $cliente->limit(1);
        $cliente->get();
        if(!$cliente->exists())
        {
            return FALSE; 
        }
        else
        {
            return $cliente;
        }
    }

      function get_servicos($id)
    {
        $cliente = new Cliente();
        $cliente->where('cases_solucao', $id);
        $cliente->order_by('cases_nome', 'asc');
        $cliente->get();
        if(!$cliente->exists())
        {
            return FALSE; 
        }
        else
        {
        $arr = array();
        foreach($cliente->all as $clientes)
        {
            $arr[] = $clientes;
        }
        return $arr;
        }
    }
    /**
     * Retorna um depoimento relacionado a um cliente escolhido de forma randomica
     * @return array
     */
    function get_titulo()
    {
        $cliente = new Cliente();
        $cliente->order_by('id', 'random');
        $cliente->limit(1);
        $cliente->get();
        if(!$cliente->exists())
        {
            return FALSE; 
        }
        else
        {
            return $cliente;
        }
    }
    /**
     * Retorna o registro anterior ao registro cujo id foi passado como parâmetro
     * @param  int $id 
     * @return mixed
     */
    function get_anterior($id)
    {
        $anterior = new Cliente();
        $anterior->order_by('id', 'desc');
        $anterior->where('id <', $id);
        $anterior->limit(1);
        $anterior->get();
        if(!$anterior->exists())
        {
            return FALSE; 
        }
        else
        {
            return $anterior;
        }
    }

    /**
     * Retorna o registro anterior ao registro cujo id foi passado como parâmetro
     * @param  int $id 
     * @return mixed
     */
    function get_proximo($id)
    {
        $proximo = new Cliente();
        $proximo->order_by('id', 'asc');
        $proximo->where('id >', $id);
        $proximo->limit(1);
        $proximo->get();
        if(!$proximo->exists())
        {
            return FALSE; 
        }
        else
        {
            return $proximo;
        }
    }
}