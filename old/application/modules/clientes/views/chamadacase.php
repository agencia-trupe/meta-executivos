<div id="cases-home">
	<h3>Conheça nossos<br>cases de sucesso</h3>
    <div id="cases-home-descricao">
	    <a href="<?php echo site_url('clientes/detalhe/' . $cliente->id); ?>" id="cases-home-img">
	    	<img src="<?php echo base_url(); ?>assets/img/cases/<?php echo $cliente->cases_imagem; ?>" alt="">
	    </a>
	    <a href="<?php echo site_url('clientes/detalhe/' . $cliente->id); ?>" id="cases-home-titulo">
	    	<?php echo $cliente->cases_titulo; ?>
	    </a>
	</div>
	<a class="cases-home-todos" href="<?php echo base_url(); ?>clientes-cases">ver todos »</a>
</div>