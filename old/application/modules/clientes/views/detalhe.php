<div class="clearfix"></div>
<div class="conteudo-clientes-detalhe">
    <div class="texto-right">
        <img src="<?php echo base_url() . 'assets/img/cases/' . $cliente->cases_imagem; ?>" alt="<?php echo $cliente->cases_nome; ?>">
        <div class="clearfix"></div>
        
        <div class="info">
            <h2 class="section"><?php echo $cliente->cases_titulo; ?></h2>
            <?php echo $cliente->cases_desc; ?>
        </div>
        <div class="clearfix"></div>
        <div class="navegacao">
            <ul>
                <?php echo isset($anterior) ? '<li>' . anchor('clientes/detalhe/' . $anterior, '&laquo; anterior') . '</li>' : ''; ?>
                <?php echo isset($proximo) ? '<li>' . anchor('clientes/detalhe/' . $proximo, 'próximo &raquo;') . '</li>' : ''; ?>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>