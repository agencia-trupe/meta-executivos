<?php if (!empty($cliente)): ?>
	    <h2 class="section">Quem já experimentou</h2>
		<?php foreach($cliente as $case): ?>
		<a class="servicos-case-img" href="<?php echo base_url() . 'clientes-cases/detalhe/' . $case->id; ?>">
		<img src="<?php echo base_url() . 'assets/img/cases/' . $case->cases_imagem; ?>" alt="<?php echo $case->clientes_nome; ?>"></a>
		<?php endforeach; ?>
<?php endif; ?>