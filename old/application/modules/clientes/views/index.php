<div class="conteudo-clientes">
    <div class="texto-right">
        <div class="logos">
            <?php foreach($result as $case): ?>
                <div class="case">
                    <a href="<?php echo base_url() . 'clientes-cases/detalhe/' . $case->id; ?>" class="cliente-box">
                        <img width="80px" height="47px" src="<?php echo base_url() . 'assets/img/cases/' . $case->cases_imagem; ?>" alt="<?php echo $case->clientes_nome; ?>" >
                    </a>
                    <p class="descricao">
                        <b><?php echo $case->cases_nome; ?></b>
                    </p>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="clearfix"></div>
        <?php echo $this->pagination->create_links(); ?>
    </div>
</div>
    <div class="clearfix"></div>