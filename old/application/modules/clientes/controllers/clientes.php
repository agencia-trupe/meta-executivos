<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* clientes 
* 
* @author Nilton de Freitas - Trupe 
* @link http://trupe.net
*
*/
Class Clientes extends MX_Controller
{
    public function index()
    {      
        $this->lista();
    }

    /**
     * Verifica se o tipo de cliente passado como parâmetro está no array
     * dos clientes e retorna a lista de clientes do tipo caso verdadeiro.
     * @return mixed Lista de clientes ativos
     */
    public function lista($tipo = NULL)
    {
        //Lógica da paginação de resultados
        $this->load->library('pagination');
        $tipo = $this->uri->segment(2);
        $pagination_config = array(
                    'base_url'       => base_url() . 'clientes-cases/lista/',
                    'total_rows'     => $this->db->get('cases')->num_rows(),
                    'per_page'       => 12,
                    'num_links'      => 5,
                    'next_link'      => 'próximo &raquo;',
                    'prev_link'      => '&laquo; anterior',
                    'first_link'     => FALSE,
                    'last_link'      => FALSE, 
                    'full_tag_open'  => '<div class="pagination center"><ul>',
                    'full_tag_close' => '</ul></div>',
                    'cur_tag_open'   => '<li class="active"><a href="#">',
                    'cur_tag_close'  => '</a></li>',
                    'num_tag_open'   => '<li>',
                    'num_tag_close'  => '</li>',
                    'next_tag_open'   => '<li>',
                    'next_tag_close'  => '</li>',
                    'prev_tag_open'   => '<li>',
                    'prev_tag_close'  => '</li>',
            );
        $this->pagination->initialize($pagination_config);
        //Obtendo resultados no banco
        $this->load->model('clientes/cliente');
        $data['result'] = $this->cliente->get_all($pagination_config['per_page'], $this->uri->segment(2));

        //Layout
        $data['titulo'] = 'Meta Executivos &middot; Clientes | Cases';
        $data['pagina'] = 'clientes';
        $data['header_titulo'] = 'Clientes | Cases';
        $data['conteudo_principal'] = "clientes/index";
        $data['header_image'] = '05.jpg';
        $this->load->view('layout/template', $data);
    }

    /**
     * Exibe a página de detalhes de um case
     * @param  int $id 
     */
    public function detalhe($id = NULL)
    {
        if($id == NULL)
        {
            redirect('clientes');
        }
        else
        {
            $this->load->model('clientes/cliente');
            //Verifica a existencia do cliente/case e retorna ao index caso falso.
            if(!$this->cliente->get_cliente($id))
            {
                redirect('clientes');
            }
            else{
                $data['cliente'] = $this->cliente->get_cliente($id);
                //Verifica a existência de registros anteriores e próximos e os retorna
                //caso seja verdadeiro.
                $anterior = $this->cliente->get_anterior($id);
                $data['anterior'] = $anterior ? $anterior->id : NULL;
                $proximo = $this->cliente->get_proximo($id);
                $data['proximo'] = $proximo ? $proximo->id : NULL;
                //Layout
                $data['titulo'] = 'Meta Executivos &middot; Clientes | Cases - ' . $data['cliente']->nome;
                $data['pagina'] = 'clientes';
                $data['header_titulo'] = 'Clientes | Cases';
                $data['header_image'] = '05.jpg';
                $data['conteudo_principal'] = "clientes/detalhe";
                $this->load->view('layout/template', $data);
            }
        }
    }

    function chamadacase()
    {
        $this->load->model('clientes/cliente');
        $data['cliente'] = $this->cliente->get_titulo();
        $this->load->view('clientes/chamadacase', $data);
    }

    function servicoscases($id)
    {
        $this->load->model('clientes/cliente');
        $data['cliente'] = $this->cliente->get_servicos($id);
        $this->load->view('clientes/servicoscases', $data);
    }
}