<?php

Class Static_pages extends MX_Controller
{

        public function index()
        {
                $this->home();
        }

        public function home()
        {
                $this->load->model('programas/programa');
        	$data['titulo'] = 'Meta Executivos &middot; Home';
                $data['pagina'] = 'home';
                $data['programas'] = $this->programa->get_home();
                $data['header_image'] = '01.jpg';
                $data['conteudo_principal'] = "static_pages/home";
                $this->load->view('layout/template', $data);
        }

        public function perfil()
        {
                $data['titulo'] = 'Meta Executivos &middot; Perfil';
                $data['pagina'] = 'perfil';
                $data['conteudo_principal'] = "static_pages/perfil";
                $data['header_image'] = '02.jpg';
                $data['header_titulo'] = 'Perfil Meta Executivos';
                $this->load->view('layout/template', $data);
        }

        public function empresas($servico = NULL)
        {       
                $servico = ($servico == NULL) ? 'executive-search' : $servico;
                if(in_array($servico, array('executive-search', 'desenvolvimento-organizacional')))
                {
                        $servico = $servico;
                }
                else
                {
                        $servico = 'executive-search';
                }
                $data['titulo'] = 'Meta Executivos &middot; Para Empresas';
                $data['pagina'] = 'empresas';
                $data['header_titulo'] = 'Serviços para empresas';
                $data['servico'] = $servico;
                $data['header_image'] = '03.jpg';
                $data['conteudo_principal'] = 'static_pages/servicos/' . $servico . '_view';
                $this->load->view('layout/template', $data);
        }

        public function clientes()
        {       
                $data['titulo'] = 'Meta Executivos &middot; Cases | Clientes';
                $data['pagina'] = 'clientes';
                $data['conteudo_principal'] = "static_pages/clientes";
                $this->load->view('layout/main', $data);
        }

        public function depoimentos()
        {       
                $data['titulo'] = 'Meta Executivos &middot; Depoimentos';
                $data['pagina'] = 'depoimentos';
                $data['header_titulo'] = 'Depoimentos';
                $data['conteudo_principal'] = "static_pages/depoimentos";
                $this->load->view('layout/template', $data);
        }

        /*public function novidades()
        {       
                $data['titulo'] = 'Meta Executivos &middot; Novidades | Artigos';
                $data['pagina'] = 'novidades';
                $data['conteudo_principal'] = "static_pages/novidades";
                $this->load->view('layout/main', $data);
        }*/

}