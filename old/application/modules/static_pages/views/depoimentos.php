<div class="clearfix"></div>
	<div class="texto-right">
		<h2 class="section">Processo de Coaching</h2>
		<p>
			“A metodologia aplicada pela Meta foi clara e objetiva deste o início dos trabalhos.
			Procuramos focar nos principais gaps das percepções relativas às minhas habilidades
			comparadas à minha percepção, dos subordinados e pares, detectadas em dois assessments
			360 graus, visando definir ações para que eu pudesse melhorar meu desempenho. Com excelentes
			ferramentas de análise, metodologia bem definida e uma profissional extremamente competente,
			o processo de Coaching foi fantástico do inicio ao fim. Estou seguro de que aprendi muito
			neste periodo, principalmente sobre os meus valores que não estavam com o devido ajuste de prioridades.” 
		</p>
		<p><em>Eduardo Rampinelli, gerente geral, Parker Hannifin</em></p>

		<h2 class="section">Executive Search</h2>
		<p>
		   “Em 2010 houve uma reestruturação global da Kodak e coube à Meta Executivos a responsabilidade
		   pelos processos de Recrutamento & Seleção da filial brasileira para os cargos de diretores, gerentes
		   e especialistas, com reporte internacional às unidades da Argentina e Estados Unidos. A assertividade
		   na condução dos processos, bem como no fechamento das posições, aliados à expertise e conhecimento
		   dos consultores foram os diferenciais apresentados pela Meta.”
		</p>
		<p><em>Antonio Herbert Duarte Ferreira, diretor de Recursos Humanos da Kodak no período.</em></p>

		<h2 class="section">Treinamento e Liderança</h2>
		<p>
		   “Contratamos a Meta Executivos para desenvolver o Treinamento de Liderança com o objetivo de
		   praticar de forma eficiente o papel de líder coach, o feedback, identificar e formar sucessores e
		   como desdobrar os objetivos organizacionais entre as equipes. Na prática houve um aprimoramento do
		   líder em como reconhecer o empenho e envolver as equipes na formulação dos planos estratégicos da
		   área, e manter a comunicação aberta e transparente criando um ambiente propício para o desenvolvimento.”  
		</p>
		<p><em>Elmo Garrudo, diretor de Recursos Humanos da Construtora Passarelli</em></p>

		<h2 class="section">Processo de Assessment</h2>
		<p>
			“Contratamos o Grupo Meta RH para a aplicação do processo de assessment para alguns profissionais 
			de nossa força de vendas. O trabalho executado pela equipe da Meta Executivos , proporcionou 
			como resultado a promoção de quatro desses profissionais para cargos de supervisão e coordenação na área comercial”. 
		</p>
		<p><em>Mirian Macedo De S. Amaral, Gerente de Desenvolvimento Organizacional, Qualicorp Administradora de Benefícios</em></p>
	</div>
<div class="clearfix"></div>