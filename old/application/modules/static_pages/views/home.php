<div class="content-left">
    <img id="img-executivos" src="<?php echo base_url(); ?>assets/img/executivos.jpg" alt="Meta Executivos">
    <div id="agilidade">
        <h1>Agilidade e expertise</h1>
        <p>
            A Meta Executivos atua no segmento de hunting e desenvolvimento de 
            executivos e profissionais especializados, com uma equipe altamente 
            qualificada de consultores bilíngues com vasta experiência local e 
            internacional. Em Desenvolvimento, o nosso objetivo é atrair e 
            reter talentos nas organizações através de Assessment, Coaching, 
            Mentoring, Counseling e Programas Taylor Made.
        </p>
    </div>
    <div class="clearfix"></div>

    <div id="separador-home"></div>
    <div class="servicos-home">
        <h1>Nossos Serviços:</h1>
    </div>
    <div class="servicos-nav">
        <ul>
            <li><a href="<?php echo site_url('para-empresas/executive-search'); ?>">Executive Search</a></li>
            <li><a href="<?php echo site_url('para-empresas/desenvolvimento-organizacional'); ?>">Desenvolvimento Organizacional</a></li>
        </ul>
    </div>
    <?php echo Modules::run('clientes/chamadacase'); ?>
</div>
<div class="content-sidebar">
    <?php echo Modules::run('vagas/lista_home'); ?>

    <a href="<?php echo site_url('para-profissionais'); ?>" id="oportunidades-home-mais">Ver mais oportunidades &raquo;</a>

    <div class="clearfix"></div>
    
    <div class="servicos-profissionais-home">
        <img src="<?php echo base_url(); ?>assets/img/servicos-profissionais.jpg" alt="Meta Executivos - Serviços para Profissionais">
        <a href="<?php echo site_url('para-profissionais'); ?>" id="mais">saiba mais &raquo;</a>
    </div>
</div>
<div class="clearfix"></div>