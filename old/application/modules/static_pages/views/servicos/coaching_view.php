<div class="servicos-nav">
    <ul>
        <li><a <?php echo ($servico == 'hunting') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/hunting'); ?>">Search e Hunting</a></li>
        <li><a <?php echo ($servico == 'assessment') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/assessment'); ?>">Assessment</a></li>
        <li><a <?php echo ($servico == 'coaching') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/coaching'); ?>">Coaching</a></li>
        <li><a <?php echo ($servico == 'desenvolvimento-organizacional') ? 'class="active"' : ''; ?>href="<?php echo site_url('para-empresas/desenvolvimento-organizacional'); ?>">Desenvolvimento Organizacional</a></li>
    </ul>
</div>
<div class="texto-right">
    <h2>Coaching</h2>
    
    <h2 class="section">Definição</h2>
    <p>Consultor (Coach) desenvolve uma “parceria” com o indivíduo
        (Coachee) e as organizações (contratantes), objetivando melhoria 
        do desempenho do Coachee. 
    </p>

    <h2 class="section">Objetivo</h2>
    <p> Por meio da Assessoria do Consultor-Coach, ajudamos a alcançar 
        resultados extraordinários, tanto para o indivíduo, 
        quanto para a Organização.
    </p>
    
    <h2 class="section">A função do Coach</h2>
    <p>Trabalhar áreas de competências, carreira, relacionamentos 
        profissionais e incrementar a performance.  
    </p>
    
    <h2 class="section">Apoio ao processo</h2>
    <p>Aplicação de um inventário comportamental, que amplie as informações 
        sobre o Coachee.
    </p>


    <h2 class="section">Nosso diferencial aplicado ao procedimento:</h2> 
    <ul>
        <li>Processo especializado e personalizado: focado na necessidade específica da empresa-cliente e do coachee, com a assessoria de um Consultor-Coach com vivência e certificação internacionais.</li>
        <li>Desenvolvimento: implica em um processo mudança e “crescimento”, reforçando as fortalezas, em tempo real, na quantidade, qualidade e no momento planejado;</li>
        <li>Período: ocorre num determinado espaço de tempo e envolve a estruturação e acompanhamento destas mudanças;</li>
        <li>Foco: nos impactos relacionados à performance;</li>
        <li>Objetivos do Negócio: projeto focado nas necessidades do negócio.</li>
        <li>Nosso diferencial – no processo de acompanhamento profissional assessorado, ao definir as suas metas, ter mais ação, tomar decisões de alta qualidade e a plena utilização de seus pontos fortes, assim como, utilizar suas competências para a alavancagem de resultados pessoais e profissionais.</li>
    </ul>
</div>