<div class="servicos-nav">
    <ul>
        <li><a <?php echo ($servico == 'hunting') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/hunting'); ?>">Search e Hunting</a></li>
        <li><a <?php echo ($servico == 'assessment') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/assessment'); ?>">Assessment</a></li>
        <li><a <?php echo ($servico == 'coaching') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/coaching'); ?>">Coaching</a></li>
        <li><a <?php echo ($servico == 'desenvolvimento-organizacional') ? 'class="active"' : ''; ?>href="<?php echo site_url('para-empresas/desenvolvimento-organizacional'); ?>">Desenvolvimento Organizacional</a></li>
    </ul>
</div>
<div class="texto-right">
    <h2>Assessment</h2>
    <h2 class="section">Definição</h2>
    <p>É um processo de Avaliação de Potencial (por Competências).</p>

    <h2 class="section">Objetivo Principal</h2>
    <p>
        Evidenciar competências (essenciais e estratégicas) para o pleno 
        exercício da nova função, dentro de uma “linha do tempo”.
    </p>
    
    <h2 class="section">Público Alvo</h2> 
    <p>
        Profissionais identificados com alta performance, 
        podendo ser elegíveis para prontidão (imediata, no 
        curto, médio, ou longo prazos). Ou por sucessão dentro da
        Organização (local ou globalmente).
    </p>

    <h2 class="section">Etapas de análise especializada e qualificada</h2>
    <p>
        Por parte de nossos Consultores, que utilizam uma metodologia 
        sistematizada buscando avaliar o profissional por meio de Entrevista 
        (por competências), aplicação de inventário comportamental e/ou estilos, 
        e/ou cases, para evidenciar o perfil de competências, pontos fortes e 
        gaps existentes em relação aos desejados (esperados) para a posição
        em está sendo cotado a assumir. 
    </p>

    <h2 class="section">Ações de desenvolvimento e coaching</h2>
    <p>
        Podem estar previstas pela empresa como formas de superação de gaps. 
        Podemos negociar a inclusão desta etapa como apoio ao avaliado, dentro de 
        uma trajetória de carreira na linha sucessória de posições, tais como: 
        Coordenação, Gerência e Direção, respectivamente. 
    </p>
    
    <h2 class="section">Nossos diferenciais</h2> 
    <p>
        Vão desde a expertise de Consultores habilitados às nossas instalações que são preparadas para que o 
        processo seja feito em local reservado, personalizado e que o 
        profissional possa estar confortável.
    </p>

    <h2 class="section">Quem já experimentou</h2>
    <a class="servicos-case-img" href="<?php echo site_url('clientes-cases/detalhe/10'); ?>">
        <img src="<?php echo base_url(); ?>assets/img/clientes/parker.jpg" alt="Parker">
    </a>
</div>
    <div class="clearfix"></div>
