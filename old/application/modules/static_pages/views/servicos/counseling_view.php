<div class="servicos-nav">
    <ul>
        <li><a <?php echo ($servico == 'hunting') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/hunting'); ?>">Search & Hunting</a></li>
        <li><a <?php echo ($servico == 'assessment') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/assessment'); ?>">Assessment</a></li>
        <li><a <?php echo ($servico == 'coaching') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/coaching'); ?>">Coaching</a></li>
        <li><a <?php echo ($servico == 'counseling') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/counseling'); ?>">Counseling</a></li>
        <li><a <?php echo ($servico == 'desenvolvimento-organizacional') ? 'class="active"' : ''; ?>href="<?php echo site_url('para-empresas/desenvolvimento-organizacional'); ?>">Desenvolvimento Organizacional (D.O.)</a></li>
    </ul>
</div>
<div class="texto-right">
    <h2>Counseling</h2>
    
    <h2 class="section">Definição</h2>
    <p>Assessoria de aconselhamento no ajuste de atitudes e comportamentos de profissionais que necessitam “conhecer a abrangência de seu papel na Organização”, ou seja, em função de gaps significativos e específicos relacionados ao seu autoconhecimento e percepção do ambiente, possam estar sofrendo uma vulnerabilidade em sua posição profissional e pessoal, que poderão trazer impactos negativos ao ambiente de trabalho.  
    
    <h2 class="section">Objetivo</h2>
    <p>Empresas que necessitam recuperar, sinalizar, tentar preservar do risco e trazer “à luz do cotidiano” um profissional (estratégico), que não está atendendo às expectativas em determinado âmbito, no qual transita dentro de sua função (como: gestor, colaborador, par, colega de trabalho, formador de outros profissionais, parceiro e fornecedor) e que por ter uma função estratégica na Empresa, esta deseja mantê-lo, desenvolvê-lo e melhorar sua eficácia de sua convivência/ conduta no âmbito profissional.   

    <h2 class="section">Público Alvo</h2>
    <ul>
        <li>Funcionários em posições de liderança-chave para a Organização;</li>
        <li>Expatriados e sua família que sofreram com mudanças multiculturais;</li>
        <li>Profissionais em fase de pré-aposentadoria; </li>
    </ul>

    <h2 class="section">Complemento ao processo</h2>
    <p>Pode-se optar pela aplicação de uma ferramenta
        ou inventário comportamental e de autoavaliação, conforme decisão a ser 
        compartilhada com o cliente.
    </p>

    <p>
        Ao final do processo será fornecido um relatório geral sobre o estágio
        que se atingiu com o Counseling em comparação com as expectativas que 
        foram contratadas pelo cliente.   
    </p>





</div>