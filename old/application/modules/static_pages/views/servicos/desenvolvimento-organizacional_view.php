<div class="servicos-nav">
    <ul>
        <li><a <?php echo ($servico == 'executive-search') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/executive-search'); ?>">Executive Search</a></li>
        <li><a <?php echo ($servico == 'desenvolvimento-organizacional') ? 'class="active"' : ''; ?>href="<?php echo site_url('para-empresas/desenvolvimento-organizacional'); ?>">Desenvolvimento Organizacional</a></li>
    </ul>
</div>
<div class="texto-right">
    <h2>Desenvolvimento Organizacional</h2>
    
    <p>
        Atuamos de forma customizada buscando otimizar resultados para as empresas e para seus 
        profissionais. Nossas soluções de desenvolvimento são construídas a partir de um diagnóstico 
        que considera a estratégia da organização e o desempenho esperado dos profissionais, 
        líderes e times.
    <p>
        Com base nesse diagnóstico, sugerimos o método: <em>coaching</em>, <em>assessment</em>, 
        <em>workshop</em>, treinamento ou um conjunto de soluções integradas que melhor se adequem à situação 
        para atingir os resultados esperados.
    </p>
    <p class="servicos-sub">Oferecemos:</p>
    <ul>
        <li>Coaching Executivo para todos os níveis de gestão</li>
        <li>Coaching de Times locais e internacionais, <em>cross functional</em> ou virtuais</li>
        <li><em>Assessment</em> de competências comportamentais</li>
        <li>Cursos, Workshops e Palestras para desenvolvimento de líderes, indivíduos e times.</li>
    </ul>
    </p>

    <h2 class="section">Coaching</h2>
    <p class="servicos-sub">Coaching Executivo</p>
    <p>
        Atendemos a todos os níveis das organizações, de colaboradores individuais à diretoria executiva. 
        Ajudamos a desenvolver as competências, <em>insights</em> e autoconsciência que os profissionais necessitam 
        para alavancar e sustentar o seu sucesso e das suas organizações.
    </p>
    <p class="servicos-sub">Coaching de Times locais e internacionais, cross functional ou virtuais</p>
    <p>
        Alavancamos o desempenho dos times promovendo alinhamento, comprometimento, engajamento e 
        <em>accountability</em> dos seus membros e foco nas metas e resultados.
    </p>

    <h2 class="section">Assessment de competências comportamentais</h2>
    <p>
        O <em>assessment</em> é utilizado para apoiar as ações de desenvolvimento de lideres e colaboradores 
        praticadas nas empresas que utilizam as melhores práticas de gestão de pessoas. Com o <em>assessment</em> 
        as empresas podem diagnosticar as necessidades de desenvolvimento e, assim, tomar ações eficazes, 
        pois possibilita uma avaliação mais eficiente das competências potenciais e atuais do indivíduo para 
        o desempenho das tarefas requeridas em cada cargo ou função.
    </p>
    <p>
        Algumas das ferramentas de <em>assessment</em> que utilizamos são: MBTI, DISC, Talent Insights.
    </p>

    <h2 class="section">Cursos e Workshops</h2>
    <h3 class="cursos-titulo">» Cursos e Workshops para Desenvolvimento de Líderes</h3>
    <div class="cursos-interna">
    <p class="servicos-sub">1. Competências essenciais de liderança: transição de colaborador individual a gestor de pessoas</p>
    <p>
        Este curso visa conscientizar líderes recentes ou potenciais sobre a diferença entre ser 
        liderado e ser líder e sobre as responsabilidades e competências exigidas à função de líder. 
        Os participantes aprenderão a desenvolver pessoas e a mudar o foco de resultados individuais 
        para resultados do time.  
    </p>
    <p style="margin-bottom: 22px;">
        Público-alvo: gestores recém-promovidos ou líderes potenciais.
    </p>
    <p class="servicos-sub">2. Liderança para maximizar performance</p>
    <p>
        Workshop para gestores orientado a otimizar seu desempenho como gestor. Os assuntos abordados variam
         de acordo com o diagnóstico e necessidade específicos dos públicos-alvo. Workshop com conteúdo 
         prático que estimula a troca de ideias entre os gestores e enfoca situações reais vividas pelos 
         participantes sobre temas como liderança situacional®, comunicação e feedback, gestão de conflito,
          gestão de clima, inteligência emocional, motivação e retenção de talentos, diversidade,  delegação 
          e <em>empowerment</em>, desenvolvimento de pipeline de sucessão. 
    </p>
    <p>
        Público-alvo: gestores que já exercem liderança desde coordenação/supervisão até diretoria executiva. O conteúdo do workshop é customizado ao público específico.
    </p>
    </div>

    <h3 class="cursos-titulo">» Cursos e Workshops para Desenvolvimento de Times</h3>
    <div class="cursos-interna">
    <p class="servicos-sub">1. Team coaching: Desenvolvimento de Equipes de Alta Performance</p>
    <p>Workshop prático e vivencial no qual o time alinha, com auxilio do facilitador que exerce papel de coach, seus objetivos, regras, valores, papéis e responsabilidades, processos de comunicação e interação entre os membros, diagnóstico de pontos fortes e pontos a melhorar e traça planos de ação para tornar-se efetivamente uma equipe de alta performance. O time utiliza e aplica ferramentas práticas que possibilitam continuar seu desenvolvimento após o workshop.</p>
    <p style="margin-bottom: 22px;">Público-alvo: equipes no estágio de formação que queiram assegurar alta performance, equipes já formadas que queiram alavancar seu desempenho ou equipes multifuncionais que necessitem melhorar seu alinhamento.</p>
    <p class="servicos-sub">2. Teambulding</p>
    <p>Workshop de integração e construção de identidade do time com utilização de jogos e vivências práticas.</p>
    <p style="margin-bottom: 22px;">Público-alvo: times no estágio de formação ou times já formados que queiram melhorar a integração entre seus membros.</p>
    <p class="servicos-sub">3. Times Virtuais e Multiculturais</p>
    <p>Workshop prático sobre como gerir e atuar como membro de times virtuais e multiculturais. Como desenvolver processos de comunicação, conduzir reuniões eficazes, motivar, lidar com diferenças culturais entre diferentes regiões ou países, assegurar <em>accountability</em> dos membros da equipe e gerar resultados.</p>
    <p>Público-alvo: Gestores e membros de times virtuais.</p>
    </div>

    <h3 class="cursos-titulo">» Cursos e Workshops para Desenvolvimento de Comunicação</h3>
    <div class="cursos-interna">
    <p class="servicos-sub">1. Comunicação e Influência</p>
    <p>Workshop que ensina técnicas para melhorar a comunicação e influência com os diversos públicos e <em>stakeholders</em> das organizações.</p>
    <p style="margin-bottom: 22px;">Público-alvo: membros de times, gestores de projetos e colaboradores individuais.</p>
    <p class="servicos-sub">2. Apresentações de Sucesso</p>
    <p>Workshop prático e vivencial que visa elevar a qualidade e a eficácia das apresentações e estimular as atitudes favoráveis à venda de conceitos, produtos e serviços. Os participantes aprenderão e utilizarão técnicas para desenvolver apresentações de forma a potencializar as competências adquiridas, ampliar a consciência sobre a importância do papel do apresentador e estimular sua motivação e entusiasmo e consequentemente alavancar sua capacidade de influência.</p>
    <p style="margin-bottom: 22px;">Público-alvo: qualquer colaborador que queira melhorar o desempenho das suas apresentações.</p>
    <p class="servicos-sub">3. Formação de instrutores e multiplicadores de treinamento</p>
    <p>Workshop vivencial com o objetivo de ensinar técnicas de facilitação de treinamento que incluem desde a preparação e planejamento à condução e facilitação de grupos.</p>
    <p>Público-alvo: colaboradores que queiram ou necessitem atuar como instrutores ou multiplicadores de treinamento.</p>
    </div>

    <h3 class="cursos-titulo">» Cursos e Workshops para Desenvolvimento de Eficácia Pessoal</h3>
    <div class="cursos-interna">
    <p class="servicos-sub">1. Resiliência: como lidar com pressões e superar desafios</p>
    <p>Resiliência é a capacidade de voltar ao estado natural após ter sofrido um choque. É ter inteligência prática para perseverar, encontrar novas soluções e superar desafios.</p>
    <p>Esse workshop ensina a como aumentar a capacidade de lidar com mudanças e pressões do ambiente e de encontrar soluções positivas para problemas e desafios.</p>
    <p>Os participantes aumentam sua autoconsciência em relação ao que pensam e sentem e como lidam com o estresse. Aprendem a identificar seus modelos mentais e a utilizar o pensamento positivo e criativo para lidar com situações difíceis. Aprendem também a controlar suas emoções e a lidar com o estresse para atingir os resultados desejados.</p>
    <p>Os líderes, em especial, entendem o impacto das suas ações e emoções sobre a produtividade do seu time e a como assegurar um clima de trabalho positivo.</p>
    <p style="margin-bottom: 22px;">Público-alvo: qualquer colaborador ou líder que queira aumentar seu grau de resiliência e assegurar resultados positivos.</p>
    <p class="servicos-sub">2. Lidere a si mesmo</p>
    <p>O workshop Lidere a si mesmo está desenhado para orientar os profissionais a compreenderem e se comprometerem com os cinco pontos-chave que vão garantir melhoria de performance.</p>
    <ul>
        <li>Ser Proativo</li>
        <li>Ter Visão/Senso de Propósito</li>
        <li>Ter Atitude de Aprendiz</li>
        <li>Questionar Modelos Mentais</li>
        <li>Ter Autocontrole</li>
    </ul>
    <p style="margin-top: 16px;">Os comportamentos decorrentes dessa compreensão são traduzidos para o dia a dia de trabalho, promovendo um melhor domínio do ambiente e o desenvolvimento de uma postura e mentalidade de maior responsabilidade pelos resultados.</p>
    <p style="margin-bottom: 22px;">Público-alvo: colaboradores e líderes que queiram melhorar sua eficácia pessoal.</p>
    <p class="servicos-sub">3. Priorização e Gestão Eficaz do Tempo</p>
    <p>Esse curso ensina os participantes a priorizarem seus compromissos de acordo com o grau de urgência e importância em relação às suas metas e objetivos.</p>
    <p>Público-alvo: colaboradores que queiram administrar suas prioridades de forma mais eficaz.</p>
    </div>

    <h3 class="cursos-titulo">» Cursos e Workshops para Desenvolvimento de Relacionamento com Clientes</h3>
    <div class="cursos-interna">
    <p class="servicos-sub">1. Excelência no Atendimento – presencial ou por telefone</p>
    <p>Neste workshop prático os participantes aprenderão o ciclo de relacionamento com o cliente, ou seja, preparação, estabelecer sintonia, ouvir o cliente, ter empatia, reconhecer os diversos tipos de clientes, lidar com clientes difíceis, transmitir informações com segurança e credibilidade. São utilizados casos práticos com situações reais.</p>
    <p style="margin-bottom: 22px;">Público-alvo: Colaboradores que necessitam melhorar a excelência no atendimento.</p>
    <p class="servicos-sub">2. Gestão da qualidade em serviços</p>
    <p>Neste workshop os gestores aprendem como gerenciar a qualidade dos serviços e dos momentos da verdade com os clientes. O workshop é customizado para focar a realidade do negócio e dos participantes.</p>
    <p style="margin-bottom: 22px;">Público-alvo: gestores que desejam aumentar seus conhecimentos sobre gestão em qualidade em serviço.</p>
    <p class="servicos-sub">3. Vendas Consultivas e Negociação</p>
    <p>Nesse workshop os participantes aprendem técnicas e simulam situações reais de como vender de forma consultiva entendendo as necessidades dos clientes, lidando com objeções e oferecendo soluções ganha-ganha.</p>
    <p>Público-alvo: colaboradores que queiram aumentar suas habilidades de vendas consultivas.</p>
    </div>
    <?php echo Modules::run('clientes/servicoscases','DO'); ?>
</div>
<div class="clearfix"></div>