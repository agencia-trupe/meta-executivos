<div class="servicos-nav">
    <ul>
        <li><a <?php echo ($servico == 'hunting') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/hunting'); ?>">Search e Hunting</a></li>
        <li><a <?php echo ($servico == 'assessment') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/assessment'); ?>">Assessment</a></li>
        <li><a <?php echo ($servico == 'coaching') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/coaching'); ?>">Coaching</a></li>
        <li><a <?php echo ($servico == 'desenvolvimento-organizacional') ? 'class="active"' : ''; ?>href="<?php echo site_url('para-empresas/desenvolvimento-organizacional'); ?>">Desenvolvimento Organizacional</a></li>
    </ul>
</div>
<div class="texto-right">
    <h2>Search e Hunting</h2>
    <p>
        Executive Search – processo estruturado, focado na captação e seleção 
        especializada/ personalizada de profissionais para posições Gerenciais e 
        Executivas, que possuam competências agregadoras ao perfil e necessidades 
        de nossas empresas-clientes.  
    </p>
    
    <p>Metodologia assegurada e aplicada por Consultores Seniores:</p>
    <ul>
        <li>Entrevistas por competências (1h30min);</li>
        <li>Teste de proficiência em idioma(s);</li>
        <li>Aplicação de inventário comportamental/estilos;</li>
        <li>Aplicação de estudos de caso ou games; </li>
        <li>Laudo completo – personalizado.</li>
        <li>Independente de ser feito o processo seletivo completo, 
            podemos oferecer (negociar) opções específicas.</li>
    </ul>
    
    <p>Exemplo: inclusão de um short list.</p>
    <ul>    
        <li>Mapeamento completo – da estrutura, dos profissionais e/ou por empresas;</li>
        <li>Processo de hunting (com abordagem); </li>
    </ul>
    
    <h2 class="section">Diferencial agregado</h2>
    <p>
        Todo o processo é planejado, compartilhado e aprovado previamente, em parceria com o cliente, incluindo a definição e necessidade de aplicação de ferramentas de avaliação específicas, comportamentais e de estilos de liderança. Nossa abordagem no atendimento junto aos nossos clientes é diferenciada, com estreita proximidade à sua realidade, valores e expectativas.
    </p>
    <p>
        Esta negociação e definição prévia de plano de busca feita junto aos nossos clientes visa o atendimento aos prazos e exigências específicas do negócio.
    </p>
    <p>    
        Garantimos a reposição do profissional selecionado, dentro do prazo acordado.        
    </p>


    <h2 class="section">Quem já experimentou</h2>
    <a class="servicos-case-img" href="<?php echo site_url('clientes-cases/detalhe/9'); ?>">
        <img src="<?php echo base_url(); ?>assets/img/clientes/indusval.jpg" alt="Parker">
    </a>
    <a class="servicos-case-img" href="<?php echo site_url('clientes-cases/detalhe/4'); ?>">
        <img src="<?php echo base_url(); ?>assets/img/clientes/kodak.jpg" alt="KODAK">
    </a>
    <a class="servicos-case-img" href="<?php echo site_url('clientes-cases/detalhe/8'); ?>">
        <img src="<?php echo base_url(); ?>assets/img/clientes/monsanto.jpg" alt="Monsanto">
    </a>
    <a class="servicos-case-img" href="<?php echo site_url('clientes-cases/detalhe/7'); ?>">
        <img src="<?php echo base_url(); ?>assets/img/clientes/qualicorp.jpg" alt="Qualicorp">
    </a>
    <a class="servicos-case-img" href="<?php echo site_url('clientes-cases/detalhe/5'); ?>">
        <img src="<?php echo base_url(); ?>assets/img/clientes/vitro.jpg" alt="Vitro">
    </a>
</div>
    <div class="clearfix"></div>