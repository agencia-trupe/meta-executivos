<div class="servicos-nav">
    <ul>
        <li><a <?php echo ($servico == 'executive-search') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas/executive-search'); ?>">Executive Search</a></li>
        <li><a <?php echo ($servico == 'desenvolvimento-organizacional') ? 'class="active"' : ''; ?>href="<?php echo site_url('para-empresas/desenvolvimento-organizacional'); ?>">Desenvolvimento Organizacional</a></li>
    </ul>
</div>
<div class="texto-right">
    <h2>Executive Search</h2>
    <p>
        Processo estruturado e customizado, focado na identificação, atração e 
        seleção de profissionais para posições de Gestão e Executivas, que possuam 
        competências agregadoras ao perfil e necessidades de nossos clientes.
    </p>
    <p>
        Todo o processo é planejado, compartilhado e aprovado previamente, em 
        parceria com o cliente, incluindo a definição e necessidade de aplicação de 
        ferramentas de avaliação específicas, comportamentais e de estilos de liderança.
    </p>
    <p>
        Cada processo está alinhado à visão, missão, cultura, valores e estratégias do 
        cliente, visando contribuir para os resultados da organização.
    </p>

    <h2 class="section">Processo de hunting</h2>
    <p>
        Contempla moderna metodologia de trabalho com estratégias que nos permitem encontrar 
        para o cliente, o profissional único, que atenda a todas as suas exigências dentro de 
        altos padrões de assertividade e qualidade.
    </p>

    <h2 class="section">Mapeamento de Mercado</h2>
    <p>
        Estudo detalhado do mercado em empresas pré-definidas pelo cliente e de algumas características 
        dos profissionais que se pretende buscar. Com base neste estudo será elaborado um organograma 
        das empresas alvo, assim como um relatório detalhando todas as informações de carreira dos 
        profissionais contatados e identificar se os mesmos têm disponibilidade de participar de processos 
        seletivos e se seus perfis estão adequados à oportunidade.
    </p>
    <p> 
        Também pode-se verificar a média salarial praticada no mercado de acordo com uma lista de 
        vagas, atribuições e experiências necessárias.
    </p>
    <?php echo Modules::run('clientes/servicoscases','ES'); ?>
</div>
<div class="clearfix"></div>