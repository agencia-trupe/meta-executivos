<div class="clearfix"></div>
	<div class="texto-right">
		<p>
			A Meta Executivos é a divisão do Grupo Meta RH especializada em prover 
			soluções de <em>Executive Search</em> e Desenvolvimento Organizacional, com a 
			missão primordial de identificar, atrair, selecionar e desenvolver 
			profissionais para cargos executivos em posições de liderança, seja 
			qual for a área de negócio de nossos clientes.
		</p>
		<p>
			Apoiamos as empresas, de forma rápida e assertiva, no diagnóstico, 
			análise e implantação das soluções mais adequadas.
		</p>
		<p>
			Contamos com uma equipe multidisciplinar com expertise em diversos 
			segmentos, formada por consultores bilíngues com larga experiência 
			em empresas nacionais e globais e ampla visão de negócios. 
			Profissionais aptos a identificar e avaliar competências técnicas 
			e comportamentais. Trabalhamos de forma integrada, com qualidade e 
			agilidade, oferecendo suporte decisório e acompanhamento em todo o processo.
		</p>


		<h2 class="section">Nossos Diferenciais</h2>

		<p>
			O constante contato com o mercado, com profissionais de ponta, mídias 
			sociais e demais recursos de informação e pesquisa, nos mantêm em 
			permanente atualização com as tendências, mudanças e especificidades dos diferentes segmentos. 
		</p>
		<p>
			Atuamos com especialistas em recrutamento com células dedicadas por setor 
			de atuação da empresa, garantindo que o consultor seja um especialista em 
			sua determinada divisão, oferecendo consultoria consistente, tanto para os 
			clientes quanto para seus candidatos.
		</p>
		<p>
			Buscamos atender cada cliente como único, tratando os projetos de forma 
			personalizada conforme suas necessidades. Acreditamos que empresas diferentes 
			precisam de soluções diferentes.
		</p>
		<p>
			Esta flexibilidade nos permite oferecer serviços exclusivos e sob medida 
			em um processo mais assertivo no qual buscamos a adequação ideal com a 
			confluência entre o momento de carreira do profissional e o momento da empresa. 
		</p>
	</div>
<div class="clearfix"></div>