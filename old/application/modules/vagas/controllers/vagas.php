<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Vagas 
* 
* @author Nilton de Freitas - Trupe 
* @link http://trupe.net
*
*/
Class Vagas extends MX_Controller
{
    public function index()
    {      
        $this->lista();
    }

    /**
     * Verifica se o tipo de vaga passado como parâmetro está no array
     * dos vagas e retorna a lista de vagas do tipo caso verdadeiro.
     * @return mixed Lista de vagas ativos
     */
    public function lista(){
        //Array com os tipos atuais de vagas

        //Verifica se o vaga passado como parâmetro está no array
            //Lógica da paginação de resultados
            $this->load->library(array('pagination', 'calendar'));
            $tipo = $this->uri->segment(2);
            $pagination_config = array(
                        'base_url'       => base_url() . 'para-profissionais/lista/',
                        'total_rows'     => $this->db->get('vagas')->num_rows(),
                        'per_page'       => 10,
                        'num_links'      => 5,
                        'next_link'      => 'próximo &raquo;',
                        'prev_link'      => '&laquo; anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE, 
                        'full_tag_open'  => '<div class="pagination center"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                );
            $this->pagination->initialize($pagination_config);
            //Obtendo resultados no banco
            $this->load->model('vagas/vaga');
            $data['result'] = $this->vaga->get_all($pagination_config['per_page'], $this->uri->segment(3));

            //Layout
            $data['tipo'] = $tipo;
            $data['header_titulo'] = 'Serviços para profissionais';
            $data['titulo'] = 'Meta Executivos &middot; Para Candidatos | ' . ucfirst($tipo);
            $data['pagina'] = 'vagas-lista';
            $data['header_image'] = '04.jpg';
            $data['conteudo_principal'] = "vagas/lista";
            $this->load->view('layout/template', $data);
    }

    function lista_home()
    {
        $this->load->model('vaga');
        $this->load->library('calendar');
        $data['result'] = $this->vaga->get_all_home();
        //Layout
        $this->load->view('vagas/lista_home', $data);
    }

    public function detalhe($origem, $vagaid)
    {
        if($origem == 'interna')
        {
            $this->load->model('vagas/vaga');
            $data['vaga'] = $this->vaga->get_vaga($vagaid);
            $this->load->view('vagas/modal', $data);
        }
        else
        {
            $data['vagaid'] = $this->uri->segment(4);
            //Verifica se o vaga passado como parâmetro está no array
                //Lógica da paginação de resultados
                $this->load->library(array('pagination', 'calendar'));
                $pagination_config = array(
                            'base_url'       => base_url() . 'vagas/',
                            'total_rows'     => $this->db->get('vagas')->num_rows(),
                            'per_page'       => 9,
                            'num_links'      => 5,
                            'next_link'      => 'próximo',
                            'prev_link'      => 'anterior',
                            'first_link'     => FALSE,
                            'last_link'      => FALSE, 
                            'full_tag_open'  => '<div class="pagination center"><ul>',
                            'full_tag_close' => '</ul></div>',
                            'cur_tag_open'   => '<li class="active"><a href="#">',
                            'cur_tag_close'  => '</a></li>',
                            'num_tag_open'   => '<li>',
                            'num_tag_close'  => '</li>',
                            'next_tag_open'   => '<li>',
                            'next_tag_close'  => '</li>',
                            'prev_tag_open'   => '<li>',
                            'prev_tag_close'  => '</li>',
                    );
                $this->pagination->initialize($pagination_config);
                //Obtendo resultados no banco
                $this->load->model('vagas/vaga');
                $data['result'] = $this->vaga->get_all($pagination_config['per_page'], $this->uri->segment(3));

                //Layout
                $data['titulo'] = 'Meta Executivos &middot; Para Candidatos';
                $data['pagina'] = 'vagas-lista';
                $data['header_image'] = '04.jpg';
                $data['header_titulo'] = 'Serviços para profissionais';
                $data['conteudo_principal'] = "vagas/lista";
                $this->load->view('layout/template', $data);
        }
    }
}