<div class="conteudo-vagas">
        		<div class="clearfix"></div>
<div class="texto-right">
    <h2 id="oportunidades">Oportunidades</h2>
    <div class="boxes">
		<div class="content-boxes">
			<ul>
            <?php foreach($result as $vaga): ?>
				<li>
					<div class="box-vaga" >
						<a data-toggle="modal" href="<?php echo base_url(); ?>vagas/detalhe/<?php echo 'interna/' . $vaga->id; ?>" data-target="#Modal_<?php echo $vaga->id; ?>" class="vaga-imagem left">
                            <?php if($vaga->vaga_imagem != NULL): ?>
                                <img 
                                    width="120" 
                                    height="34" 
                                    src="<?php echo base_url() 
                                    . 'assets/img/vagas/' 
                                    . $vaga->vaga_imagem; ?>" 
                                    alt="<?php echo $vaga->vaga_empresa; ?>">
                                    
                            <?php else:?>
                                <p><?php echo $vaga->vaga_empresa; ?></p>
                            <?php endif; ?>
                        </a>
                        <div class="vaga-descricao left">
                            
                            <a data-toggle="modal" href="<?php echo base_url(); ?>vagas/detalhe/<?php echo 'interna/' . $vaga->id; ?>" data-target="#Modal_<?php echo $vaga->id; ?>" class="vaga-empresa"><h1><?php echo 
                            $vaga->vaga_empresa; ?></h1></a>
						    
                            <div class="vaga-data-publicacao"><span><?php echo 
                            date('d', $vaga->vaga_data_cadastro) . ' ' 
                            . $this->calendar->get_month_name(date('m', $vaga->vaga_data_cadastro)) 
                            . ' ' . date('Y', $vaga->vaga_data_cadastro); ?></span></div>
                            
                            <div class="vaga-titulo"><span><?php echo 
                            $vaga->vaga_titulo; ?></span></div>
                            
                            <div class="vaga-quantidade-local"><span><?php echo 
                            ($vaga->vaga_quantidade == '1') ? '1 Posição' : $vaga->vaga_quantidade . ' posições'; ?><?php echo 
                            ($vaga->vaga_local != NULL) ? ' &middot; ' . $vaga->vaga_local : ''; ?></span></div>
                            
                            <a data-toggle="modal" href="<?php echo base_url(); ?>vagas/detalhe/<?php echo 'interna/' . $vaga->id; ?>" data-target="#Modal_<?php echo $vaga->id; ?>" class="vaga-mais right">saiba +</a>
                                                 
						</div>
                        <div class="clearfix"></div>                     
					</div>
				</li>
			<?php endforeach; ?>
		    </ul>
		</div>
		<div class="clearfix"></div>
		<?php echo $this->pagination->create_links(); ?>

	</div>
	<?php foreach($result as $vaga): ?>
      <div id="Modal_<?php echo $vaga->id; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        </div>
        <div class="modal-body">
          
        </div>
      </div>
    <?php endforeach; ?>
      <div id="homeModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        </div>
        <div class="modal-body">
          
        </div>
      </div>
	<div class="clearfix"></div>
    
</div>

<div class="clearfix"></div>
<div class="newsletter">
    <div class="interna">
        <div class="clearfix"></div>
        <div id="newsletter-label">
            <h1>NewsLetter</h1>
            <p>cadastre-se para receber<br> nossos newsletters</p>
        </div>
        <?php echo form_open('newsletter', 'id="newsletter"'); ?>
        <ul>
            <li>
                <?php echo form_input(array('name'=>'nome','value'=>'','class'=>'nome textbox', 'placeholder'=>'nome')); ?>
                <br>
                <?php echo form_input(array('name'=>'email','value'=>'','class'=>'email textbox', 'placeholder'=>'email')); ?>
            </li>
            <li>
                <label for="selecao">
                    <p>
                        Selecione o conteúdo <br>
                        ao lado que deseja <br>
                        receber no seu email
                    </p>
                </label>
          </li>
            <li>
                <div>
                    <label for="vagas"><?php echo form_checkbox(array('name'=>'vagas','id'=>'vagas','value'=>'1','checked'=>TRUE,)); ?>vagas</label>
                </div>
                <div>
                    <label for="novidades"><?php echo form_checkbox(array('name'=>'novidades','id'=>'novidades-news','value'=>'1','checked'=>TRUE,)); ?>
novidades</label>
                </div>
                <div>
                    <label for="eventos"><?php echo form_checkbox(array('name'=>'eventos','id'=>'eventos','value'=>'1','checked'=>TRUE)); ?>
eventos</label>
                </div>
            </li>
            <li>
                <div>
                    <label for="resultados"><?php echo form_checkbox(array('name'=>'resultados','id'=>'resultados','value'=>'1','checked'=>TRUE)); ?>resultados de pesquisa</label>
                </div>
                <div>    
                    <label for="eventos"><?php echo form_checkbox(array('name'=>'noticias','id'=>'noticias','value'=>'1','checked'=>TRUE)); ?>noticias</label>
                </div>         
            </li>
            <li id="ultimo"><input type="submit" value="enviar" id="newslettersalva" /></li>
        </ul>
        <?php echo form_close(); ?>
    </div>
</div>
<div id="social-vagas">
        <a href="http://www.facebook.com/metaexecutivos" id="facebook"></a>
        <a href="http://www.linkedin.com/company/meta-executivos" id="linkedin"></a>
    </div>
    <div class="clearfix"></div>
</div>