<div id="oportunidades-home">
    <h1>Oportunidades</h1>
	    <?php foreach($result as $vaga): ?>
	    <a class="vagas-home-link" href="<?php echo base_url(); ?>vagas/detalhe/home/<?php echo $vaga->id; ?>"  >
	    <div class="oportunidades-home-empresa"><?php echo $vaga->vaga_empresa; ?></div>
	   	</a>
	    <div class="oportunidades-home-data">
	    	<?php echo 
	           date('d', $vaga->vaga_data_cadastro) . ' ' 
	           . $this->calendar->get_month_name(date('m', $vaga->vaga_data_cadastro)) 
	           . ' ' . date('Y', $vaga->vaga_data_cadastro); ?>	
	    </div>
	    <div class="oportunidades-home-descricao">
	    	<?php echo $vaga->vaga_titulo; ?>
	    </div>
	    <div class="oportunidades-home-vagas-local">
	    		<?php echo 
	                        ($vaga->vaga_quantidade == '1') ? '1 Posição' : $vaga->vaga_quantidade . ' posições'; ?> &middot; <?php echo 
	                        $vaga->vaga_local; ?>
	    </div>
    <div class="clearfix"></div>
	<?php endforeach; ?>
</div>