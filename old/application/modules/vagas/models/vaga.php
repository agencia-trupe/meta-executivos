<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* vaga
*
* Model responsável pelas regras de negócio dos vagas de estágio, trainee e aprendiz
*
* @author Nilton de Freitas - Trupe
* @link http://trupe.net
*
*/
Class Vaga extends Datamapper{
    var $table = 'vagas';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }
    /**
     * Retorna os vagas ativos em ordem crescente de antiguidade
     * @param    int    $limit   quantidade limite por retorno
     * @param    int    $offset  offset para a paginação
     * @return   array           array com a lista de vagas ativos
     */
    function get_all($limit, $offset)
    {
        $vaga = new Vaga();
        $vaga->order_by('vaga_data_cadastro', 'desc');
        $vaga->order_by('vaga_data_expiracao', 'asc');
        $vaga->get($limit, $offset);

        $arr = array();
        foreach($vaga->all as $vagas)
        {
            $arr[] = $vagas;
        }

        return $arr;

    }

    function get_all_all($limit, $offset)
    {
        $vaga = new vaga();
        $vaga->order_by('vaga_data_expiracao', 'asc');
        $vaga->order_by('vaga_data_cadastro', 'desc');
        $vaga->get($limit, $offset);

        $arr = array();
        foreach($vaga->all as $vagas)
        {
            $arr[] = $vagas;
        }

        return $arr;

    }

    function get_all_home()
    {
        $vaga = new Vaga();
        $vaga->where('vaga_destaque', '1');
        $vaga->order_by('vaga_data_cadastro', 'desc');
        $vaga->order_by('vaga_data_expiracao', 'asc');
        $vaga->get(4);

        $arr = array();
        foreach($vaga->all as $vagas)
        {
            $arr[] = $vagas;
        }

        return $arr;

    }

    function get_vaga($id)
    {
        $vaga = new Vaga();
        $vaga->where('id', $id);
        $vaga->limit(1);
        $vaga->get();

        return $vaga;

    }


    function cadastra($dados){

        $vaga = new Vaga();

        $vaga->vaga_empresa = $dados['empresa'];
        $vaga->vaga_titulo = $dados['titulo'];
        $vaga->vaga_descritivo = $dados['descritivo'];
        $vaga->vaga_quantidade = $dados['quantidade'];
        $vaga->vaga_link = $dados['link'];
        $vaga->vaga_local = $dados['local'];
        $vaga->vaga_data_expiracao = strtotime(str_replace('/', '-', $dados['data_expiracao']));;
        $vaga->vaga_destaque = $dados['destaque'];
        $vaga->vaga_data_cadastro = time();

        if($vaga->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }

    }

    function cadastra_imagem($dados){

        $vaga = new Vaga();

        $vaga->vaga_empresa = $dados['empresa'];
        $vaga->vaga_titulo = $dados['titulo'];
        $vaga->vaga_descritivo = $dados['descritivo'];
        $vaga->vaga_quantidade = $dados['quantidade'];
        $vaga->vaga_imagem = $dados['imagem'];
        $vaga->vaga_link = $dados['link'];
        $vaga->vaga_local = $dados['local'];
        $vaga->vaga_data_expiracao = strtotime(str_replace('/', '-', $dados['data_expiracao']));;
        $vaga->vaga_destaque = $dados['destaque'];
        $vaga->vaga_data_cadastro = time();

        if($vaga->save()){
            return TRUE;
        } else
        {
            return FALSE;
        }

    }

    function atualiza($dados){

        $vaga = new vaga();
        $vaga->where('id', $dados['id']);
        $update = $vaga->update(array(
            'vaga_empresa' => $dados['empresa'],
            'vaga_titulo' => $dados['titulo'],
            'vaga_descritivo' => $dados['descritivo'],
            'vaga_quantidade' => $dados['quantidade'],
            'vaga_link' => $dados['link'],
            'vaga_local' => $dados['local'],
            'vaga_data_expiracao' => strtotime(str_replace('/', '-',
                $dados['data_expiracao'])),
            'vaga_destaque' => $dados['destaque'],
        ));
        return $update;
    }

    function atualiza_imagem($dados){

        $vaga = new vaga();
        $vaga->where('id', $dados['id']);
        $update = $vaga->update(array(
            'vaga_empresa' => $dados['empresa'],
            'vaga_titulo' => $dados['titulo'],
            'vaga_descritivo' => $dados['descritivo'],
            'vaga_imagem' => $dados['imagem'],
            'vaga_quantidade' => $dados['quantidade'],
            'vaga_link' => $dados['link'],
            'vaga_local' => $dados['local'],
            'vaga_data_expiracao' => strtotime(str_replace('/', '-',
                $dados['data_expiracao'])),
            'vaga_destaque' => $dados['destaque'],
        ));
        return $update;
    }

    function delete_vaga($id){
        $vaga = new Vaga();
        $vaga->where('id', $id)->get();


        if($vaga->delete()){
            return TRUE;
        } else
        {
            return FALSE;
        }
    }
}