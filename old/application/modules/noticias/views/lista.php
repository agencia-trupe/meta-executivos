<div class="conteudo-programas">
	<div class="interna">
		<header>
			<h1>Para candidatos</h1>
			<ul>
				<li><a <?php echo ($tipo == 'trainees') ? 'class="active"' : ''; ?> id="trainees" href="<?php echo base_url(); ?>programas/trainees">Trainees</a></li>
				<li><a <?php echo ($tipo == 'estagios') ? 'class="active"' : ''; ?> id="estagios" href="<?php echo base_url(); ?>programas/estagios">Estágios</a></li>
				<li><a <?php echo ($tipo == 'aprendizes') ? 'class="active"' : ''; ?> id="aprendizes" href="<?php echo base_url(); ?>programas/aprendizes">Aprendizes</a></li>
			</ul>
			<div class="clearfix"></div>
		</header>
		<div class="clearfix"></div>
		<div class="banners">
			<?php foreach($result as $programa): ?>
			<a href="<?php echo $programa->programa_link; ?>" title="<?php echo $programa->programa_empresa; ?> - <?php echo $programa->programa_id; ?>">
				<img src="<?php echo base_url() . 'assets/img/programas/banners/' . $programa->programa_imagem; ?>" alt="<?php echo $programa->programa_empresa; ?>">
			</a>
			<?php endforeach; ?>
			<?php echo $this->pagination->create_links(); ?>
		</div>			
		<div class="info">
			<p><b>Nossos Programas de Estágio em andamento</b></p>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
				sed do eiusmod tempor incididunt ut labore et dolore magna
			</p>
		</div>
		<div class="clearfix"></div>
	</div>
</div>