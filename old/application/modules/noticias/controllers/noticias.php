<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Programas 
* 
* @author Nilton de Freitas - Trupe 
* @link http://trupe.net
*
*/
Class Programas extends MX_Controller
{
    public function index()
    {      
        $data['titulo'] = 'Meta Executivos &middot; Para Candidatos';
        $data['pagina'] = 'candidatos';
        $data['conteudo_principal'] = "static_pages/candidatos";
        $this->load->view('layout/main', $data);
    }

    /**
     * Verifica se o tipo de programa passado como parâmetro está no array
     * dos programas e retorna a lista de programas do tipo caso verdadeiro.
     * @return mixed Lista de programas ativos
     */
    public function lista($tipo = NULL){
        //Array com os tipos atuais de programas
        $programas = array('trainees', 'estagios', 'aprendizes');

        //Verifica se o programa passado como parâmetro está no array
        if(in_array($tipo, $programas)){
            //Lógica da paginação de resultados
            $this->load->library('pagination');
            $tipo = $this->uri->segment(2);
            $pagination_config = array(
                        'base_url'       => base_url() . 'programas/' . $tipo . '/',
                        'total_rows'     => $this->db->where('programa_categoria', $tipo)->get('programas')->num_rows(),
                        'per_page'       => 3,
                        'num_links'      => 5,
                        'next_link'      => 'próximo',
                        'prev_link'      => 'anterior',
                        'first_link'     => FALSE,
                        'last_link'      => FALSE, 
                        'full_tag_open'  => '<div class="pagination center"><ul>',
                        'full_tag_close' => '</ul></div>',
                        'cur_tag_open'   => '<li class="active"><a href="#">',
                        'cur_tag_close'  => '</a></li>',
                        'num_tag_open'   => '<li>',
                        'num_tag_close'  => '</li>',
                        'next_tag_open'   => '<li>',
                        'next_tag_close'  => '</li>',
                        'prev_tag_open'   => '<li>',
                        'prev_tag_close'  => '</li>',
                );
            $this->pagination->initialize($pagination_config);
            //Obtendo resultados no banco
            $this->load->model('programas/programa');
            $data['result'] = $this->programa->get_all($tipo, $pagination_config['per_page'], $this->uri->segment(3));

            //Layout
            $data['tipo'] = $tipo;
            $data['titulo'] = 'Meta Executivos &middot; Para Candidatos | ' . ucfirst($tipo);
            $data['pagina'] = 'candidatos';
            $data['conteudo_principal'] = "programas/lista";
            $this->load->view('layout/main', $data);
        }
        else
        {
            //Caso o tipo de programa passado como parâmetro não seja válido,
            //retorna para a página principal dos programas
            $this->index();
        }
    }
}