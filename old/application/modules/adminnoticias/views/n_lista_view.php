<div class="span9">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span6">
            <legend>Listar Noticias</legend>
        </div>
        <?php 
            if (isset($update_success))
            {
                echo '<div class="span8"><div class="alert alert-success">'.$update_success.$today.'às '.$time.'</div></div>';
            }
        ?>
    </div>
  <?php if(isset($result)): ?>
     <?php
        
            $tmpl = array (
                                'table_open'          => '<table class="table table-striped table-bordered table-condensed">',

                                'heading_row_start'   => '<tr>',
                                'heading_row_end'     => '</tr>',
                                'heading_cell_start'  => '<th>',
                                'heading_cell_end'    => '</th>',

                                'row_start'           => '<tr>',
                                'row_end'             => '</tr>',
                                'cell_start'          => '<td>',
                                'cell_end'            => '</td>',


                                'table_close'         => '</table>'
                        );

            $this->table->set_template($tmpl); 
            $this->table->set_heading(array('Data de Publicação', 'Título', 'Detalhes'));
            foreach ($result as $item)
            {
            $data_publicacao = date('d/m/Y', $item->noticia_data_publicacao);
            $titulo = $item->noticia_titulo;
            $detalhes = anchor('painel/noticias/editar/' . $item->id, 'Editar') . ' | ' . anchor('painel/noticias/apaga/' . $item->id, 'Remover', 'id="removelink"') ;            $quantidade = $item->noticia_quantidade;
            $this->table->add_row(array($data_publicacao, $titulo, $detalhes));
            }
            echo $this->table->generate();

            ?> 
  <?php endif; ?>
        

</div><!--/span-->