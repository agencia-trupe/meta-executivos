<div class="row-fluid">
<div class="span12">
    <div class="alert alert-info">
        Inscrição self<br>
        Esta é a página de inscrição para o treinamento <b>Nome do Treinamento</b><br/>
        Você pode efetuar até 5 inscrições diferentes incluindo a sua.<br/>
        As inscrições serão efetivadas após a confirmação do pagamento.
    </div>
    <h3>Participante 1</h3>
    <?php echo form_open('inscricao/self'); ?>
    <?php 
        $nome = array(
            'name' => 'nome',
            'value' => $self->nome,
        );
        $email = array(
            'name' => 'email',
            'value' => $self->email,
        );
        echo form_label('Nome');
        echo form_input($nome);
        echo form_label('Email');
        echo form_input($email);
        echo form_hidden('turma_id', $id);
        echo form_submit('', 'Finalizar','class="btn btn-success"');
        
        echo form_close();
        ?>
          
</div>
</div>