<div id="direita">
    <div id="curso">
        <img src="<?php echo base_url(); ?>assets/img/featured/unique5.png"/>
        <header>
            <h3>Curso</h3>
            <h1>Etiqueta Profissional Lorem Ipsum Dolor Sic Amec Lorem Ipsum</h1>
        </header>
        <div class="clearfix"></div>
        <div class="curso-actions">
            <div class="btn-group">
                <button class="btn btn-small btn-primary dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-pencil"></i> Inscreva-se <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">Turma de 20/06/2012</a></li>
                    <li><a href="#">Turma de 07/07/2012</a></li>
                </ul>
            </div><!-- /btn-group -->
            <div class="btn-group">
                <button class="btn btn-small btn-info dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-share"></i> Compartilhe <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Linkedin</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Envie por email</a></li>
                </ul>
            </div><!-- /btn-group -->
            <div class="clearfix"></div>
        </div>
        <div id="curso-content">
            <dl>
                <dt><i class="icon-calendar"></i> <span>Data</span></dt>
                <dd>20/06/2012</dd>
                
                <dt><i class="icon-time"></i> Carga Horária</dt>
                <dd>8 horas</dd>
                
                <dt><i class="icon-screenshot"></i> Objetivo</dt>
                <dd> 
                    <p>
                        Estimular o participante a perceber o aspecto estratégico 
                        da etiqueta no ambiente profissional, permitindo que passe 
                        a atentar para a postura, o vestuário, o vocabulário e, 
                        principalmente, o comportamento como referências positivas 
                        da sua marca no negócio e no mercado.
                    </p>
                </dd>
                
                <dt><i class="icon-user"></i> Público Alvo</dt>
                <dd><span> - </span></dd>
                <dt><i class="icon-ok"></i> Pré-requisitos</dt>
                <dd>Nenhum</dd>
                <dt><i class="icon-list-alt"></i> Programa</dt>
                <dd>
                    <ul>
                        <li>Etiqueta profissional: quem precisa dela?</li>
                        <li>O que é ... cortesia?</li>
                        <li>Você é comprado conforme se apresenta</li>
                        <li>Em boca fechada não entra mosquito...</li>
                        <li>Etiqueta para Celulares</li>
                        <li>Comportamento Social e Etiqueta Empresarial</li> 
                        <li>Cortesia no dia-a-dia</li>
                        <li>Network</li>
                        <li>Como lidar com os boatos de escritório</li>
                        <li>A postura chega antes de você</li> 
                        <li>O que vestir?</li>
                        <li>Palavras Mágicas</li>
                        <li>Ao Telefone</li>
                        <li>Etiqueta Global e Gentilezas Virtuais</li>
                        <li>Atenção, retardatários!</li>
                    </ul>
                </dd>
                <dt><i class="icon-book"></i> Metodologia</dt>
                <dd>
                    <p>
                        O programa é totalmente desenvolvido visando discutir cada 
                        um dos temas através de dinâmicas de grupo, jogos e 
                        simulações, proporcionando ao treinando uma oportunidade 
                        única de rever atitudes e comportamentos que permitam 
                        adequar-se aos conceitos e práticas para uma postura 
                        profissional eficiente.
                    </p>
                </dd>
            </dl>
        </div>
        <div class="curso-actions">
            <a href="#" class="btn btn-small btn-primary">Inscreva-se</a>
            <div class="btn-group">
                <button class="btn btn-small btn-info dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-share"></i> Compartilhe <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Linkedin</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Envie por email</a></li>
                </ul>
            </div><!-- /btn-group -->
            <div class="clearfix"></div>
        </div>
    </div>
    
    
</div>						
                <div class="clearfix"></div>

		</div>
                <div id="container-base"></div>
		
