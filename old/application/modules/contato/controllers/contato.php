<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Contato
* 
* @author Nilton de Freitas - Trupe 
* @link http://trupe.net
*
*/
Class Contato extends MX_Controller
{
        /**
         * Exibe o formulário de contato
         */
        public function index()
        {
                $data['titulo'] = 'Meta Executivos &middot; Contato';
                $data['pagina'] = 'contato';
                $data['conteudo_principal'] = "contato/index";
                $data['header_titulo'] = 'Contato';
                $data['header_image'] = '08.jpg';
                $this->load->view('layout/template', $data);
        }

        function ajax_check() 
        {
                $this->load->library('form_validation');
                $this->load->library('typography');

                if($this->input->post('ajax') == '1') 
                {
                        $this->form_validation->set_rules('nome', 'nome', 'required|xss_clean|max_length[255]');
                        $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required|xss_clean');
                        $this->form_validation->set_rules('telefone', 'telefone', 'required|xss_clean|max_length[18]');
                        $this->form_validation->set_rules('mensagem', 'mensagem', 'required|xss_clean|min_length[10]');
                        if($this->form_validation->run() == FALSE) 
                        {
                                echo validation_errors();
                        } 
                        else 
                        {

                                // send an email  
                                $this->load->library('email');  
                                $this->email->from($this->input->post('email'),$this->input->post('nome'));  
                                $this->email->to("metataexecutivos@metaexecutivos.com.br");
                                $this->email->subject('Contato do site Meta Executivos - ' . $this->input->post('nome'));
                                $data['dados'] = array(
                                                'nome' => $this->input->post('nome'),
                                                'email' => $this->input->post('email'),
                                                'telefone' => $this->input->post('telefone'),
                                                'mensagem' => $this->typography->auto_typography($this->input->post('mensagem')),
                                        );
                                $email_view = $this->load->view('contato/email', $data, TRUE);

                                $this->email->message($email_view);

                                if($this->email->send())
                                {
                                        echo 'Mensagem enviada com sucesso!';
                                } 
                                else
                                {
                                        echo 'Erro ao enviar mensagem, tente novamente.';
                                }
                        }
                }
        }
}

