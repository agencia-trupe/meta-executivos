<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Detalhes SEO
|
| Configurações básicas de SEO.
|--------------------------------------------------------------------------
*/
$config['site_name'] = 'Meta Executivos';

$config['site_decription'] = 'A Meta Executivos é uma Unidade de 
        Negócios do Grupo Meta RH. Especializada em atrair, reter e desenvolver 
        jovens talentos. A força jovem que a sua empresa precisa.';

$config['site_keywords'] = 'Meta Executivos, Grupo Meta RH, Talentos,
        Estágio, Aprendiz, Trainee';

$config['site_author'] = 'Trupe Agência Criativa - http://trupe.net';
/* End of file seo.php */
/* Location: ./application/config/seo.php */