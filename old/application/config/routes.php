<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "static_pages";

$route['login'] = 'auth/login';

$route['perfil'] = 'static_pages/perfil';
$route['empresas'] = 'static_pages/empresas';
$route['candidatos'] = 'programas';
$route['programas/trainees'] = 'programas/lista/trainees';
$route['programas/estagios'] = 'programas/lista/estagios';
$route['programas/aprendizes'] = 'programas/lista/aprendizes';
$route['programas/trainees/(:num)'] = 'programas/lista/trainees/$1';
$route['programas/estagios/(:num)'] = 'programas/lista/estagios/$1';
$route['programas/aprendizes/(:num)'] = 'programas/lista/aprendizes/$1';

$route['para-profissionais'] = 'vagas';
$route['para-profissionais/(:any)'] = 'vagas/$1';

$route['vagas/trainees'] = 'vagas/lista/trainees';
$route['vagas/estagios'] = 'vagas/lista/estagios';
$route['vagas/aprendizes'] = 'vagass/lista/aprendizes';
$route['vagas/trainees/(:num)'] = 'vagas/lista/trainees/$1';
$route['vagas/estagios/(:num)'] = 'vagas/lista/estagios/$1';
$route['vagas/aprendizes/(:num)'] = 'vagas/lista/aprendizes/$1';

$route['painel/programas'] = 'adminprogramas';
$route['painel/programas/(:any)'] = 'adminprogramas/$1';

$route['painel/vagas'] = 'adminvagas';
$route['painel/vagas/(:any)'] = 'adminvagas/$1';

$route['painel/newsletter'] = 'adminnewsletters';
$route['painel/newsletter/(:any)'] = 'adminnewsletters/$1';

$route['painel/noticias'] = 'adminnoticias';
$route['painel/noticias/(:any)'] = 'adminnoticias/$1';

$route['painel'] = 'adminnoticias';
$route['novidades'] = 'novidades/lista';

$route['logout'] = 'auth/logout';

$route['404_override'] = '';

$route['depoimentos'] = 'static_pages/depoimentos';
$route['depoimentos/(:any)'] = 'static_pages/depoimentos/$1';

$route['para-empresas'] = 'static_pages/empresas';
$route['para-empresas/hunting'] = 'static_pages/empresas/executive-search';
$route['para-empresas/assessment'] = 'static_pages/empresas/desenvolvimento-organizacional';
$route['para-empresas/coaching'] = 'static_pages/empresas/desenvolvimento-organizacional';
$route['para-empresas/(:any)'] = 'static_pages/empresas/$1';

$route['clientes-cases'] = 'clientes';
$route['clientes-cases/(:any)'] = 'clientes/$1';

$route['noticias'] = 'novidades/lista';
$route['noticias/(:any)'] = 'novidades/$1';



/* End of file routes.php */
/* Location: ./application/config/routes.php */