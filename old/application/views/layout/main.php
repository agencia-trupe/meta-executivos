<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <title><?php echo $titulo; ?></title>
        <meta name="description" content="A Meta Executivos é uma Unidade de 
        Negócios do Grupo Meta RH. Especializada em atrair, reter e desenvolver 
        jovens talentos. A força jovem que a sua empresa precisa.">
        <meta name="keywords" content="Meta Executivos, Grupo Meta RH, Talentos,
        Estágio, Aprendiz, Trainee">
        <meta name="author" content="Trupe Agência Criativa">
        <meta name="viewport" content="width=device-width">

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
        <?php if($pagina == 'vagas-lista') : ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" >
        <?php endif; ?>

    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <header>
            <div class="interna <?php echo $pagina; ?>">
               <a href="<?php echo base_url(); ?>" id="logo"></a>
               <div class="clearfix"></div>
               <nav>
                    <ul>
                       <li><a <?php echo ($pagina == 'perfil') ? 'class="active"' : ''; ?> id="perfil" href="<?php echo base_url(); ?>perfil">perfil Meta Executivos</a></li>
                       <li><a <?php echo ($pagina == 'empresas') ? 'class="active"' : ''; ?> id="empresas" href="<?php echo base_url(); ?>empresas">para empresas</a></li>
                       <li><a <?php echo ($pagina == 'candidatos' || $pagina == 'vagas-lista') ? 'class="active"' : ''; ?> id="candidatos" href="<?php echo base_url(); ?>candidatos">para candidatos</a></li>
                       <li><a <?php echo ($pagina == 'clientes') ? 'class="active"' : ''; ?> id="clientes" href="<?php echo base_url(); ?>clientes">cases | clientes</a></li>
                       <li><a <?php echo ($pagina == 'novidades') ? 'class="active"' : ''; ?> id="novidades" href="<?php echo base_url(); ?>novidades">novidades | artigos</a></li>
                       <li><a <?php echo ($pagina == 'contato') ? 'class="active"' : ''; ?>id="contato" href="<?php echo base_url(); ?>contato">contato</a></li> 
                    </ul>
               </nav>
               <div class="clearfix"></div> 
               <aside class="sidebar">
                   <div class="box" id="trainees">
                       <h1>Trainees</h1>
                       <p>
                           Acompanhe aqui os Programas para Trainees em andamento.
                       </p>
                       <div class="clearfix"></div>
                       <a href="<?php echo base_url(); ?>programas/trainees" class="right mais">Ver mais</a>
                   </div>
                   <div class="box" id="estagios">
                       <h1>Estágios</h1>
                       <p>
                           Diversas oportunidades para quem <br> está iniciando a carreira profissional.
                       </p>
                       <div class="clearfix"></div>
                       <a href="<?php echo base_url(); ?>programas/estagios" class="right mais">Ver mais</a>
                   </div>
                   <div class="box" id="aprendizes">
                       <h1>Aprendizes</h1>
                       <p>
                           Menores aprendizes podem conseguir seu primeiro emprego aqui.
                       </p>
                       <div class="clearfix"></div>
                       <a href="<?php echo base_url(); ?>programas/aprendizes" class="right mais">Ver mais</a>
                   </div>

                   <?php if( $pagina != 'home' ){
                    echo Modules::run('twitter');
                    };
                    ?>
               </aside>
               <?php if($pagina == 'home'): ?>
                   <img style="float:right; margin-top: 16px; "src="<?php echo base_url(); ?>assets/img/banner-principal.png" alt="Banner Meta Executivos">
                    <?php endif; ?>
            </div>
        </header>
        <?php if(isset($conteudo_principal)){ $this->load->view($conteudo_principal); } ?>
            <div class="grupo">
                <div class="interna">

                    <div class="descricao-grupo">
                        <p>
                            A Meta Executivos é uma empresa do GRUPO META RH. 
                            Conheça as empresas do Grupo:
                        </p>
                        <a href="<?php echo base_url(); ?>" id="logo-menor"></a>
                        <ul>
                            <li><a href="http://grupometarh.com.br/premios">prêmios &raquo;</a></li>
                            <li><a href="http://grupometarh.com.br/responsabilidade-social">responsabilidade social &raquo;</a></li>
                            <li><a href="http://grupometarh.com.br/visao-missao-valores">visão, missão, valores &raquo;</a></li>
                        </ul>
                    </div><!--Fim descrição grupo-->

                    <div id="unidades">
                        <div class="unidade">
                            <ul>
                                <li> <a target="_blank" href="http://metarh.com.br" id="meta-rh"></a> </li>
                                <li>» Recrutamento e seleção «</li>
                                <li>» Temporários «</li>
                                <li>» Terceiros | Outsourcing | BPO – Business Process Outsourcing «</li>
                            </ul>
                        </div>
                        <div class="unidade">
                            <ul>
                                <li> <a target="_blank" href="http://metaexecutivos.com.br" id="meta-executivos"></a> </li>
                                <li>» Search & Hunting «</li>
                                <li>» Assessment «</li>
                                <li>» Coaching «</li>
                                <li>» Counseling «</li>
                                <li>» Desenvolvimento Organizacional (D.O.) «</li>
                            </ul>
                        </div>
                        <div class="unidade">
                            <ul>
                                <li> <a target="_blank" href="http://metatalentos.com.br" id="meta-talentos"></a> </li>
                                <li>» Menores aprendizes «</li>
                                <li>» Estagiários «</li>
                                <li>» Trainees «</li>
                                <li>» Mentoring «</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        <div class="footer">
            <div class="interna">
                <div class="info">
                    <ul>
                        <li>
                            <p>Tel 11 <b>5525-2711</b></p>
                        </li>
                        <li class="endereco">
                            <p>
                                Meta Executivos<br>
                                Av. Adolfo Pinheiro, 1001<br>
                                Alto da Boa Vista <br>
                                04733-100 &middot; São Paulo - SP
                            </p>
                        </li><!--Fim do endereço-->
                        <li class="sitemap">
                           <ul>
                                <li><a href="<?php echo base_url(); ?>perfil">&raquo; perfil Meta Executivos</a></li>
                                <li><a href="<?php echo base_url(); ?>empresas">&raquo; para empresas</a></li>
                                <li><a href="<?php echo base_url(); ?>candidatos">&raquo; para candidatos</a></li>
                                <li><a href="<?php echo base_url(); ?>clientes">&raquo; cases | clientes</a></li>
                                <li><a href="<?php echo base_url(); ?>novidades">&raquo; novidades | artigos</a></li>
                                <li><a href="<?php echo base_url(); ?>contato">&raquo; contato</a></li>
                            </ul>
                        </li><!--Fim do sitemap-->
                    </ul>
                </div><!--Fim da div info-->
                <div class="copyright">
                    <p id="copy-meta">
                        &copy; 2012 Grupo Meta RH<br>
                        Todos os direitos reservados
                    </p>
                    <p id="trupe">
                        <a target="_blank" href="http://trupe.net">Criação de sites: Trupe Agência Criativa</a>
                    </p>
                    <a href="http://trupe.net" target="_blank" id="logo-trupe"></a>
                </div>
            </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/libs/bootstrap.min.js"></script>
        </body>
        <?php if(isset($vagaid)): ?>
            <script type="text/javascript">
                $(window).load(function(){
                    $('#homeModal').modal({
                        show: true,
                        remote: "<?php echo base_url(); ?>vagas/detalhe/<?php echo $tipo . '/interna/' . $vagaid; ?>"
                    });
                });
            </script>
        <?php endif; ?>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-35751088-1'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>
