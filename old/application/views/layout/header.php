<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <title><?php echo $titulo; ?></title>
        <meta name="description" content="A Meta Executivos atua no segmento de 
        hunting e desenvolvimento de executivos e profissionais especializados">
        <meta name="keywords" content="meta executivos, executivos, 
        recrutamento, hunting, assessment, coaching, counseling, 
        desenvolvimento organizacional,">
        <meta name="author" content="Trupe Agência Criativa">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
        <link rel="stylesheet/less" href="<?php echo base_url(); ?>assets/css/main.less">
        <script src="<?php echo base_url(); ?>assets/js/less-1.3.0.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
        <?php if($pagina == 'vagas-lista') : ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" >
        <?php endif; ?>

    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div id="main">
            <header id="principal">
                <div class="clearfix"></div>
                <?php if($pagina == 'home') : ?> 
                <div id="header-image">
                    <img class="header-image" src="<?php echo base_url(); ?>assets/img/header/home-01.jpg" alt="header image">
                    <img class="header-image" src="<?php echo base_url(); ?>assets/img/header/home-02.jpg" alt="header image">
                    <img class="header-image" src="<?php echo base_url(); ?>assets/img/header/home-03.jpg" alt="header image">
                    <img class="header-image" src="<?php echo base_url(); ?>assets/img/header/home-04.jpg" alt="header image">
                </div>
                <?php else : ?>
                <img class="header-image" src="<?php echo base_url(); ?>assets/img/header/<?php echo (isset($header_image)) ? $header_image : '01.jpg'; ?>" alt="header image">
                <?php endif; ?>

                <div id="logo">
                    <a href="<?php echo base_url(); ?>">
                        <img src="<?php echo base_url(); ?>assets/img/marca-meta-executivos.jpg" alt="Meta Executivos">
                    </a>
                </div>
                <nav>
                    <ul>
                        <li><a <?php echo ($pagina == 'perfil') ? 'class="active"' : ''; ?> href="<?php echo site_url('perfil'); ?>">A empresa | perfil <span>&raquo;</span></a></li>
                        <li><a <?php echo ($pagina == 'empresas') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-empresas'); ?>">Serviços para empresas <span>&raquo;</span></a></li>
                        <li><a <?php echo ($pagina == 'vagas-lista') ? 'class="active"' : ''; ?> href="<?php echo site_url('para-profissionais'); ?>">Serviços para profissionais <span>&raquo;</span></a></li>
                        <li><a <?php echo ($pagina == 'clientes') ? 'class="active"' : ''; ?> href="<?php echo site_url('clientes-cases'); ?>">Clientes | cases <span>&raquo;</span></a></li>
                        <li><a <?php echo ($pagina == 'depoimentos') ? 'class="active"' : ''; ?> href="<?php echo site_url('depoimentos'); ?>">Depoimentos <span>&raquo;</span></a></li>
                        <li><a <?php echo ($pagina == 'novidades') ? 'class="active"' : ''; ?> href="<?php echo site_url('noticias'); ?>">Notícias <span>&raquo;</span></a></li>
                        <li><a <?php echo ($pagina == 'contato') ? 'class="active"' : ''; ?>pagina href="<?php echo site_url('contato'); ?>">Contato | Localização <span>&raquo;</span></a></li>
                    </ul>
                </nav>
                <?php if($pagina == 'home'): ?>
                <div id="frase">
                    <h1 id="frase-titulo">Ajudamos Empresas a desenvolver líderes e a potencializar indivíduos e equipes.</h1>
                </div>
                <?php endif; ?>
                <div id="content-header">
                    <?php if($pagina == 'home'): ?>
                    <div id="social-header">
                        <a href="http://www.facebook.com/metaexecutivos" id="facebook"></a>
                        <a href="http://www.linkedin.com/company/meta-executivos" id="linkedin"></a>
                    </div>
                    <?php else: ?>
                        <h1><?php echo $header_titulo; ?></h1>
                    <?php endif; ?>
                    
                </div>
            </header>

            <div id="content" class="<?php echo $pagina; ?>">
                <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 