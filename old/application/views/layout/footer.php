            </div>
        </div>
        <div class="grupo">
                <div class="interna">

                    <div class="descricao-grupo">
                        <p>
                            A Meta Executivos é uma divisão do GRUPO META RH.
                            Conheça as empresas do Grupo:
                        </p>
                        <a href="http://grupometarh.com.br" id="logo-grupo-meta"></a>
                        <ul>
                            <li><a href="http://grupometarh.com.br/premios">prêmios &raquo;</a></li>
                            <li><a href="http://grupometarh.com.br/responsabilidade-social">responsabilidade social &raquo;</a></li>
                            <li><a href="http://grupometarh.com.br/visao-missao-valores">visão, missão, valores &raquo;</a></li>
                        </ul>
                    </div><!--Fim descrição grupo-->

                    <div id="unidades">
                        <div class="unidade">
                            <ul>
                                <li> <a class="servicos-rodape" target="_blank" href="http://metabpo.com.br" id="meta-rh"></a> </li>
                                <li><a class="servicos-rodape" href="http://metabpo.com.br/para-empresas/recrutamento" target="_blank">» Recrutamento e seleção «</a></li>
                                <li><a class="servicos-rodape" href="http://metabpo.com.br/para-empresas/temporarios" target="_blank">» Temporários «</a></li>
                                <li><a class="servicos-rodape" href="http://metabpo.com.br/para-empresas/outsourcing" target="_blank">» Outsourcing - BPO «</a></li>
                            </ul>
                        </div>
                        <div class="unidade">
                            <ul>
                                <li> <a target="_blank" href="http://metaexecutivos.com.br" id="meta-executivos"></a> </li>
                                <li><a class="servicos-rodape" href="http://metaexecutivos.com.br/para-empresas/hunting" target="_blank">» Search e Hunting «</a></li>
                                <li><a class="servicos-rodape" href="http://metaexecutivos.com.br/para-empresas/assessment" target="_blank">» Assessment «</a></li>
                                <li><a class="servicos-rodape" href="http://metaexecutivos.com.br/para-empresas/coaching" target="_blank">» Coaching «</a></li>
                                <li><a class="servicos-rodape" href="http://metaexecutivos.com.br/para-empresas/desenvolvimento-organizacional" target="_blank">» Desenvolvimento Organizacional «</a></li>
                            </ul>
                        </div>
                        <div class="unidade">
                            <ul>
                                <li><a target="_blank" href="http://metatalentos.com.br" id="meta-talentos"></a></li>
                                <li><a class="servicos-rodape" href="http://metatalentos.com.br/empresas" target="_blank">» Estagiários «</a></li>
                                <li><a class="servicos-rodape" href="http://metatalentos.com.br/empresas" target="_blank">» Trainees «</a></li>
                                <li><a class="servicos-rodape" href="http://metatalentos.com.br/empresas" target="_blank">» Desenvolvimento de Jovens Talentos «</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="separador-footer"></div>
        <div class="footer">
            <div class="interna">
                <div class="info">
                    <div class="clearfix"></div>
                    <div id="social-footer">
                        <a href="http://www.facebook.com/metaexecutivos" id="facebook"></a>
                        <a href="http://www.linkedin.com/company/meta-executivos" id="linkedin"></a>
                    </div>
                    <ul>
                        <li>
                            <p>Tel 11 <b>5525-2711</b></p>
                        </li>
                        <li class="endereco">
                            <p>
                                Meta Executivos<br>
                                Av. Adolfo Pinheiro, 1001 &middot; 15<br>
                                Alto da Boa Vista <br>
                                04733-100 &middot; São Paulo - SP
                            </p>
                        </li><!--Fim do endereço-->
                        <li class="sitemap">
                           <ul>
                                <li><a href="<?php echo base_url(); ?>perfil">&raquo; a empresa | perfil</a></li>
                                <li><a href="<?php echo base_url(); ?>empresas">&raquo; serviços para empresas</a></li>
                                <li><a href="<?php echo base_url(); ?>candidatos">&raquo; serviços para profissionais</a></li>
                                <li><a href="<?php echo base_url(); ?>clientes">&raquo; clientes | cases</a></li>
                                <li><a href="<?php echo base_url(); ?>depoimentos">&raquo; depoimentos</a></li>
                                <li><a href="<?php echo base_url(); ?>novidades">&raquo; notícias</a></li>
                                <li><a href="<?php echo base_url(); ?>contato">&raquo; contato | localização</a></li>
                            </ul>
                        </li><!--Fim do sitemap-->
                    </ul>
                </div><!--Fim da div info-->
                <div class="copyright">
                    <p id="copy-meta">
                        &copy2012 Grupo Meta RH<br>
                        Todos os direitos reservados
                    </p>
                    <p id="trupe">
                        <a target="_blank" href="http://trupe.net">Criação de sites: Trupe Agência Criativa</a>
                    </p>
                    <a href="http://trupe.net" target="_blank" id="logo-trupe"></a>
                </div>
            </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/libs/bootstrap.min.js"></script>
        </body>
        <?php if(isset($vagaid)): ?>
            <script type="text/javascript">
                $(window).load(function(){
                    $('#homeModal').modal({
                        show: true,
                        remote: "<?php echo base_url(); ?>vagas/detalhe/<?php echo 'interna/' . $vagaid; ?>"
                    }).appendTo($("body"));
                });
            </script>
        <?php endif; ?>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-41810208-1', 'metaexecutivos.com.br');

  ga('send', 'pageview');

</script>
    </body>
</html>
