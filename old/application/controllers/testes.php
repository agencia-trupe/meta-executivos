<?php

Class Testes extends CI_Controller
{
    function index()
    {
        $this->load->library('seo');
        $title = $this->seo->get_title();
        $title2 = $this->seo->get_title('Título de Testes');

        echo 'Teste de título sem parâmetro: ' . $title . '<br>';
        echo 'Teste de título com parâmetro: ' . $title2 . '<br>';
    }
}