<?php

class Csv extends CI_Controller
{
    function index()
    {
        $query = $this->db->query("SELECT `newsletters_nome` AS `nome`,  `newsletters_email` AS `email` , `newsletters_vagas` AS `vagas`, `newsletters_novidades` AS `novidades`, `newsletters_eventos` AS `eventos`, `newsletters_resultados` AS `resultados`, `newsletters_noticias` AS `noticias`  FROM `newsletters`");
 
        $this->load->helper('csv');
        echo query_to_csv($query, TRUE, 'newsletter.csv');
    }
}