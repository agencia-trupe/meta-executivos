<?php
#################################################################
# Google Analytics by WarMarks.com                              #
# Website   : www.WarMarks.com                                  #
# Author     : Warren Marks                                     #
# Email      : admin@warmarks.com                               #
# Version    : 1.0.0                                            #
# License    : http://www.gnu.org/copyleft/gpl.html GNU/GPL     #
#################################################################

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin');

class plgSystemWarmarksga extends JPlugin
{
  function plgSystemWarmarksga(&$subject, $config)
  {
    parent::__construct($subject, $config);

    $this->_plugin = JPluginHelper::getPlugin( 'system', 'warmarksga' );
    $this->_params = new JParameter( $this->_plugin->params );
  }

  function onAfterRender()
  {
    global $mainframe;

    $ua_id = $this->params->get('uaid', '');

    if($ua_id == '' || $mainframe->isAdmin() || strpos($_SERVER["PHP_SELF"], "index.php") === false)
    {
      return;
    }

    $buffer = JResponse::getBody();
    $ga_code = '
<!-- Google Anayalatics Code starts -->
      <script type="text/javascript">
	  var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	  document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
	  </script>
	  <script type="text/javascript">
	  try {
	  var pageTracker = _gat._getTracker("'.$ua_id.'");
	  pageTracker._trackPageview();
	  } catch(err) {}</script>
      <!-- Google Anaylatics Code ends -->
      ';

    $pos = strrpos($buffer, "</body>");

    if($pos > 0)
    {
      $buffer = substr($buffer, 0, $pos).$ga_code.substr($buffer, $pos);

      JResponse::setBody($buffer);
    }

    return true;
  }
}
?>